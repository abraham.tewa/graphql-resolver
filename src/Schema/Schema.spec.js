/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */
// ============================================================
// Import packages
import { assert, expect } from 'chai';
import faker from 'faker';
import proxyquire from 'proxyquire';

// ============================================================
// Import modules
import {
    SchemaAlreadyDeclaredError,
    SchemaSealedError,
    TypeResolverAlreadyDeclaredError,
} from '../errors';

import Schema,
{
    declareSchema,
    getFileContent,
    resolvePatterns,
}
    from './Schema';
import GraphqlTypeResolver from '../GraphqlTypeResolver';
import { Type } from '../public';

// ============================================================
// Tests

describe('Schema suite', () => {
    describe('class Schema', () => {
        describe('instanciation', () => {
            it('doesn\'t declare it', () => {
                const ref = Symbol('testing');
                // eslint-disable-next-line no-new
                new Schema(ref);

                assert.isUndefined(Schema.get(ref));
            });
        });

        describe('.constructor', () => {
            it('doesn\'t declare schema', function it() {
                const ref = Symbol(this.test.fullTitle());
                // eslint-disable-next-line no-new
                new Schema(ref);

                assert.isUndefined(Schema.get(ref));
            });

            it('set correctly the reference', function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                assert.strictEqual(ref, schema.getRef());
            });

            it('set graphql file patterns as empty list', function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                assert.isArray(schema.getGraphqlFilePatterns());
                assert.lengthOf(schema.getGraphqlFilePatterns(), 0);
            });

            it('can accept a string as file pattern', function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(
                    ref,
                    '**/*.graphql',
                );

                assert.isArray(schema.getGraphqlFilePatterns());
                assert.lengthOf(schema.getGraphqlFilePatterns(), 1);
            });

            it('can accept a list as file pattern', function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(
                    ref,
                    ['**/*.graphql'],
                );

                assert.isArray(schema.getGraphqlFilePatterns());
                assert.lengthOf(schema.getGraphqlFilePatterns(), 1);
            });
        });

        describe('add', () => {
            it('add declaration', async function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                const declaration1 = `
                    schema {
                        query: Query
                    }
                `;

                const declaration2 = `
                type Query {
                    emptySchema: Boolean
                }
                `;

                schema.add(declaration1, declaration2);

                const promise = schema.getSchemaContent();

                expect(promise).to.be.promise();

                const content = await promise;

                assert.isArray(content);
                assert.lengthOf(content, 2);
                assert.strictEqual(content[0], declaration1, 'declaration1');
                assert.strictEqual(content[1], declaration2, 'delcaration2');
            });
        });

        describe('.addGraphqlFilePattern()', () => {
            it('add file pattern', function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                const pattern = '**/*.graphql';
                schema.addGraphqlFilePattern(pattern);

                const patterns = schema.getGraphqlFilePatterns();

                assert.lengthOf(patterns, 1);
                assert.equal(patterns[0], pattern);
            });

            it('throw an error if sealed', () => {
                const ref = Symbol('sealing test');
                const schema = new Schema(ref);

                schema.seal();

                const pattern = '**/*.graphql';
                expect(() => schema.addGraphqlFilePattern(pattern))
                    .to.matchErrorSnapshot(SchemaSealedError)
                    .to.have.property('schemaRef', ref);
            });
        });

        describe('.compile()', () => {
            it('doesn\'t throw error if no schema', async function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                await schema.compile();

                assert.isTrue(true);
            });

            it('seal the schema synchronously', async function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                const promise = schema.compile();

                assert.isTrue(schema.isSealed());

                await promise;
            });

            it('use cache', async function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                const executableSchema = await schema.compile();
                const newSchema = await schema.compile();

                assert.strictEqual(newSchema, executableSchema);
            });

            it('use promise cache', async function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                const promise = schema.compile();
                const newPromise = schema.compile();

                const fromPromise = await promise;
                const fromNewPromise = await newPromise;

                const fromAfterPromise = await schema.compile();

                assert.strictEqual(fromNewPromise, fromPromise, 'from new promise');
                assert.strictEqual(fromAfterPromise, fromPromise, 'from after promise');
            });

            it('compile schema', async function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                const Class = class {
                    static fieldA() {
                        return 'A';
                    }
                };

                Type(
                    Class,
                    {
                        name: 'Type',
                        schema,
                    },
                );

                schema.add(`
                    schema {
                        query: Query
                    }

                    type Query {
                        type: Type
                    }

                    type Type {
                        fieldA: String!
                    }
                `);

                const promise = schema.compile();

                assert.isTrue(schema.isSealed());

                await promise;
            });
        });

        describe('.declareTypeResolver()', () => {
            it('throw an error if sealed', () => {
                const ref = Symbol('sealing test');
                const schema = new Schema(ref);

                schema.seal();

                expect(() => schema.declareTypeResolver(undefined))
                    .to.matchErrorSnapshot(SchemaSealedError)
                    .to.have.property('schemaRef', ref);
            });

            it('add a type resolver', function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);
                const name = 'abc';

                const TypeResolver = getNewTypeResolver({ name });

                schema.declareTypeResolver(name, TypeResolver);
                assert.property(schema.getTypeResolvers(), name);
            });

            it('throw an error if resolver declared with the same name', () => {
                const ref = Symbol('another symbol');
                const schema = new Schema(ref);
                const name = 'abc';

                const TypeResolver = getNewTypeResolver({ name });

                schema.declareTypeResolver(name, TypeResolver);
                expect(() => schema.declareTypeResolver(name, TypeResolver))
                    .to.matchErrorSnapshot(TypeResolverAlreadyDeclaredError)
                    .to.include({
                        typeName: name,
                        schemaRef: ref,
                        TypeResolver,
                    });
            });
        });

        describe('.getGraphqlSchema()', () => {
            it('return undefined if no compilation has been performed', function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                const executableSchema = schema.getGraphqlSchema();
                assert.isUndefined(executableSchema);
            });

            it.only('return compiled schema', async function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                const executableSchema = await schema.compile();
                const returnedExecutableSchema = schema.getGraphqlSchema();

                assert.strictEqual(returnedExecutableSchema, executableSchema);
            });
        });

        describe('.getGraphqlFilePaths()', () => {
            it('return empty list if no pattern provided', async function it() {
                // ==============================
                // Prepare
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                // ==============================
                // Execution
                const files = await schema.getGraphqlFilePaths();

                // ==============================
                // Assertions
                assert.isArray(files);
                assert.equal(files.length, 0);
            });

            it('return all file paths from patterns', async function it() {
                // ==============================
                // Creating proxy
                const pattern = '**/*.graphql';
                const fileA = 'a.graphql';
                const fileB = 'b.graphql';
                const fileMap = {
                    [pattern]: [
                        fileA,
                        fileB,
                    ],
                };

                const { default: SchemaMocked } = proxyquire(
                    './Schema',
                    {
                        glob: mockGlob(fileMap),
                    },
                );

                // ==============================
                // Prepare
                const ref = Symbol(this.test.fullTitle());
                const schema = new SchemaMocked(ref, [pattern]);

                const files = await schema.getGraphqlFilePaths();

                assert.lengthOf(files, 2);

                const endsWith = (file) => file.endsWith(fileA) || file.endsWith(fileB);

                assert.isTrue(endsWith(files[0]), 'fileA');
                assert.isTrue(endsWith(files[1]), 'fileB');
            });
        });

        describe('.getSchemaContent()', () => {
            it('return empty list if no pattern provided', async function it() {
                // ==============================
                // Prepare
                const ref = Symbol(this.test.fullTitle());
                const schema = new Schema(ref);

                // ==============================
                // Execution
                const schemaContent = await schema.getSchemaContent();

                // ==============================
                // Assertions
                assert.isArray(schemaContent);
                assert.equal(schemaContent.length, 0);
            });

            it('return empty list if no files', async function it() {
                // ==============================
                // Creating proxy
                const pattern = '**/*.graphql';
                const fileMap = {
                    [pattern]: [],
                };

                const { default: SchemaMocked } = proxyquire(
                    './Schema',
                    {
                        glob: mockGlob(fileMap),
                    },
                );

                // ==============================
                // Prepare
                const ref = Symbol(this.test.fullTitle());
                const schema = new SchemaMocked(ref, [pattern]);

                // ==============================
                // Execution
                const schemaContent = await schema.getSchemaContent();

                // ==============================
                // Assertions
                assert.isArray(schemaContent);
                assert.equal(schemaContent.length, 0);
            });

            it('return graphql file contents', async function it() {
                // ==============================
                // Creating proxy
                const pattern = '**/*.graphql';
                const fileA = 'a.graphql';
                const fileB = 'b.graphql';
                const content = {
                    [fileA]: faker.random.words(20),
                    [fileB]: faker.random.words(20),
                };
                const fileMap = {
                    [pattern]: [fileA, fileB],
                };

                const { default: SchemaMocked } = proxyquire(
                    './Schema',
                    {
                        'fs-extra': {
                            // Since readFile will receive an absolute path, we need
                            // to search the correct file
                            readFile: async (filePath) => Object.entries(content)
                                .filter(([name]) => filePath.endsWith(name))
                                .map(([, fileContent]) => fileContent)[0],
                        },
                        glob: mockGlob(fileMap),
                    },
                );

                // ==============================
                // Prepare
                const ref = Symbol(this.test.fullTitle());
                const schema = new SchemaMocked(ref, [pattern]);

                // ==============================
                // Execution
                const schemaContent = await schema.getSchemaContent();

                // ==============================
                // Assertions
                assert.isArray(schemaContent);
                assert.equal(schemaContent.length, Object.keys(content).length);
                assert.include(schemaContent, content[fileA]);
                assert.include(schemaContent, content[fileB]);
            });
        });

        describe('static .declare()', () => {
            it('throw an exception if schema already exists', () => {
                const ref = Symbol('Schema / Schema.declare / 1');

                Schema.declare(ref);

                expect(() => Schema.declare(ref)).to.matchErrorSnapshot(SchemaAlreadyDeclaredError);
                Schema.forgot(ref);
            });

            it('declare a new schema', () => {
                const ref = Symbol('Schema / Schema.declare / 2');

                const schema = Schema.declare(ref);

                assert.instanceOf(schema, Schema);

                const gettedSchema = Schema.get(ref);

                assert.strictEqual(schema, gettedSchema);
                Schema.forgot(ref);
            });

            it('can declare any type of schema', () => {
                const listRef = [
                    3.14,
                    Symbol,
                    '1',
                    {},
                    true,
                    false,
                ];

                assert.doesNotThrow(() => listRef.forEach((ref) => Schema.declare(ref)));
                listRef.forEach((ref) => Schema.forgot(ref));
            });

            it('does not use toString()', () => {
                const listRef = [
                    {},
                    {},
                    Symbol('abc'),
                    Symbol('abc'),
                    true,
                    false,
                ];

                assert.doesNotThrow(() => listRef.map((ref) => Schema.declare(ref)));
                listRef.forEach((ref) => Schema.forgot(ref));
            });

            describe('direct declaration', () => {
                it('return the declared schema', function it() {
                    const ref = Symbol(this.test.fullTitle());
                    const schema = new Schema(ref);

                    const declaredSchema = Schema.declare(schema);

                    assert.strictEqual(schema, declaredSchema);
                    Schema.forgot(ref);
                });

                it('correctly declared the schema', function it() {
                    const ref = Symbol(this.test.fullTitle());
                    const schema = new Schema(ref);

                    Schema.declare(schema);

                    const gettedSchema = Schema.get(ref);

                    assert.strictEqual(schema, gettedSchema);
                    Schema.forgot(ref);
                });

                it('throw an exception if the reference has already been declared', () => {
                    const ref = Symbol('some symbol');
                    const schema = new Schema(ref);

                    Schema.declare(ref);

                    expect(() => Schema.declare(schema)).to.matchErrorSnapshot(SchemaAlreadyDeclaredError);
                    Schema.forgot(ref);
                });

                it('can redeclare schema twice', function it() {
                    const ref = Symbol(this.test.fullTitle());

                    const count = Schema.getDeclared().length;
                    const schema = new Schema(ref);

                    Schema.declare(schema);

                    assert.equal(Schema.getDeclared().length, count + 1);

                    const newSchema = Schema.declare(schema);

                    assert.equal(Schema.getDeclared().length, count + 1);
                    assert.equal(newSchema, schema);

                    Schema.forgot(ref);
                });
            });
        });

        describe('static .forgot()', () => {
            it('do nothing if ref is not found', function it() {
                const ref = Symbol(this.test.fullTitle());

                const count = Schema.getDeclared().length;

                Schema.forgot(ref);

                assert.strictEqual(Schema.getDeclared().length, count);
            });

            it('forgot the schema', function it() {
                const ref = Symbol(this.test.fullTitle());

                const schema = Schema.declare(ref);

                Schema.forgot(ref);

                assert.notInclude(Schema.getDeclared(), schema);
            });
        });

        describe('static .get()', () => {
            it('return undefined if reference is not found', function it() {
                const ref = Symbol(this.test.fullTitle());
                assert.isUndefined(Schema.get(ref));
                Schema.forgot(ref);
            });

            it('return the declared schema', function it() {
                const ref = Symbol(this.test.fullTitle());
                const schema = Schema.declare(ref);

                assert.equal(Schema.get(ref), schema);
                Schema.forgot(ref);
            });
        });

        describe('static .getDeclare()', () => {
            it('return the list of declared schema', () => {
                const refA = Symbol('A');
                const refB = Symbol('B');
                const refC = Symbol('C');

                const schemaA = Schema.declare(refA);
                const schemaB = Schema.declare(refB);
                const schemaC = Schema.declare(refC);

                const list = Schema.getDeclared();

                assert.include(list, schemaA);
                assert.include(list, schemaB);
                assert.include(list, schemaC);

                Schema.forgot(refA);
                Schema.forgot(refB);
                Schema.forgot(refC);
            });
        });
    });

    describe('public declareSchema()', () => {
        it('declare a schema', () => {
            const ref = Symbol('some ref');
            const files = '**/*.graphqls';

            const schema = declareSchema(ref, files);

            assert.equal(Schema.get(ref), schema);
            assert.equal(schema.getRef(), ref);
            assert.deepEqual(schema.getGraphqlFilePatterns(), [files]);
        });
    });

    describe('internal resolvePatterns()', () => {
        it('return undefined if list is empty', async () => {
            const patterns = await resolvePatterns([]);

            assert.isArray(patterns);
            assert.isEmpty(patterns);
        });

        it('return the list of found files', async () => {
            const pattern = '**/*.graphql';
            const fileA = 'a.graphql';
            const fileB = 'b.graphql';
            const fileMap = {
                [pattern]: [fileA, fileB],
            };

            const { resolvePatterns: resolvePatternsMocked } = proxyquire(
                './Schema',
                {
                    glob: mockGlob(fileMap),
                },
            );

            const files = await resolvePatternsMocked([pattern]);

            assert.equal(files.length, fileMap[pattern].length);
            const endsWith = (file) => file.endsWith(fileA) || file.endsWith(fileB);

            assert.isTrue(endsWith(files[0]));
            assert.isTrue(endsWith(files[1]));
        });

        it('check all provided patterns', async () => {
            const patternA = 'src/**/*.graphql';
            const patternB = 'graphs/**/*.graphql';
            const fileMap = {
                [patternA]: [
                    'a.graphql',
                    'b.graphql',
                ],
                [patternB]: [
                    'c.graphql',
                    'd.graphql',
                ],
            };

            const { resolvePatterns: resolvePatternsMocked } = proxyquire(
                './Schema',
                {
                    glob: mockGlob(fileMap),
                },
            );

            const files = await resolvePatternsMocked([patternA, patternB]);

            assert.equal(files.length, fileMap[patternA].length + fileMap[patternB].length);
        });

        it('return deduplicated results', async () => {
            const patternA = 'src/**/*.graphql';
            const patternB = 'graphs/**/*.graphql';
            const fileMap = {
                [patternA]: [
                    'a.graphql',
                    'b.graphql',
                ],
                [patternB]: [
                    'a.graphql',
                    'b.graphql',
                ],
            };

            const { resolvePatterns: resolvePatternsMocked } = proxyquire(
                './Schema',
                {
                    glob: mockGlob(fileMap),
                },
            );

            const files = await resolvePatternsMocked([patternA, patternB]);

            assert.equal(files.length, 2);
        });
    });

    describe('internal getFileContent()', () => {
        it('return empty list if no paths is empty', async () => {
            const result = await getFileContent([]);

            assert.isArray(result);
            assert.isEmpty(result);
        });

        it('return file content', async () => {
            const contents = {
                'a.graphql': faker.random.words(10),
                'b.graphql': faker.random.words(10),
            };

            const { getFileContent: getFileContentMocked } = proxyquire(
                './Schema',
                {
                    'fs-extra': {
                        readFile: async (filePath) => contents[filePath],
                    },
                },
            );

            const content = await getFileContentMocked(Object.keys(contents));

            assert.isArray(content);
            assert.lengthOf(content, 2);

            // Order is not guaranted by the function
            assert.include(content, contents['a.graphql']);
            assert.include(content, contents['b.graphql']);
        });

        it('limit the number of simultaneous reads', async function it() {
            const firstPhaseDuration = 20;
            const secondPhaseDuration = 3;
            const files = {
                'a.graphql': { duration: firstPhaseDuration, content: faker.random.words(10) },
                'b.graphql': { duration: firstPhaseDuration, content: faker.random.words(10) },
                'c.graphql': { duration: secondPhaseDuration, content: faker.random.words(10) },
                'd.graphql': { duration: secondPhaseDuration, content: faker.random.words(10) },
            };

            const timeout = (firstPhaseDuration + secondPhaseDuration) * 1.5;

            this.timeout(timeout * 3);

            const starts = {};

            const { getFileContent: getFileContentMocked } = proxyquire(
                './Schema',
                {
                    'fs-extra': {
                        readFile: async (filePath) => {
                            starts[filePath] = new Date().getTime();
                            await wait(files[filePath].duration);
                            return files[filePath].content;
                        },
                    },
                },
            );

            const initialDate = new Date().getTime();
            const content = await getFileContentMocked(Object.keys(files), 2);

            assert.isArray(content);
            assert.lengthOf(content, 4);

            assert.closeTo(starts['a.graphql'], initialDate, 100, 'a.graphql');
            assert.closeTo(starts['b.graphql'], initialDate, 100, 'b.graphql');
            assert.closeTo(starts['c.graphql'], initialDate + firstPhaseDuration, 100, 'c.graphql');
            assert.closeTo(starts['d.graphql'], initialDate + firstPhaseDuration, 100, 'd.graphql');

            assert.strictEqual(files['a.graphql'].content, content[0]);
            assert.strictEqual(files['b.graphql'].content, content[1]);
            assert.strictEqual(files['c.graphql'].content, content[2]);
            assert.strictEqual(files['d.graphql'].content, content[3]);
        });
    });
});

// ============================================================
// Helpers
function mockGlob(listMap) {
    const glob = async function fct(pattern) {
        return listMap[pattern];
    };

    glob.glob = glob;

    return glob;
}

function getNewTypeResolver(options) {
    return new GraphqlTypeResolver(class {}, options);
}

async function wait(duration) {
    return new Promise((resolve) => {
        setTimeout(resolve, duration);
    });
}
