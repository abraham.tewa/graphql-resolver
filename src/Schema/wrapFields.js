// ============================================================
// Import modules
import {
    convertToType,
    getTypeInfo,
    fromEntries,
} from '../helpers';

// ============================================================
// Functions

/**
 *
 * @param {*} schema
 * @param {*} name
 * @param {*} wrappedTypes
 * @param {Object.<GraphqlTypeResolver>} resolverMap
 * @returns {Array.<{field: string, type: string, arrayLevel: Number, resolvedType: string}>}
 */
function listFieldsToWrap(schema, name, wrappedTypes = [], resolverMap = {}) {
    const ast = schema.getType(name).astNode;

    wrappedTypes.push(name);

    if (ast.kind === 'UnionTypeDefinition') {
        const unionTypeNameList = ast.types.map(({ name: typeName }) => typeName.value);

        const lists = unionTypeNameList
            .map((unionType) => listFieldsToWrap(
                schema,
                unionType,
                wrappedTypes,
                resolverMap,
            ))
            .flat();

        return lists;
    }
    if (ast.kind === 'ScalarTypeDefinition' || ast.kind === 'EnumTypeDefinition') {
        return [];
    }

    const fields = ast
        .fields

        // Getting field info
        .map((field) => getTypeInfo(field))

        .filter((info) => info)

        // Keeping only resolved types
        .filter(({ type }) => resolverMap[type])

        // Removing types already wrapped
        .filter(({ type: fieldType }) => !wrappedTypes.includes(fieldType))

        // Removing fields that have resolvers
        .filter(({ name: fieldName }) => !resolverMap[name].getResolvedFields().includes(fieldName))

        .map((info) => ({
            type: name,
            field: info.name,
            resolvedType: info.type,
            arrayLevel: info.arrayLevel,
        }));

    const types = fields.reduce((acc, field) => {
        if (!acc.includes(field.resolvedType)) {
            acc.push(field.resolvedType);
        }

        return acc;
    }, []);

    const childTypes = types
        .map((childType) => listFieldsToWrap(
            schema,
            childType,
            wrappedTypes,
            resolverMap,
        ))
        .flat();

    return [
        ...fields,
        ...childTypes,
    ];
}

function wrapFields(schema, resolverMap) {
    const wrapTypes = [];

    const resolverClassMap = Object.entries(resolverMap)
        .map(([name, graphqlTypeResolver]) => [
            name,
            graphqlTypeResolver.getResolverClass(),
        ])
        .reduce(fromEntries, {});

    const fields = [];

    if (schema.getType('Query')) {
        fields.push(...listFieldsToWrap(schema, 'Query', wrapTypes, resolverMap));
    }

    if (schema.getType('Mutation')) {
        fields.push(...listFieldsToWrap(schema, 'Mutation', wrapTypes, resolverMap));
    }

    if (schema.getType('Subscription')) {
        fields.push(...listFieldsToWrap(schema, 'Subscription', wrapTypes, resolverMap));
    }

    const typeConvertCallbaks = Object
        .entries(resolverClassMap)
        .map(([name, Class]) => [name, convertToType.bind(undefined, Class)])
        .reduce(fromEntries, {});

    const resolvers = fields.reduce((
        acc,
        {
            field, type, resolvedType,
        },
    ) => {
        if (!acc[type]) {
            acc[type] = {};
        }

        acc[type][field] = typeConvertCallbaks[resolvedType];

        return acc;
    },
    {});

    return resolvers;
}

// ============================================================
// Exports
export default wrapFields;
