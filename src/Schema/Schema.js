// ============================================================
// Import packages
import {
    addResolveFunctionsToSchema,
    makeExecutableSchema,
} from 'graphql-tools';
import async from 'async';
import fs from 'fs-extra';
import glob from 'glob';
import { graphql } from 'graphql';
import _ from 'lodash';
import path from 'path';

// ============================================================
// Import packages
import { defaultSchema as defaultSchemaRef } from '../constants';
import {
    SchemaAlreadyDeclaredError,
    SchemaSealedError,
    TypeResolverAlreadyDeclaredError,
} from '../errors';

import { fromEntries } from '../helpers';
import {
    onCompilationStart,
    onCompilationEnd,
} from '../status';

import wrapFields from './wrapFields';
import QueryContext from '../QueryContext';

// ============================================================
// Class

class Schema {
    /**
     * List of all declared schema.
     * @type {Map.<Schema>}
     */
    static #schemas = new Map();

    /**
     * Promise resolved once compilation has finished.
     * @type {Promise}
     */
    #compilePromise;

    #contextCreator;

    /**
     * Graphql types declarations.
     * @type {string[]}
     */
    #graphqlDeclarations = [];

    /**
     * Content of graphql files.
     * Will be emptied each time a new pattern is added.
     * We keep the content of the file so users can debug their graphs.
     * @type {string[]}
     */
    #graphqlFilesContent;

    /**
     * List of graphql files pattern.
     * @type {string[]}
     */
    #graphqlFilesPattern = [];

    /**
     * Reference of the schema
     * @type {*}
     */
    #ref;

    /**
     * List of resolvers
     * @type {Object.<GraphqlTypeResolver>}
     */
    #resolvers = {};

    /**
     * Indicate if the schema has been sealed or not
     * @type {boolean}
     */
    #sealed = false;

    /**
     * Graphql Schema object
     * @type {GraphqlSchema}
     */
    #schema;

    // ========================================
    // Methods

    /**
     * @param {*}               ref            - Reference of the schema
     * @param {string|string[]} [graphqlFiles] - List of graphql files. Accept glob patterns
     */
    constructor(
        ref,
        graphqlFiles = [],
        contextCreator,
    ) {
        this.#ref = ref;
        this.#graphqlFilesPattern = Array.isArray(graphqlFiles)
            ? [...graphqlFiles]
            : [graphqlFiles];

        this.#contextCreator = contextCreator;
    }

    /**
     * Add graphql declarations to the schema
     * @param {string} declarations
     * @public
     */
    add(...declarations) {
        this.#graphqlDeclarations.push(...declarations);
    }

    /**
     * Add a new path where to find graphql files.
     * Throw an error if already compiled.
     * @param {string} pattern
     * @public
     */
    addGraphqlFilePattern(pattern, cwd) {
        if (this.isSealed()) {
            throw new SchemaSealedError(this.getRef());
        }

        this.#graphqlFilesPattern.push({
            pattern,
            cwd: cwd || process.cwd(),
        });
    }

    /**
     * Compile and seal the schema.
     * If the compilation is ongoing or already performed, will do nothing.
     * @returns {ExecutableSchema}
     * @public
     */
    async compile() {
        if (this.#compilePromise) {
            return this.#compilePromise;
        }

        onCompilationStart(this);

        this.seal();

        let resolvePromise;

        this.#compilePromise = new Promise((resolve) => {
            resolvePromise = resolve;
        });

        const types = await this.getSchemaContent();

        let typeDefs = types;

        if (!types.length) {
            typeDefs = `
            schema {
                query: Query
            }

            type Query {
                emptySchema: Boolean
            }
            `;
        }

        const resolvers = compileResolvers(this.#resolvers);

        this.#schema = makeExecutableSchema({
            typeDefs,
            resolvers,
        });

        Object.values(this.#resolvers)
            .forEach((resolver) => resolver.finalize());

        const wrappedFields = wrapFields(
            this.#schema,
            this.#resolvers,
        );

        addResolveFunctionsToSchema({
            schema: this.#schema,
            resolvers: wrappedFields,
        });

        resolvePromise(this.#schema);

        onCompilationEnd(this);

        return this.#compilePromise;
    }

    /**
     * Declare a type resolver
     * @param {string}              name         - Name of the resolver
     * @param {GraphqlTypeResolver} TypeResolver - Declared resolver
     * @private
     */
    declareTypeResolver(TypeResolver) {
        if (this.isSealed()) {
            throw new SchemaSealedError(this.getRef());
        }

        const name = TypeResolver.getTypeName();

        if (this.#resolvers[name]) {
            throw new TypeResolverAlreadyDeclaredError({
                name,
                TypeResolver,
                schema: this,
            });
        }

        this.#resolvers[name] = TypeResolver;
    }

    /**
     * Return the executable schema.
     * If schema has not been compiled, return undefined.
     * This will seal the schema.
     * @returns {ExecutableSchema}
     * @public
     */
    getGraphqlSchema() {
        return this.#schema;
    }

    /**
     * Return the list of declare files paths
     * @returns {string[]}
     * @public
     */
    getGraphqlFilePatterns() {
        return this.#graphqlFilesPattern.slice(0);
    }

    /**
     * Return graphql files paths used in this schema
     * @returns {Promise.<string[]>}
     * @public
     */
    async getGraphqlFilePaths() {
        const files = await resolvePatterns(this.#graphqlFilesPattern);
        return files;
    }

    /**
     * Return the reference of the schema
     * @returns {*}
     * @public
     */
    getRef() {
        return this.#ref;
    }

    /**
     * Return the content of all graphs included in the schema
     * @returns {Promise.<string[]>}
     * @public
     */
    async getSchemaContent() {
        const paths = await this.getGraphqlFilePaths();
        this.#graphqlFilesContent = await getFileContent(paths);

        return [
            ...this.#graphqlFilesContent,
            ...this.#graphqlDeclarations,
        ];
    }

    /**
     * @param {string} name
     */
    getTypeResolver(name) {
        return this.#resolvers[name];
    }

    /**
     * Return the list of all type resolvers.
     * @returns {Object}
     * @public
     */
    getTypeResolvers() {
        return { ...this.#resolvers };
    }

    /**
     * Indicate if the schema has been compiled or not.
     * @returns {boolean}
     * @public
     */
    isSealed() {
        return this.#sealed;
    }

    /**
     * Execute a query
     * @param {string} query - Query to resolve
     * @public
     */
    resolve(query) {
        const rootValue = {};

        const publicContext = this.#contextCreator
            ? this.#contextCreator()
            : {};

        const context = new QueryContext(
            publicContext,
        );

        return graphql(
            this.#schema,
            query,
            rootValue,
            context,
        );
    }

    /**
     * Seal the schema.
     * @public
     */
    seal() {
        this.#sealed = true;
    }

    // ========================================
    // Static methods
    /**
     * Declare a new schema
     * @param {*|Schema}        ref            - Reference of the schema
     * @param {string|string[]} [graphqlFiles] - List of graphql files. Accept glob patterns
     * @returns {Schema}
     * @public
     */
    static declare(ref, graphqlFiles, contextCreator) {
        if (ref instanceof Schema) {
            const existingSchema = this.get(ref.getRef());

            // If the schema has already been declared, we do nothing
            if (existingSchema === ref) {
                return ref;
            }

            // If a schema exists with the same ref, we throw an exception
            if (existingSchema) {
                throw new SchemaAlreadyDeclaredError(ref.getRef());
            }

            // Saving the schema
            this.#schemas.set(ref.getRef(), ref);

            return ref;
        }

        if (this.get(ref)) {
            throw new SchemaAlreadyDeclaredError(ref);
        }

        const schema = new Schema(ref, graphqlFiles, contextCreator);

        this.#schemas.set(ref, schema);

        return schema;
    }

    /**
     * Forgot a schema: Schema will no longer be visible.
     * @param {*} ref - Forgot a schema
     * @returns {Schema}
     * @public
     */
    static forgot(ref) {
        const schema = this.#schemas.get(ref);
        this.#schemas.delete(ref);
        return schema;
    }

    /**
     * Get a schema.
     *
     * @param {*} ref - Reference of the schema
     * @returns {Schema}
     * @public
     */
    static get(ref) {
        return this.#schemas.get(ref);
    }

    /**
     * Return the list of declared schemas
     * @returns {Schema[]}
     * @public
     */
    static getDeclared() {
        return [...this.#schemas.values()];
    }
}

// ============================================================
// Helpers

/**
 * @param {Object.<GraphqlTypeResolver>} typeResolvers
 * @returns {Object.<Object.<ApolloPropertyResolver>>}
 */
function compileResolvers(typeResolvers) {
    return Object.entries(typeResolvers)
        .map(([name, typeResolver]) => [
            name,
            typeResolver.getApolloResolver(),
        ])
        .reduce(fromEntries, {});
}

function declareSchema(ref, graphqlFiles) {
    return Schema.declare(ref, graphqlFiles);
}

/**
 * Return the content of graphql files.
 * @param {string[]} paths - Paths where to find graphql files
 * @param {number}   limit - Number of files to read simultaneously
 * @returns {Promise.<string[]>}
 * @private
 */
async function getFileContent(paths, limit = 10) {
    const files = await async.mapLimit(
        paths,
        limit,
        async (filePath, callback) => {
            const fileBuffer = await fs.readFile(filePath);
            const content = fileBuffer.toString();
            callback(undefined, content);
        },
    );

    return files;
}

/**
 * Resolve a list of glob patterns.
 * The method will return absolute file paths deduplicated.
 * @param {string[]} patterns
 * @returns {Promise.<string[]>}
 * @private
 */
async function resolvePatterns(patterns) {
    const promises = patterns.map(({ pattern, cwd }) => new Promise((resolve, reject) => {
        glob(pattern, { cwd }, (error, files) => {
            if (error) {
                reject(error);
                return;
            }

            resolve(
                files.map((relativePath) => path.join(cwd, relativePath)),
            );
        });
    }));

    const listPaths = await Promise.all(promises);
    const relativePaths = listPaths.flat();

    // Deduplicate files
    const paths = _.uniq(
        relativePaths.map((relativePath) => path.resolve(relativePath)),
    );

    return paths;
}

const defaultSchema = Schema.declare(defaultSchemaRef);

// ============================================================
// Exports
export default Schema;
export {
    declareSchema,
    defaultSchema,
    getFileContent,
    resolvePatterns,
};
