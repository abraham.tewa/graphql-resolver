// ============================================================
// Import packages
import _ from 'lodash';

// ============================================================
// Import modules
import { gqlFieldOptions } from '../constants';
import { DuplicateDeclarationError } from '../errors';
import GraphqlFieldResolver from '../GraphqlFieldResolver';

// ============================================================
// Function

function toResolver(resolver) {
    let isAggregation;
    let isMultifield;
    let nameOrOptions;

    if (resolver[gqlFieldOptions]) {
        nameOrOptions = resolver[gqlFieldOptions].options;
        isAggregation = resolver[gqlFieldOptions].isAggregation;
        isMultifield = resolver[gqlFieldOptions].isMultifield;
    }
    else {
        isAggregation = false;
        isMultifield = false;
    }

    const options = toOptions({
        nameOrOptions,
        resolver,
        isAggregation,
        isMultifield,
    });

    // Checking unicity of resolvers
    if (Array.isArray(options.resolve)) {
        const duplications = Object
            .entries(_.countBy(options.resolve))
            .filter(([, count]) => count > 1)
            .map(([name]) => name);

        if (duplications.length) {
            throw new DuplicateDeclarationError(duplications);
        }
    }

    return new GraphqlFieldResolver(options);
}

function toOptions({
    nameOrOptions,
    resolver,
    isAggregation,
    isMultifield,
}) {
    let options = {
        isMultifield,
        resolve: undefined,
        resolver,
    };

    if (isAggregation) {
        options.groupBy = {
            aggregator: undefined,
            query: true,
            path: false,
            properties: [],
            limits: {
                maxWait: 100,
                maxItems: 100,
            },
        };
    }

    if (!isMultifield) {
        options.resolve = [String(nameOrOptions)];
    }
    else if (isMultifield) {
        options.resolve = [...nameOrOptions];
    }
    else if (typeof nameOrOptions === 'boolean') {
        options.skipped = !nameOrOptions;
    }
    else if (nameOrOptions && !Array.isArray(nameOrOptions)) {
        let groupBy;

        if (isAggregation) {
            groupBy = {
                ...options.groupBy,
                ...nameOrOptions.groupBy,
                limits: {
                    ...options.groupBy.limits,
                    ...(nameOrOptions.groupBy || {}).limits,
                },
            };
        }

        options = {
            ...options,
            ...nameOrOptions,
            groupBy,
        };
    }

    return options;
}

// ============================================================
// Exports
export default toResolver;
export {
    toOptions,
};
