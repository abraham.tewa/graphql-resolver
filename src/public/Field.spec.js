/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */
// ============================================================
// Import packages
import { assert } from 'chai';

// ============================================================
// Import modules
import Field from './Field';
import { gqlFieldOptions } from '../constants';


// ============================================================
// Tests

describe('Field', () => {
    it('Add field information', () => {
        const options = {
            resolve: ['a', 'b'],
        };

        const fct = () => {};
        const returnedFct = Field(options, fct);

        assert.strictEqual(returnedFct, fct, 'resolver');
        assert.property(fct, gqlFieldOptions);
        assert.hasAllDeepKeys(fct[gqlFieldOptions], ['isAggregation', 'isMultifield', 'options']);

        assert.isFalse(fct[gqlFieldOptions].isAggregation, 'isAggregation');
        assert.isFalse(fct[gqlFieldOptions].isMultifield, 'isMultifield');
        assert.strictEqual(fct[gqlFieldOptions].options, options, 'options');
    });
});
