// ============================================================
// Import modules
import { gqlFieldOptions } from '../constants';

// ============================================================
// Functions
/**
 * @typedef {Object} GraphqlMultiFieldOptions
 * @extends GraphqlFieldOptions
 * @param {string[]}                         resolve
 * @param {GraphqlMultiFieldCallback|Object} resolve
 * @public
 */

/**
  * @callback GraphqlMultiFieldCallback
  * @this {GraphqlType}
  * @returns {Array.<Object>}
  * @public
  */

/**
 * Declare a function as a graphql property resolver.
 * @param {string|boolean|GraphqlMultiFieldOptions} nameOrOptions
 * @param {*} resolver
 * @public
 */
function MultiField(nameOrOptions, resolver) {
    // eslint-disable-next-line no-param-reassign
    resolver[gqlFieldOptions] = {
        isAggregation: false,
        isMultifield: true,
        options: nameOrOptions,
    };

    return resolver;
}

// ============================================================
// Exports
export default MultiField;
