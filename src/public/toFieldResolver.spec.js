/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */
// ============================================================
// Import packages
import { assert, expect } from 'chai';
import sinon from 'sinon';

// ============================================================
// Import modules
import { DuplicateDeclarationError } from '../errors';
import toResolver, { toOptions } from './toFieldResolver';

describe('toOptions()', () => {
    it('have default aggregation values', () => {
        const options = toOptions({
            resolver: sinon.spy(),
            isAggregation: true,
        });

        expect(options).to.matchSnapshot();
        assert.property(options, 'groupBy');
    });

    
});

describe('toResolver', () => {
    it('throw an exception if field declared twice', () => {
        const resolver = sinon.spy();
        const options = {
            nameOrOptions: {
                resolve: ['field', 'field'],
            },
            resolver,
            isMultifield: true,
            isAggregation: false,
        };

        expect(() => toResolver(options))
            .to.matchErrorSnapshot(DuplicateDeclarationError);
    });
});
