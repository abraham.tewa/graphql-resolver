// ============================================================
// Import modules
import { gqlFieldOptions } from '../constants';

// ============================================================
// Functions
/**
 * @typedef {Object} GraphqlMultiFieldAggregatorOptions
 * @extends GraphqlMultiFieldOptions
 * @param {GraphqlAggregatorGroupByOptions}  groupBy
 * @param {GraphqlAggregatorCallback|Object} resolve
 * @public
 */

/**
  * @callback GraphqlAggregatorCallback
  * @param {Array.<instance, fieldArgs, QueryContext>} items
  * @param {GraphqlAggregatorCallbackGroup}            group
  * @returns {Array.<Object>}
  * @public
  */

/**
 * Declare a function as a graphql multi field aggregator resolver.
 * @param {string|boolean|GraphqlMultiFieldAggregatorOptions} nameOrOptions
 * @param {*} callback
 * @public
 */
function MultiFieldAggregator(nameOrOptions, resolver) {
    // eslint-disable-next-line no-param-reassign
    resolver[gqlFieldOptions] = {
        isAggregation: true,
        isMultifield: true,
        options: nameOrOptions,
    };

    return resolver;
}

// ============================================================
// Exports
export default MultiFieldAggregator;
