/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */
// ============================================================
// Import packages
import { assert, expect } from 'chai';

// ============================================================
// Import modules
import Type, { toConflictResolverMap, toOptions } from './Type';

import { defaultSchema } from '../constants';
import { SchemaNotFoundError } from '../errors';
import Schema from '../Schema';

// ============================================================
// Tests

describe('Type suite', () => {
    describe('Type', () => {
        const ref = Symbol('Type');
        const schema = Schema.declare(ref);
        let lastNameNumber = 0;

        function getNewName() {
            lastNameNumber += 1;
            return `TypeName${lastNameNumber}`;
        }

        it('declare the type into the schema', () => {
            // ==============================
            // Preparation
            const Class = class {};
            const name = getNewName();

            // ==============================
            // Execution
            const TypedClass = Type(Class, { name, schema: ref });
            const resolvers = schema.getTypeResolvers();

            // ==============================
            // Assertions
            assert.instanceOf(TypedClass.prototype, Class);
            assert.property(resolvers, name);

            assert.strictEqual(TypedClass, resolvers[name].getResolverClass());
        });

        it('use the name of the class if none provided', () => {
            // ==============================
            // Preparation
            const Class = class Class {};

            // ==============================
            // Execution
            const TypedClass = Type(Class, { schema: ref });
            const resolvers = schema.getTypeResolvers();

            // ==============================
            // Assertions
            assert.instanceOf(TypedClass.prototype, Class);
            assert.property(resolvers, 'Class');
            assert.strictEqual(TypedClass, resolvers.Class.getResolverClass());
        });

        it('accept schema or reference', () => {
            // ==============================
            // Preparation
            const Class = class {};
            const nameA = getNewName();
            const nameB = getNewName();

            // ==============================
            // Execution
            const WithSchema = Type(Class, { name: nameA, schema });
            const WithRef = Type(Class, { name: nameB, schema: ref });

            const resolvers = schema.getTypeResolvers();

            // ==============================
            // Assertions
            assert.strictEqual(WithSchema, resolvers[nameA].getResolverClass());
            assert.strictEqual(WithRef, resolvers[nameB].getResolverClass());
        });
    });

    describe('internal toOptions()', () => {
        it('allow no parameters', () => {
            assert.doesNotThrow(() => toOptions(undefined, class {}));
        });

        it('use default schema if none provided', () => {
            const Class = class {};
            const options = toOptions(undefined, Class);

            assert.isObject(options);
            assert.hasAllKeys(
                options,
                [
                    'conflictResolver',
                    'name',
                    'ignore',
                    'schema',
                ],
            );

            assert.deepOwnInclude(
                options,
                {
                    name: 'Class',
                    ignore: [],
                    schema: Schema.get(defaultSchema),
                },
            );
        });

        it('accept a string as options', () => {
            // ==============================
            // Preparation
            const name = 'SomeType';
            const Class = class {};

            // ==============================
            // Execution
            const options = toOptions(name, Class);

            // ==============================
            // Assertions
            assert.isObject(options);
            assert.hasAllKeys(
                options,
                [
                    'conflictResolver',
                    'name',
                    'ignore',
                    'schema',
                ],
            );
            assert.deepOwnInclude(
                options,
                {
                    name,
                    ignore: [],
                    schema: Schema.get(defaultSchema),
                },
            );
        });

        it('accept an object as options', () => {
            // ==============================
            // Preparation
            const initialOptions = {
                name: 'SomeType',
                ignore: ['fieldA', 'fieldB'],
            };
            const Class = class {};

            // ==============================
            // Execution
            const options = toOptions(initialOptions, Class);

            // ==============================
            // Assertions
            assert.isObject(options);
            assert.hasAllKeys(
                options,
                [
                    'conflictResolver',
                    'name',
                    'ignore',
                    'schema',
                ],
            );
            assert.deepOwnInclude(
                options,
                {
                    name: initialOptions.name,
                    ignore: initialOptions.ignore,
                    schema: Schema.get(defaultSchema),
                },
            );
        });

        it('throw an error if schema is not declared', () => {
            const ref = Symbol('some schema');

            // ==============================
            // Preparation
            const initialOptions = {
                schema: ref,
            };
            const Class = class {};

            // ==============================
            // Execution
            expect(() => toOptions(initialOptions, Class))
                .to.not.throw();
        });

        it('use the provided schema', () => {
            const ref = Symbol('some schema');

            const schema = Schema.declare(ref);

            // ==============================
            // Preparation
            const initialOptions = {
                schema: ref,
            };
            const Class = class {};

            const options = toOptions(initialOptions, Class);

            assert.strictEqual(options.schema, schema);

            Schema.forgot(ref);
        });
    });

    describe('toConflictResolverMap', () => {
        it('return empty object if no map provided', () => {
            const map = toConflictResolverMap();

            assert.isObject(map);
            assert.isEmpty(map);
        });

        it('transform string into instance declaration', () => {
            const map = toConflictResolverMap({
                fieldA: 'propertyA',
            });

            assert.isObject(map);
            assert.hasAllKeys(map, ['fieldA']);
            assert.deepPropertyVal(map, 'fieldA', { static: false, resolvedBy: 'propertyA' });
        });

        it('do not transform if object', () => {
            const map = toConflictResolverMap({
                fieldA: { static: true, resolvedBy: 'propertyA' },
            });

            assert.isObject(map);
            assert.hasAllKeys(map, ['fieldA']);
            assert.deepPropertyVal(map, 'fieldA', { static: true, resolvedBy: 'propertyA' });
        });

        it('set non static as default', () => {
            const map = toConflictResolverMap({
                fieldA: { resolvedBy: 'propertyA' },
            });

            assert.isObject(map);
            assert.hasAllKeys(map, ['fieldA']);
            assert.deepPropertyVal(map, 'fieldA', { static: false, resolvedBy: 'propertyA' });
        });
    });
});
