// ============================================================
// Import modules
import { defaultSchema } from '../constants';
import Schema from '../Schema';
import GraphqlTypeResolver from '../GraphqlTypeResolver';
import { fromEntries } from '../helpers';

// ============================================================
// Functions

/**
 * @param {Class}                     TypeResolver - Type resolver
 * @param {GraphqlTypeOptions|string} options      - Options or name of the type resolver
 * @returns {GraphqlTypeResolver}
 */
function Type(RawResolver, nameOrOptions) {
    const options = toOptions(nameOrOptions, RawResolver);

    // Converting the class to a type resolver
    const TypeResolver = new GraphqlTypeResolver(RawResolver, options);

    // Returning the resolver class
    return TypeResolver.getResolverClass();
}

// ============================================================
// Helpers

function toConflictResolverMap(conflictResolver) {
    if (!conflictResolver) {
        return {};
    }

    return Object
        .entries(conflictResolver)
        .map(([field, value]) => {
            let property;

            if (typeof value === 'string') {
                property = {
                    static: false,
                    resolvedBy: value,
                };
            }
            else {
                property = {
                    static: false,
                    ...value,
                };
            }

            return [field, property];
        })
        .reduce(fromEntries, undefined);
}

/**
 *
 * @param {string|Object} [nameOrOptions]
 * @param {class}         typeResolver
 * @returns {GraphqlTypeOptions}
 */
function toOptions(nameOrOptions, typeResolver) {
    const defaultOptions = {
        conflictResolver: {},
        name: typeResolver.name,
        ignore: [],
        schema: defaultSchema,
    };

    let options;

    if (!nameOrOptions) {
        options = {
            ...defaultOptions,
            name: typeResolver.name,
        };
    }
    else if (typeof nameOrOptions === 'string') {
        options = {
            ...defaultOptions,
            name: nameOrOptions,
        };
    }
    else {
        options = {
            ...defaultOptions,
            ...nameOrOptions,
        };

        options.conflictResolver = toConflictResolverMap(options.conflictResolver);
    }

    // Setting the schema
    if (!(options.schema instanceof Schema)) {
        let schema = Schema.get(options.schema);

        // If schema not found, not throwing an exception
        // Why ?
        // Declaring a schema can be tricky: the declaration should be done before the
        // type creation. However, this is not always possible due to javascript import policy.
        // Instead, we create schema "on the fly".
        if (!schema) {
            schema = Schema.declare(options.schema);
        }

        options.schema = schema;
    }

    return options;
}

// ============================================================
// Exports

export default Type;
export {
    toConflictResolverMap,
    toOptions,
};
