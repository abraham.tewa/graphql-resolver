/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */
// ============================================================
// Import packages
import { assert } from 'chai';

// ============================================================
// Import modules
import Aggregator from './Aggregator';
import { gqlFieldOptions } from '../constants';


// ============================================================
// Tests

describe('Aggregator', () => {
    it('Add field information', () => {
        const options = {
            resolve: ['a', 'b'],
        };

        const fct = () => {};
        const returnedFct = Aggregator(options, fct);

        assert.strictEqual(returnedFct, fct, 'resolver');
        assert.property(fct, gqlFieldOptions);
        assert.hasAllDeepKeys(fct[gqlFieldOptions], ['isAggregation', 'isMultifield', 'options']);

        assert.isTrue(fct[gqlFieldOptions].isAggregation, 'isAggregation');
        assert.isFalse(fct[gqlFieldOptions].isMultifield, 'isMultifield');
        assert.strictEqual(fct[gqlFieldOptions].options, options, 'options');
    });
});
