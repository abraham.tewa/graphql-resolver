// ============================================================
// Import modules
import { gqlFieldOptions } from '../constants';

// ============================================================
// Functions
/**
 * @typedef {Object} GraphqlAggregatorOptions
 * @extends GraphqlFieldOptions
 * @param {GraphqlAggregatorGroupByOptions}       groupBy
 * @param {GraphqlFieldAggregatorCallback|Object} resolver
 */

/**
 * @callback GraphqlFieldAggregatorCallback
 * @param {Array.<instance, args, QueryContext>} items
 * @param {GraphqlAggregatorCallbackGroup}       group
 * @returns {Array.<*>}
    */

/**
 * Declare a function as a graphql aggregator resolver.
 * @param {string|GraphqlAggregatorOptions} nameOrOptions
 * @param {*} callback
 * @public
 */
function Aggregator(nameOrOptions, resolver) {
    // eslint-disable-next-line no-param-reassign
    resolver[gqlFieldOptions] = {
        isAggregation: true,
        isMultifield: false,
        options: nameOrOptions,
    };

    return resolver;
}

// ============================================================
// Exports
export default Aggregator;
