// ============================================================
// Import modules
import { gqlFieldOptions } from '../constants';

// ============================================================
// Functions

/**
 * @typedef {Object} GraphqlFieldOptions
 * @param {GraphqlAggregatorGroupByOptions} groupBy
 * @param {string}                          resolve
 * @param {GraphqlFieldCallback|Object}     resolver
 */

/**
 * @callback GraphqlFieldCallback
 * @this {GraphqlType}
 */

/**
 * Declare a function as a graphql property resolver.
 * @param {string|GraphqlFieldOptions} nameOrOptions
 * @param {function:*} callback
 * @public
 */
function Field(nameOrOptions, resolver) {
    // eslint-disable-next-line no-param-reassign
    resolver[gqlFieldOptions] = {
        isAggregation: false,
        isMultifield: false,
        options: nameOrOptions,
    };

    return resolver;
}

// ============================================================
// Exports
export default Field;
