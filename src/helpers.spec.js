/* eslint-env node, mocha */
// ============================================================
// Import packages
import { assert } from 'chai';

// ============================================================
// Import modules
import * as helpers from './helpers';

// ============================================================
// Tests
describe('isPromise', () => {
    it('recognize promise', () => {
        assert.isTrue(helpers.isPromise(Promise.resolve()));
    });

    it('doesn\'t recognize non-promise value', () => {
        assert.isFalse(helpers.isPromise(null));
        assert.isFalse(helpers.isPromise(undefined));
        assert.isFalse(helpers.isPromise(1));
        assert.isFalse(helpers.isPromise('1'));
        assert.isFalse(helpers.isPromise({}));
        assert.isFalse(helpers.isPromise({ then: 1 }));
        assert.isFalse(helpers.isPromise([]));
    });
});
