/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert, expect } from 'chai';

// ============================================================
// Import modules
import GraphqlTypeHelper from './GraphqlTypeHelper';
import { MultiField } from './public';
import GraphqlTypeResolver from './GraphqlTypeResolver';

// ============================================================
// Tests

describe('GraphqlTypeHelper suite', () => {
    const value = {};
    const valueA = {};
    const valueB = {};
    const Class = class {
        static field() {
            return value;
        }

        static multiField = MultiField(
            ['A', 'B'],
            () => ({ A: valueA, B: valueB }),
        )
    };

    const TypeResolver = new GraphqlTypeResolver(Class);

    describe('get GraphqlTypeHelper.prototype.queryContext', () => {
        it('return queryContext object', () => {
            const queryContext = {};
            const helper = new GraphqlTypeHelper(TypeResolver, queryContext, new Class());
            assert.strictEqual(helper.queryContext, queryContext);
        });
    });

    describe('GraphqlTypeHelper.prototype.resolve', () => {
        it('resolve a value', () => {
            const queryContext = {};
            const helper = new GraphqlTypeHelper(TypeResolver, queryContext, new Class());

            const resolvedField = helper.resolve('field');

            assert.strictEqual(resolvedField, value, 'field');
        });

        it('throw an error if arg is not an array', () => {
            const queryContext = {};
            const helper = new GraphqlTypeHelper(TypeResolver, queryContext, new Class());
            expect(() => helper.resolve('field', {})).to.matchErrorSnapshot(TypeError);
        });

        it('resolve multifield value', () => {
            const queryContext = {};
            const helper = new GraphqlTypeHelper(TypeResolver, queryContext, new Class());

            const resolvedFieldA = helper.resolve('A');
            const resolvedFieldB = helper.resolve('B');

            assert.strictEqual(resolvedFieldA, valueA, 'A');
            assert.strictEqual(resolvedFieldB, valueB, 'B');
        });
    });
});
