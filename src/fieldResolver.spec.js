/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert, expect } from 'chai';
import sinon from 'sinon';

// ============================================================
// Import modules
import { gqlData } from './constants';
import { Aggregator, Field, MultiField } from './public';
import fieldResolver, { aggregate } from './fieldResolver';
import GraphqlTypeResolver from './GraphqlTypeResolver';

// ============================================================
// Tests

describe('internal fieldResolver()', () => {
    it('respect synchronicity and passed arguments', () => {
        // ==============================
        // Preparation
        const args = {};
        const value = {};
        const parent = {};
        const spy = sinon.spy(() => value);
        const Class = class {
            fieldA = spy;
        };

        const typeResolver = new GraphqlTypeResolver(Class);
        const resolverMap = typeResolver.getResolverMap();

        // ==============================
        // Execution
        const resolvedValue = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldA',
            false,
            parent,
            [args],
            {},
            {},
        );

        // ==============================
        // Assertions
        assert.strictEqual(resolvedValue, value);

        // Checking spy call
        assert.isTrue(spy.calledOnce, 'spy called once');
        assert.instanceOf(spy.firstCall.thisValue, Class, 'this value');
        assert.lengthOf(spy.firstCall.args, 1);
        assert.strictEqual(spy.firstCall.args[0], args, 'arguments');

        assert.property(parent, gqlData);
        assert.isObject(parent[gqlData]);
        assert.strictEqual(parent[gqlData].instance, spy.firstCall.thisValue);
    });

    it('work with async results', async () => {
        // ==============================
        // Preparation
        const args = {};
        const value = {};
        const parent = {};
        const spy = sinon.spy(async () => value);
        const Class = class {
            fieldA = spy;
        };

        const typeResolver = new GraphqlTypeResolver(Class);
        const resolverMap = typeResolver.getResolverMap();

        // ==============================
        // Execution
        const promise = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldA',
            false,
            parent,
            [args],
            {},
            {},
        );

        // ==============================
        // Assertions

        expect(promise).to.be.promise();

        const resolvedValue = await promise;

        assert.strictEqual(resolvedValue, value);

        // Checking spy call
        assert.isTrue(spy.calledOnce, 'spy called once');
        assert.instanceOf(spy.firstCall.thisValue, Class, 'this value');
        assert.lengthOf(spy.firstCall.args, 1);
        assert.strictEqual(spy.firstCall.args[0], args, 'arguments');

        assert.property(parent, gqlData);
        assert.isObject(parent[gqlData]);
        assert.strictEqual(parent[gqlData].instance, spy.firstCall.thisValue);
    });

    it('use cache for resolved data', () => {
        // ==============================
        // Preparation
        const args = {};
        const value = {};
        const parent = {};
        const spy = sinon.spy(() => value);
        const Class = class {
            fieldA = spy;
        };

        const typeResolver = new GraphqlTypeResolver(Class);
        const resolverMap = typeResolver.getResolverMap();

        // ==============================
        // Execution
        const resolve = () => fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldA',
            false,
            parent,
            [args],
            {},
            {},
        );

        const firstResolvedValue = resolve();
        const secondResolvedValue = resolve();

        // ==============================
        // Assertions
        assert.strictEqual(secondResolvedValue, firstResolvedValue);

        // Checking spy call
        assert.isTrue(spy.calledOnce, 'spy called once');
    });

    it('use cache for multifield', () => {
        // ==============================
        // Preparation
        const args = {};
        const valueA = {};
        const valueB = {};
        const value = {
            fieldA: valueA,
            fieldB: valueB,
        };
        const parent = {};
        const spy = sinon.spy(() => value);
        const Class = class {
            fieldA = MultiField(['fieldA', 'fieldB'], spy);
        };

        const typeResolver = new GraphqlTypeResolver(Class);
        const resolverMap = typeResolver.getResolverMap();

        // ==============================
        // Execution
        const resolveFieldA = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldA',
            false,
            parent,
            [args],
            {},
            {},
        );

        const resolveFieldB = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldB',
            false,
            parent,
            [args],
            {},
            {},
        );

        // ==============================
        // Assertions

        // Spy only called once
        assert.isTrue(spy.calledOnce, 'spy called once');
        assert.strictEqual(spy.firstCall.args[0], args);

        // Correct values are returned
        assert.strictEqual(resolveFieldA, valueA, 'fieldA');
        assert.strictEqual(resolveFieldB, valueB, 'fieldB');
    });

    it('use cache for promise multifield', async () => {
        // ==============================
        // Preparation
        const args = {};
        const valueA = {};
        const valueB = {};
        const value = {
            fieldA: valueA,
            fieldB: valueB,
        };
        const parent = {};
        const spy = sinon.spy(async () => value);
        const Class = class {
            fieldA = MultiField(['fieldA', 'fieldB'], spy);
        };

        const typeResolver = new GraphqlTypeResolver(Class);
        const resolverMap = typeResolver.getResolverMap();

        // ==============================
        // Execution
        const promiseA = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldA',
            false,
            parent,
            [args],
            {},
            {},
        );

        const promiseB = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldB',
            false,
            parent,
            [args],
            {},
            {},
        );

        expect(promiseA).to.be.promise('promiseA');
        expect(promiseB).to.be.promise('promiseB');

        const [resolveFieldA, resolveFieldB] = await Promise.all([promiseA, promiseB]);

        // ==============================
        // Assertions

        // Spy only called once
        assert.isTrue(spy.calledOnce, 'spy called once');
        assert.strictEqual(spy.firstCall.args[0], args);

        // Correct values are returned
        assert.strictEqual(resolveFieldA, valueA, 'fieldA');
        assert.strictEqual(resolveFieldB, valueB, 'fieldB');
    });

    it('handle correctly undefined resolution for multifield', () => {
        // ==============================
        // Preparation
        const parent = {};
        const spy = sinon.spy(() => undefined);
        const Class = class {
            fieldA = MultiField(['fieldA', 'fieldB'], spy);
        };

        const typeResolver = new GraphqlTypeResolver(Class);
        const resolverMap = typeResolver.getResolverMap();

        // ==============================
        // Execution
        const resolveFieldA = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldA',
            false,
            parent,
            [],
            {},
            {},
        );

        const resolveFieldB = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldB',
            false,
            parent,
            [],
            {},
            {},
        );

        // ==============================
        // Assertions

        // Correct values are returned
        assert.isNull(resolveFieldA);
        assert.isNull(resolveFieldB);
    });

    it('handle correctly undefined resolution for async multifield', async () => {
        // ==============================
        // Preparation
        const parent = {};
        const spy = sinon.spy(async () => undefined);
        const Class = class {
            fieldA = MultiField(['fieldA', 'fieldB'], spy);
        };

        const typeResolver = new GraphqlTypeResolver(Class);
        const resolverMap = typeResolver.getResolverMap();

        // ==============================
        // Execution
        const promiseA = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldA',
            false,
            parent,
            [],
            {},
            {},
        );

        const promiseB = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldB',
            false,
            parent,
            [],
            {},
            {},
        );

        const [resolveFieldA, resolveFieldB] = await Promise.all([promiseA, promiseB]);

        // ==============================
        // Assertions

        // Correct values are returned
        assert.isNull(resolveFieldA);
        assert.isNull(resolveFieldB);
    });

    it('handle correctly field inheritance', () => {
        const value = {};
        // ==============================
        // Preparation
        const parentFieldA = sinon.spy();
        const childFieldA = sinon.spy(() => value);
        const Parent = class {
            fieldA = Field(parentFieldA);
        };

        const Child = class extends Parent {
            fieldA = Field(childFieldA)
        };

        const typeResolver = new GraphqlTypeResolver(Child);
        const resolverMap = typeResolver.getResolverMap();

        // ==============================
        // Execution
        const resolvedValue = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldA',
            false,
            {},
            [],
            {},
            {},
        );

        // ==============================
        // Assertions
        assert.isTrue(parentFieldA.notCalled);
        assert.isTrue(childFieldA.calledOnce);

        assert.strictEqual(resolvedValue, value);
    });

    it('handle correctly multi-field inheritance', () => {
        const valueA = {};
        const valueParentB = {};
        const valueChildB = {};
        const valueC = {};
        const valueParent = {
            fieldA: valueA,
            fieldB: valueParentB,
        };
        const valueChild = {
            fieldB: valueChildB,
            fieldC: valueC,
        };
        // ==============================
        // Preparation
        const parentFieldAB = sinon.spy(() => valueParent);
        const childFieldBC = sinon.spy(() => valueChild);
        const Parent = class {
            fieldA = MultiField(
                ['fieldA', 'fieldB'],
                parentFieldAB,
            );
        };

        const Child = class extends Parent {
            fieldC = MultiField(
                ['fieldB', 'fieldC'],
                childFieldBC,
            );
        };

        const typeResolver = new GraphqlTypeResolver(
            Child,
            {
                conflictResolver: {
                    fieldB: { static: false, resolvedBy: 'fieldC' },
                },
            },
        );
        const resolverMap = typeResolver.getResolverMap();
        const parent = {};

        // ==============================
        // Execution
        const resolvedValueA = fieldResolver(
            typeResolver,
            resolverMap.fieldA,
            'fieldA',
            false,
            parent,
            [],
            {},
            {},
        );

        const resolvedValueB = fieldResolver(
            typeResolver,
            resolverMap.fieldC,
            'fieldB',
            false,
            parent,
            [],
            {},
            {},
        );

        const resolvedValueC = fieldResolver(
            typeResolver,
            resolverMap.fieldC,
            'fieldC',
            false,
            parent,
            [],
            {},
            {},
        );

        // ==============================
        // Assertions
        assert.isTrue(parentFieldAB.calledOnce, 'parnetFieldAB called once');
        assert.isTrue(childFieldBC.calledOnce, 'childFieldBC called once');

        assert.strictEqual(resolvedValueA, valueA, 'value A');
        assert.strictEqual(resolvedValueB, valueChildB, 'value B');
        assert.strictEqual(resolvedValueC, valueC, 'value C');
    });

    it('resolve aggregations', async () => {
        // ==============================
        // Preparation
        const args = {};
        const value = {};
        const parent = {};
        // eslint-disable-next-line prefer-arrow-callback
        const spy = sinon.spy(async function spyCallackb() {
            return [value];
        });
        const Class = class {
            static fieldA = Aggregator(spy);
        };

        const TypeResolver = new GraphqlTypeResolver(Class);
        const resolverMap = TypeResolver.getResolverMap();

        // ==============================
        // Execution
        const promise = fieldResolver(
            TypeResolver,
            resolverMap.fieldA,
            'fieldA',
            true,
            parent,
            [args],
            {},
            {},
        );

        // ==============================
        // Assertions

        expect(promise).to.be.promise();

        const resolvedValue = await promise;

        assert.strictEqual(resolvedValue, value, 'value');

        // Checking spy call
        assert.isTrue(spy.calledOnce, 'spy called once');
        assert.strictEqual(
            spy.firstCall.thisValue,
            TypeResolver.getResolverClass(),
            'static this value',
        );
        assert.lengthOf(spy.firstCall.args, 2, 'args');

        const passedList = spy.firstCall.args[0];
        assert.isArray(passedList, 'passed list');

        const firstItem = passedList[0];

        assert.isObject(firstItem);
        assert.hasAllKeys(firstItem, ['instance', 'args', 'context'], 'item object');
        assert.isArray(firstItem.args, 'item args');

        assert.strictEqual(firstItem.args[0], args, 'arguments');

        assert.property(parent, gqlData);
        assert.isObject(parent[gqlData]);
        assert.strictEqual(parent[gqlData].instance, firstItem.instance, 'instance');
    });
});

describe('(internal) aggregate()', () => {
    it('resolve an aggregation', async () => {
        class Class {
            static field = Aggregator(
                (list) => list.map(({ args: [value] }) => value),
            )
        }

        const typeResolver = new GraphqlTypeResolver(Class);
        const resolver = typeResolver.getResolverMap().field;

        const fieldValue = 3.141592;

        const valuePromise = aggregate(
            typeResolver,
            resolver,
            new Class(),
            [fieldValue],
            {},
        );

        expect(valuePromise).to.be.promise();
        const value = await valuePromise;

        assert.strictEqual(value, fieldValue);
    });

    describe('aggregation group creation', () => {
        it('group by path', async () => {
            const spy = sinon.spy(
                (list, group) => list.map(({ instance, args }) => ({ instance, args, group })),
            );

            class Class {
                static field = Aggregator(
                    {
                        groupBy: {
                            path: true,
                            limits: {
                                maxWait: 10,
                            },
                        },
                    },
                    spy,
                )
            }

            const typeResolver = new GraphqlTypeResolver(Class);
            const resolver = typeResolver.getResolverMap().field;

            const fieldValue = 3.141592;

            const pathA = 'A';
            const pathB = 'B';
            const pathC = 'C';
            const queryContext = {};

            const promises = [pathA, pathA, pathB, pathB, pathC, pathC].map(async (path) => {
                const instance = new Class();

                const promise = aggregate(
                    typeResolver,
                    resolver,
                    instance,
                    [fieldValue],
                    queryContext,
                    { path },
                );

                expect(promise).to.be.promise();

                const value = await promise;

                assert.strictEqual(value.instance, instance, 'instance');
                assert.hasAllKeys(value.group, ['path']);
                assert.propertyVal(value.group, 'path', path, 'path');
            });

            await Promise.all(promises);

            assert.strictEqual(spy.callCount, 3);
        });

        it('provide the right parameters', async () => {
            const instanceMap = new Map();

            const aggregator = function aggregatorCallback(instance, [object, num]) {
                assert.strictEqual(this, typeResolver.getResolverClass());

                const info = instanceMap.get(instance);

                assert.isObject(info, 'info aggregator');
                assert.strictEqual(object, info.args[0]);
                assert.strictEqual(num, info.args[1]);

                return info.group;
            };

            class Class {
                static fieldA() {
                    return 'A';
                }

                static field = Aggregator(
                    {
                        groupBy: {
                            aggregator,
                            path: true,
                            properties: ['fieldA'],
                            limits: {
                                maxWait: 10,
                            },
                        },
                    },
                    function field(list, group) {
                        assert.strictEqual(this, typeResolver.getResolverClass(), '"this" value of resolver');

                        assert.hasAllKeys(group, ['aggregator', 'path', 'properties']);
                        assert.propertyVal(group.properties, 'fieldA', 'A', 'property');
                        assert.propertyVal(group, 'path', 'abc', 'path');

                        return list.map(({ instance, args }) => {
                            const info = instanceMap.get(instance);
                            assert.strictEqual(group.aggregator, info.group);

                            assert.isObject(info, 'info field');
                            assert.strictEqual(args, info.args);
                            return info;
                        });
                    },
                )
            }

            const groupA = 'A';
            const groupB = 'B';

            instanceMap.set(new Class(), { group: groupA, args: [{ }, 1] });
            instanceMap.set(new Class(), { group: groupA, args: [{ }, 2] });
            instanceMap.set(new Class(), { group: groupA, args: [{ }, 3] });
            instanceMap.set(new Class(), { group: groupB, args: [{ }, 4] });
            instanceMap.set(new Class(), { group: groupB, args: [{ }, 5] });
            instanceMap.set(new Class(), { group: groupB, args: [{ }, 6] });

            const typeResolver = new GraphqlTypeResolver(Class);
            const resolver = typeResolver.getResolverMap().field;

            const promises = Array
                .from(instanceMap.entries())
                .map(async ([instance, info]) => {
                    const promise = aggregate(
                        typeResolver,
                        resolver,
                        instance,
                        info.args,
                        {},
                        { path: 'abc' },
                    );

                    expect(promise).to.be.promise();

                    const value = await promise;

                    assert.strictEqual(value, info, 'value');
                });

            await Promise.all(promises);
        });

        it('can group by query', async () => {
            const spy = sinon.spy(() => []);

            class Class {
                static field = Aggregator(
                    {
                        groupBy: {
                            path: true,
                            query: true,
                            limits: {
                                maxWait: 10,
                            },
                        },
                    },
                    spy,
                )
            }

            const typeResolver = new GraphqlTypeResolver(Class);
            const resolver = typeResolver.getResolverMap().field;

            const fieldValue = 3.141592;

            const path = 'A';
            const queryContext = {};

            const promises = [path, path].map(async () => {
                const instance = new Class();

                const promise = aggregate(
                    typeResolver,
                    resolver,
                    instance,
                    [fieldValue],
                    queryContext,
                    { path },
                );

                expect(promise).to.be.promise();

                return promise;
            });

            await Promise.all(promises);

            assert.strictEqual(spy.callCount, 1);
        });

        it('can distinguish queries', async () => {
            const spy = sinon.spy(() => []);

            class Class {
                static field = Aggregator(
                    {
                        groupBy: {
                            path: true,
                            query: true,
                            limits: {
                                maxWait: 10,
                            },
                        },
                    },
                    spy,
                )
            }

            const typeResolver = new GraphqlTypeResolver(Class);
            const resolver = typeResolver.getResolverMap().field;

            const fieldValue = 3.141592;

            const path = 'A';

            const promises = [{}, {}].map(async (queryContext) => {
                const instance = new Class();

                const promise = aggregate(
                    typeResolver,
                    resolver,
                    instance,
                    [fieldValue],
                    queryContext,
                    { path },
                );

                expect(promise).to.be.promise();

                return promise;
            });

            await Promise.all(promises);

            assert.strictEqual(spy.callCount, 2);
        });

        it('can group without taking care of query context', async () => {
            const spy = sinon.spy(() => []);

            class Class {
                static field = Aggregator(
                    {
                        groupBy: {
                            path: true,
                            query: false,
                            limits: {
                                maxWait: 10,
                            },
                        },
                    },
                    spy,
                )
            }

            const typeResolver = new GraphqlTypeResolver(Class);
            const resolver = typeResolver.getResolverMap().field;

            const fieldValue = 3.141592;

            const path = 'A';
            const queryContext = {};

            const promises = [queryContext, queryContext].map(async (context) => {
                const instance = new Class();

                const promise = aggregate(
                    typeResolver,
                    resolver,
                    instance,
                    [fieldValue],
                    context,
                    { path },
                );

                expect(promise).to.be.promise();

                return promise;
            });

            await Promise.all(promises);

            assert.strictEqual(spy.callCount, 1);
        });
    });
});
