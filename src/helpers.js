// ============================================================
// Import modules
import { gqlResolverData } from './constants';

// ============================================================
// Functions

function fromEntries(acc, [name, value]) {
    (acc || {})[name] = value;
    return acc;
}

function convertToType(type, value) {
    if (isPromise(value)) {
        return value.then((v) => convertToType(type, v));
    }

    if (Array.isArray(value)) {
        return value.map((v) => convertToType(type, v));
    }

    return value instanceof type
        ? value[gqlResolverData].object
        : value;
}

function isPromise(value) {
    return Boolean(value && typeof value.then === 'function');
}

/**
 *
 * @param {GraphqlSchema} schema
 * @param {string}        type
 * @param {string}        name
 * @returns {string}
 */
function getResolvedType(schema, type, name) {
    const ast = schema.getType(type).astNode;

    const fieldAST = ast.fields.find((field) => field.name.value === name);

    return getTypeInfo(fieldAST);
}

/**
 *
 * @param {AST} field
 * @returns {{name: string, type: string, arrayLevel: Number}}
 */
function getTypeInfo(field) {
    const name = field.name
        ? field.name.value
        : ''; // TODO why the ternary ?

    let type;
    let arrayLevel = 0;

    // eslint-disable-next-line default-case
    switch (field.type.kind) {
    case 'NamedType':
        type = field.type.name.value;
        break;

    case 'ListType': {
        const itemType = getTypeInfo(field.type);

        if (!itemType) {
            return undefined;
        }

        type = itemType.type;
        arrayLevel = itemType.arrayLevel + 1;
        break;
    }

    case 'NonNullType': {
        const itemType = getTypeInfo(field.type);
        if (!itemType) {
            return undefined;
        }
        type = itemType.type;
        arrayLevel = itemType.arrayLevel;
        break;
    }
    }

    return {
        name,
        type,
        arrayLevel,
    };
}

// ============================================================
// Exports
export {
    convertToType,
    fromEntries,
    getResolvedType,
    getTypeInfo,
    isPromise,
};
