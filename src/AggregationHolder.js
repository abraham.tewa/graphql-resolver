// ============================================================
// Import packages
import stringify from 'fast-json-stable-stringify';

// ============================================================
// Import modules
import { gqlResolverData } from './constants';
import { isPromise } from './helpers';

// ============================================================
// Class
class AggregationHolder {
    static #aggregations = new WeakMap();

    /**
     * Function that will be called once the aggregator is closed.
     * The goal is to free the aggregator holder slot.
     * @type {function}
     */
    #cleanup;

    /**
     * Indicate if the aggregation holder is opened or not.
     * @type {boolean}
     */
    #closed = false;

    /**
     * Aggregation group data.
     * All items that have the same group data are grouped togehter.
     * @example
     *
     *  {
     *      path: 'user.account.profile',
     *      properties: {
     *          hobbies: ['tennis'],
     *      }
     *  }
     *
     * @type {Object}
     */
    #groupData;

    /**
     * List of fields that are aggregated together.
     * @type {Array.<instance, args>}
     */
    #items;

    /**
     * Maximum number of items that can handle the aggregation holder.
     * Once this number is reached, the holder will close and call the resolver.
     * @type {Number}
     */
    #maxItems;

    /**
     * Maximum duration to wait before closing the aggregator.
     * @type {Number}
     */
    #maxWait;

    /**
     * Resolver that is aggregated.
     * @type {GraphqlFieldResolver}
     */
    #fieldResolver

    /**
     * Promise resolved once the aggregation is finished.
     * The return elements are a list of value resolved.
     * @type {Promise.<Array.<Promise|*>>}
     */
    #promise;

    #resolve;

    /**
     * @type {Class}
     */
    #TypeResolver

    /**
     *
     * @param {Class}                   TypeResolver
     * @param {GraphqlFieldResolver} fieldResolver - Resolver that is aggregated
     * @param {Object}                  group            - Aggregation group data
     * @param {function}                cleanup          - Function that will be called once the aggregation start.
     */
    constructor(TypeResolver, fieldResolver, group, cleanup) {
        this.#fieldResolver = fieldResolver;

        this.#cleanup = cleanup;
        this.#groupData = group;
        this.#items = [];
        this.#maxItems = this.#fieldResolver.aggregation.limits.maxItems;
        this.#maxWait = this.#fieldResolver.aggregation.limits.maxWait;
        this.#TypeResolver = TypeResolver;

        this.#promise = new Promise((resolve) => {
            this.#resolve = resolve;
        });
    }

    /**
     * Add a new item to aggregator
     * @param {Object}       instance
     * @param {Array}        args
     * @param {QueryContext} context
     * @return {*|Promise.<*>} - The resolved value for this instance and argument
     */
    add(instance, args, context) {
        if (this.isClosed()) {
            throw new Error('Aggregation holder closed');
        }

        this.#items.push({ instance, args, context });

        if (this.#items.length >= this.#maxItems) {
            this.start();
        }

        return this.#promise.then(after.bind(undefined, this.#items.length - 1));
    }

    /**
     * Return the list of items
     * @returns {Array}
     */
    getItems() {
        return [...this.#items];
    }

    initialize() {
        if (this.isClosed()) {
            return;
        }

        if (this.#maxWait) {
            setTimeout(
                () => this.start(),
                this.#maxWait,
            );

            return;
        }

        this.start();
    }

    /**
     * Close the holder: adding new items will not be possible.
     * @returns {boolean}
     */
    isClosed() {
        return this.#closed;
    }

    /**
     * Start the aggregation resolver.
     */
    start() {
        if (this.isClosed()) {
            return;
        }

        this.#closed = true;
        this.#cleanup();

        const ResolverClass = this.#TypeResolver.getResolverClass();

        const items = this.#items.map(({ instance, context, args }) => {
            const resolverData = instance[gqlResolverData]
                || new ResolverClass(context, instance)[gqlResolverData];

            return {
                instance: resolverData.instance,
                context: context.publicContext,
                args,
            };
        });

        const value = this.#fieldResolver.apply(
            this.#TypeResolver.getResolverClass(),
            [
                items,
                this.#groupData,
            ],
        );

        this.#resolve(value);
    }

    then(...args) {
        return this.#promise.then(...args);
    }

    /**
     *
     * @param {Schema|QueryContext}  root
     * @param {GraphqlTypeResolver}  typeResolver
     * @param {GraphqlFieldResolver} fieldResolver
     * @private
     */
    static getCache(root, typeResolver, fieldResolver) {
        let aggregationCache = this.#aggregations.get(root);

        if (!aggregationCache) {
            aggregationCache = new Map();
            this.#aggregations.set(root, aggregationCache);
        }

        // Grouping by type
        let typeCache = aggregationCache.get(typeResolver);
        if (!typeCache) {
            typeCache = new Map();
            aggregationCache.set(typeResolver, typeCache);
        }

        // Grouping by resolver
        let resolverCache = typeCache.get(fieldResolver);
        if (!resolverCache) {
            resolverCache = {};
            typeCache.set(fieldResolver, resolverCache);
        }

        return resolverCache;
    }

    /**
     * Search or create the AggregationHolder that feat the parameters.
     * @param {Schema|QueryContext}  root
     * @param {GraphqlTypeResolver}  typeResolver
     * @param {GraphqlFieldResolver} fieldResolver
     * @param {AggregationGroupping} group
     * @returns {AggregationHolder}
     * @private
     */
    static getHolder(root, typeResolver, fieldResolver, group) {
        const groupHash = stringify(group);

        const cache = this.getCache(root, typeResolver, fieldResolver);

        if (!cache[groupHash]) {
            const holder = new AggregationHolder(
                typeResolver,
                fieldResolver,
                group,
                () => {
                    delete cache[groupHash];
                },
            );

            holder.initialize();

            cache[groupHash] = holder;
        }

        return cache[groupHash];
    }

    /**
     *
     * @param {Schema|QueryContext}  root
     * @param {Object}               group
     * @param {string}               group.path
     * @param {GraphqlTypeResolver}  group.type
     * @param {GraphqlFieldResolver} group.resolver
     * @param {Object.<Promise|*>}   group.properties
     * @param {Promise|*}            group.aggregator
     */
    static async resolve(
        root,
        {
            type: typeResolver,
            resolver: fieldResolver,
            ...groupData
        },
        instance,
        args,
        queryContext,
    ) {
        const groupOrPromise = toGroup(groupData);

        if (isPromise(groupOrPromise)) {
            return groupOrPromise.then((group) => {
                const holder = this.getHolder(root, typeResolver, fieldResolver, group);
                return holder.add(instance, args);
            });
        }

        const holder = this.getHolder(root, typeResolver, fieldResolver, groupOrPromise);
        return holder.add(instance, args, queryContext);
    }
}

/**
 * @typedef {Object} AggregationGroupping
 * @property {Object.<Promise|*>} properties
 * @property {Promise|Object}     aggregator
 */

/**
  *
  * @param {AggregationGroupping} groupData
  * @param {*} info
  * @returns {AggregationGroup}
  */
function toGroup(groupData) {
    const group = {};

    if (groupData.path) {
        group.path = groupData.path;
    }

    const waitPromise = [];

    // Grouping by properties
    // If one or several properties are promise,
    // we have to wait their resolution to continue grouping
    if (groupData.properties) {
        const { entries, promises } = Object
            .entries(groupData.properties)
            .reduce(
                (acc, [name, value]) => {
                    if (isPromise(value)) {
                        acc.promises.push(
                            value.then((resolvedValue) => [name, resolvedValue]),
                        );
                    }
                    else {
                        acc.entries.push([name, value]);
                    }

                    return acc;
                },
                {
                    entries: [],
                    promises: [],
                },
            );

        if (promises.length) {
            const promise = Promise
                .all(promises)
                .then((resolvedEntries) => {
                    entries.push(...resolvedEntries);
                    group.properties = Object.fromEntries(entries);
                });

            waitPromise.push(promise);
        }
        else {
            group.properties = Object.fromEntries(entries);
        }
    }

    // Grouping by aggregator results
    if (groupData.aggregator) {
        if (isPromise(groupData.aggregator)) {
            const promise = groupData.aggregator.then((value) => {
                group.aggregator = value;
            });
            waitPromise.push(promise);
        }
        else {
            group.aggregator = groupData.aggregator;
        }
    }

    if (waitPromise.length) {
        return Promise.all(waitPromise).then(() => group);
    }

    return group;
}

async function after(index, list) {
    return list[index];
}

// ============================================================
// Exports
export default AggregationHolder;
export {
    toGroup,
};
