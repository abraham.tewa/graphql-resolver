// ============================================================
// Import modules
import GraphqlError from './GraphqlError';

// ============================================================
// Class
class SchemaNotFoundError extends GraphqlError {
    constructor(schemaRef) {
        const name = typeof schemaRef === 'symbol'
            ? schemaRef.description
            : schemaRef;

        super(`Schema not found: "${name}"`);
        this.schemaRef = schemaRef;
    }
}

// ============================================================
// Exports
export default SchemaNotFoundError;
