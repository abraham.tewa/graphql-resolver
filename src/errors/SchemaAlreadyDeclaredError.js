// ============================================================
// Import modules
import GraphqlError from './GraphqlError';


// ============================================================
// Error
class SchemaAlreadyDeclaredError extends GraphqlError {
    constructor(schemaRef) {
        const name = typeof schemaRef === 'symbol'
            ? schemaRef.description
            : schemaRef;

        super(`Schema already declared: "${name}"`);
        this.schemaRef = schemaRef;
    }
}

// ============================================================
// Exports
export default SchemaAlreadyDeclaredError;
