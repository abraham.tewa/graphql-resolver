class GraphqlError extends Error {}

// ============================================================
// Exports
export default GraphqlError;
