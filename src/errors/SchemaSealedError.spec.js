/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert, expect } from 'chai';

// ============================================================
// Import modules
import SchemaSealedError from './SchemaSealedError';

// ============================================================
// Tests
describe('SchemaSealedError suite', () => {
    it('accept anything as reference', () => {
        const ref = {};
        const error = new SchemaSealedError(ref);

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', ref);
    });

    it('accept string as reference', () => {
        const ref = 'some string reference';
        const error = new SchemaSealedError(ref);

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', ref);
    });

    it('accept Symbol as reference', () => {
        const description = 'some symbol reference';
        const ref = Symbol(description);
        const error = new SchemaSealedError(ref);

        assert.strictEqual(error.message, `Schema "${description}" is sealed`);

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', ref);
    });
});
