/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert, expect } from 'chai';

// ============================================================
// Import modules
import SchemaAlreadyDeclaredError from './SchemaAlreadyDeclaredError';

// ============================================================
// Tests
describe('SchemaAlreadyDeclaredError suite', () => {
    it('accept anything as reference', () => {
        const ref = {};
        const error = new SchemaAlreadyDeclaredError(ref);

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', ref);
    });

    it('accept string as reference', () => {
        const ref = 'some string reference';
        const error = new SchemaAlreadyDeclaredError(ref);

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', ref);
    });

    it('accept Symbol as reference', () => {
        const description = 'some symbol reference';
        const ref = Symbol(description);
        const error = new SchemaAlreadyDeclaredError(ref);

        assert.strictEqual(error.message, `Schema already declared: "${description}"`);

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', ref);
    });
});
