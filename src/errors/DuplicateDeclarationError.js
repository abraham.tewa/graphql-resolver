import GraphqlError from './GraphqlError';

// ============================================================
// Error

class DuplicateDeclarationError extends GraphqlError {
    /**
     * @param {string[]} names
     * @hideconstructor
     */
    constructor(names) {
        const message = `Duplicate resolver declaration for fields: "${names.join(', ')}"`;
        super(message);
        this.names = [...names];
    }
}

// ============================================================
// Exports
export default DuplicateDeclarationError;
