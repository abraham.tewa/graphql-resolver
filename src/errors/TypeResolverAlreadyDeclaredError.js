// ============================================================
// Import modules
import GraphqlError from './GraphqlError';

// ============================================================
// Error
class TypeResolverAlreadyDeclaredError extends GraphqlError {
    /**
     *
     * @param {string}       name         - Name of the already declared resolver
     * @param {Schema}       schema       - Schema in which the resolver was declared
     * @param {TypeResolver} TypeResolver - Resolver declared
     * @hideconstructor
     */
    constructor({
        name,
        schema,
        TypeResolver,
    }) {
        const schemaRef = schema.getRef();
        const schemaName = typeof schemaRef === 'symbol'
            ? schemaRef.description
            : schemaRef;

        const message = `Resolver already declared in schema.\nType: ${name}\nSchema: ${schemaName}`;
        super(message);
        this.TypeResolver = TypeResolver;
        this.typeName = name;
        this.schemaRef = schema.getRef();
    }
}

// ============================================================
// Exports
export default TypeResolverAlreadyDeclaredError;
