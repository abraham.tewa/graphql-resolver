/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert, expect } from 'chai';

// ============================================================
// Import modules
import SchemaNotFoundError from './SchemaNotFoundError';

// ============================================================
// Tests
describe('SchemaNotFoundError suite', () => {
    it('accept anything as reference', () => {
        const ref = {};
        const error = new SchemaNotFoundError(ref);

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', ref);
    });

    it('accept string as reference', () => {
        const ref = 'some string reference';
        const error = new SchemaNotFoundError(ref);

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', ref);
    });

    it('accept Symbol as reference', () => {
        const description = 'some symbol reference';
        const ref = Symbol(description);
        const error = new SchemaNotFoundError(ref);

        assert.strictEqual(error.message, `Schema not found: "${description}"`);

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', ref);
    });
});
