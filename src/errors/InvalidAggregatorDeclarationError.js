// ============================================================
// Import modules
import GraphqlError from './GraphqlError';


// ============================================================
// Error
class InvalidAggregatorDeclarationError extends GraphqlError {
    constructor(name, errorMessage) {
        const message = `Invalid aggregator declaration for field "${name}": ${errorMessage}`;
        super(message);
        this.name = name;
    }
}

// ============================================================
// Exports
export default InvalidAggregatorDeclarationError;
