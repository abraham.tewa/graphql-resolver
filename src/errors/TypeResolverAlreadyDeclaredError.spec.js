/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert, expect } from 'chai';

// ============================================================
// Import modules
import Schema from '../Schema';
import { Type } from '../public';
import TypeResolverAlreadyDeclaredError from './TypeResolverAlreadyDeclaredError';

// ============================================================
// Tests
describe('TypeResolverAlreadyDeclaredError suite', () => {
    const refAny = {};
    const refSymbol = Symbol('some symbol ref');
    const refString = 'some string ref';
    let TypeResolver;
    let schemaAny;
    let schemaString;
    let schemaSymbol;

    before(() => {
        schemaAny = Schema.declare(refAny);
        schemaString = Schema.declare(refString);
        schemaSymbol = Schema.declare(refSymbol);

        TypeResolver = Type(class {});
    });

    after(() => {
        Schema.forgot(refAny);
        Schema.forgot(refString);
        Schema.forgot(refSymbol);
    });

    it('accept anything as reference', () => {
        const name = 'some name';

        const error = new TypeResolverAlreadyDeclaredError({
            name,
            schema: schemaAny,
            TypeResolver,
        });

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', refAny, 'schemaRef');
        assert.propertyVal(error, 'typeName', name, 'name');
        assert.propertyVal(error, 'TypeResolver', TypeResolver, 'TypeResolver');
    });

    it('accept string as reference', () => {
        const name = 'some name';

        const error = new TypeResolverAlreadyDeclaredError({
            name,
            TypeResolver,
            schema: schemaString,
        });

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', refString, 'schemaRef');
        assert.propertyVal(error, 'typeName', name, 'name');
        assert.propertyVal(error, 'TypeResolver', TypeResolver, 'TypeResolver');
    });

    it('accept Symbol as reference', () => {
        const name = 'some name';

        const error = new TypeResolverAlreadyDeclaredError({
            name,
            TypeResolver,
            schema: schemaSymbol,
        });

        expect(error).to.matchSnapshot();

        assert.propertyVal(error, 'schemaRef', refSymbol, 'schemaRef');
        assert.propertyVal(error, 'typeName', name, 'name');
        assert.propertyVal(error, 'TypeResolver', TypeResolver, 'TypeResolver');
    });
});
