// ============================================================
// Import modules
import GraphqlError from './GraphqlError';

// ============================================================
// Error
class SchemaSealed extends GraphqlError {
    constructor(schemaRef) {
        const name = typeof schemaRef === 'symbol'
            ? schemaRef.description
            : schemaRef;

        super(`Schema "${name}" is sealed`);
        this.schemaRef = schemaRef;
    }
}

// ============================================================
// Exports
export default SchemaSealed;
