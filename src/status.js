// ============================================================
// Module's constants and variables
const status = {};
status.compilationInProgress = false;

const schemaCurrentlyCompiled = new Set();

// ============================================================
// Functions
function onCompilationStart(schema) {
    schemaCurrentlyCompiled.add(schema);
    status.compilationInProgress = true;
}

function onCompilationEnd(schema) {
    schemaCurrentlyCompiled.delete(schema);
    status.compilationInProgress = Boolean(schemaCurrentlyCompiled.size);
}

// ============================================================
// Exports
export default status;
export {
    onCompilationEnd,
    onCompilationStart,
};
