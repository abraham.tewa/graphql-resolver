```javascript
import {
    G, GraphqlType, GraphqlFieldResolver, GraphqlAggregator, GraphqlGlobalAggregator,
} from 'graphql-resolver';

// ============================================================
// Documentation

/**
 * @callback GraphqlType
 * @param {Object} options
 * @param {string} [options.name] - Name of the resolved type
 */

/**
 * @decorator {GraphqlResolverCallback} GraphqlFieldResolver
 * @param {boolean|string|string[]} - resolvedProperty
 */

/**
 * @callback GraphqlResolverCallback
 * @param {Object}                 - params
 * @param {GraphqlResolverHandler} - handler
 */

/**
 * @typedef {Object} GraphqlResolverHandler
 * @property {GraphqlContext} context
 * @property {GraphqlInfo}    info
 */

// ============================================================
// Class
class User {
    constructor(queryContext) {
        this.currentUserRights = queryContext.currentUserRights;
        this.queryContext = queryContext;
    }

    name = Resolver(
        ['displayName', 'birthDay'],
        async ({
          displayName: args,
          birthDay: args,
        }) => {
            const user = await this.fetch();
            return `${user.firstName} ${user.lastName}`;
        },
    );

    nbBooks = Resolver(
        async ({ type }) => {
            const books = await this.books({ type });
            return books.length;
        },
    );

    fetch = GraphqlFieldResolver(
        '*',
        async () => {
          const id = this.gql;

          const user = await this.gqlResolve('user');

          const user = await fetch(`/user/${this.gql.id}`);
          user.siblings = user.siblings.map((id) => User.getFromId(id, this));
          return user;
        },
    );

    setId = GraphqlFieldResolver(
        false,
        (id) => {
            this.#id = id;
        },
    );

    registeredFrom = GraphqlFieldResolver(
        async (params, handler) => {
            const libraryRegistration = await this[G].resolve('libraryRegistration');

            if (!libraryRegistration) {
                return undefined;
            }

            return Date.now() - libraryRegistration.date;
        },
    );

    // ============================================================
    // Static methods

    static constants(object, args, queryContext) {
      return {
        PI: Math.PI,
        EULER: 0.5772156649,
        PLANK: 6.62607015,
      },
    }

    /**
     * Aggregators cannot resolved properties
     * 
     * About "options.resolve" parameter:
     *  If not defined, then 
     *
     * @callback GraphqlAggregator
     * @param {Object}   options
     * @param {GraphqlAggregatorGroupByOptions} options.groupBy                - Options about how to group instances
     * @param {Object}                          options.aggregation
     * @param {number}                          [options.aggregation.maxItems] - Maximum number of items by batch
     * @param {number}                          [options.aggregation.maxWait]  - Number of milliseconds before running the aggregator.
     * @param {string[]|string}                 [options.resolve]              - List of fields resolved by the aggregation.
     *                                                                If a list of strings, then an object is expected as result,
     *                                                                with each properties matching the strings.
     * @returns GraphqlAggregatorCallback
     */

    /**
     * 
     * @callback GraphqlAggregatorCallback
     * @param {Array.<instance, ...arguments>} groups
     * @returns {Array}
     */

    /**
     * @typedef {Objec} GraphqlAggregatorGroupByOptions
     * @param {boolean}               [context=true] - Group by property to resolve by context
     * @param {boolean}               [path=false]   - Group by properties to resolve by their path from the root of the query
     * @param {string[]}              [properties]   - Instances will be grouped by properties which has the same values
     * @param {function(instance): *} [aggregator]
     * 
     */

    static books = GraphqlFieldAggregator(
        async (items, groups) => {
            groups.map(([
              instance,
              { fields: args },
              group,
            ],
            ) => {
                if (type === 'owner' && handler.context.currentUserId !== instance.#id) {
                    return {
                        error: 'Cannot get books owned by another user',
                    };
                }

                return { id: instance.id };
            });

            const response = await fetch(`/user/${this.#id}/books?type=${type}`);
            return response.json();
        },
    )

    static libraryRegistration = GraphqlAggregator({
        groupBy: {
            query: true,
            path: true,
            properties: ['registered'],
            aggregator: ({ a }) => a.toString(),
        },
        aggregation: {
            maxWait: 100,
            maxItems: 100,
        },
        resolve: ['libraryRegistration', 'library'],
    },
    async (listArguments, group) => {
        if (!group.properties.registered) {
          return false;
        }

        // Listing all user ids
        const listId = listArguments.map((
            instance,
            resolve: [
              'libraryRegistration',
              'library',
            ]
        ) => {
            if (!instance.registered) {
                return undefined;
            }

            return instance.id;
        }).filter((x) => x);

        const response = await fetch(`/library/user?user=${listId.join(',')}`);
        const users = await response.json();

        const userMap = users.reduce((acc, user) => {
            acc[user.id] = user;
            return acc;
        }, {});

        return {
            values: listArguments.map(([{ id }]) => userMap[id]),
            errors: [],
        };
    });

    static async getFromId(id, initiator) {
        const user = GraphqlType.create(User, initiator);
        user.setId(id);
        return user;
    }
}

/**
 * @typedef {Array.<GraphqlTypeInstance, params: Object, GraphqlInfo, GraphqlContext>} GraphqlResolverArguments
 */

export default GraphqlType(
  User, {
    name: 'User',
    conflictResolver: {
      fieldA: 'fieldB',
      fieldB: {static: true, property: 'A'}
    }
  },
);
```
