# Example

```javascript
import { GraphqlType, GraphqlFieldResolver, GraphqlAggregator } from 'graphql-resolver';

// ============================================================
// Documentation

/**
 * @callback GraphqlType
 * @param {Object} options
 * @param {string} [options.name] - Name of the resolved type
 */

/**
 * @decorator {GraphqlResolverCallback} GraphqlFieldResolver
 * @param {boolean|string|string[]} - resolvedProperty
 */

/**
 * @callback GraphqlResolverCallback
 * @param {Object}                 - params
 * @param {GraphqlResolverHandler} - handler
 */

/**
 * @typedef {Object} GraphqlResolverHandler
 * @property {GraphqlContext} context
 * @property {GraphqlInfo}    info
 */

/**
 * Aggregators cannot resolved properties
 * 
 * @callback GraphqlAggregator
 * @param {Object}   options
 * @param {Object}   options.groupBy            - Options about how to group instances
 * @param {boolean}  options.groupBy.context    - Group by property to resolve by context
 * @param {boolean}  options.groupBy.path       - Group by properties to resolve by their path from the root of the query
 * @param {string[]}              options.groupBy.properties - Instances will be grouped by properties which has the same values
 * @param {function(instance): *} options.groupBy.callback
 * @param {Object}                options.aggregation
 * @param {number}               [options.aggregation.maxItems] - Maximum number of items by batch
 * @param {number}               [options.aggregation.maxWait]  - Number of milliseconds before running the aggregator.
 */

// ============================================================
// Class

@GraphqlType({
  name: 'UserQuery',
})
class User {
    constructor(queryContext) {
        this.currentUserRights = queryContext.currentUserRights;
        this.queryContext = queryContext;
    }

    name = GraphqlFieldResolver(
      'displayName',
      (params, handler) => {
        const user = await this.fetch();
        return `${user.firstName} ${user.lastName}`;
      }
    );

    nbBooks = GraphqlFieldResolver(
      async ({ type }) => {
        const books = await this.books({ type });
        return books.length;
      }
    );

    fetch = GraphqlFieldResolver(
      ['birthDay', 'address'],
      async () => {
        const user = await fetch(`/user/${this.#id}`);
        user.siblings = user.siblings.map(id => User.getFromId(id, this));
        return user;
      },
    );

    setId = GraphqlFieldResolver(
      false,
      (id) => {
        this.#id = id;
      }
    );

    getId() {
      return this.#id;
    }

    registeredFrom = async (params, handler) {
      const libraryRegistration = await handler.resolve('libraryRegistration');

      if (!libraryRegistraiton) {
        return undefined;
      }

      return Date.now() - libraryRegistration.date;
    }

    // ============================================================
    // Static methods

    static books = Aggregator(
      ['books'],
      async (groupArguments) => {
        const errors = [];

        groupArguments.map(([instance, { type }, handler ]) => {
          if (type === 'owner' && handler.context.currentUserId !== instance.#id) {
            return {
              error: 'Cannot get books owned by another user';
            };
          }

          return { id: instance.id };
        });

        const response = await fetch(`/user/${this.#id}/books?type=${type}`);
        return response.json();
      },
    )

    static libraryRegistration : GlobalAggregator({
        groupBy : {
          context: true,
          path: true,
          properties: ['registered'],
          aggregator: ({ a, b }) => a + b,
        },
        aggregation: {
          maxWait: 100,
          maxItems: 100,
        },
        resolve: ['libraryRegistration', 'library'],
      },
      async (listArguments) => {
        // Listing all user ids
        const listId = listArguments.map(({
          instance,
          {
            libraryRegistration: [params, handler]
          }
        }) => {
          if (!instance.registered) {
            return undefined;
          }

          return instance.id;
        }).filter(x => x);

        const response = await fetch(`/library/user?user=${listId.join(',')}`);
        const users = await response.json();

        const userMap = users.reduce(acc, user) => {
          acc[user.id] = user;
          return acc;
        }, {});

        return {
          values: listArguments.map(([{ id }]) => {
            return userMap[id];
          }),
          errors: [],
        };
      }

    static async getFromId(id, initiator) {
      const user = GraphqlType.create(User, initiator);
      user.setId(id);
      return user;
    }
}

/**
 * @typedef {Array.<GraphqlTypeInstance, params: Object, GraphqlInfo, GraphqlContext>} GraphqlResolverArguments
 */

export default GraphqlType(User, {
  ignore: ['getId']
});

```
