// ============================================================
// Class
class GraphqlTypeHelper {
    /**
     * Object currently resolved
     * @type {Object}
     */
    #object;

    /**
     * Query context
     * @type {GraphqlQueryContext}
     */
    #queryContext;

    /**
     * Type resolver associated to the helper
     * @type {GraphqlTypeResolver}
     */
    #typeResolver;

    /**
     *
     * @param {GraphqlTypeResolver} typeResolver
     * @param {*} queryContext
     * @param {*} object
     */
    constructor(typeResolver, queryContext, object) {
        this.#typeResolver = typeResolver;
        this.#queryContext = queryContext;
        this.#object = object;

        Object.defineProperty(
            this,
            'object',
            {
                value: object,
                enumerable: true,
                writable: false,
                configurable: false,
            },
        );
    }

    set(field, value) {
        this.#object[field] = value;
    }

    /**
     * Return the context of the graphql query
     * @returns {QueryContext}
     * @public
     */
    get queryContext() {
        return this.#queryContext;
    }

    /**
     *
     * @param {string|string[]} field - Field or list of fields to resolve
     * @return {*}
     * @public
     */
    resolve(field, args = []) {
        if (!Array.isArray(args)) {
            throw new TypeError('args is not an array');
        }

        return this.#typeResolver.resolve(field, this.#object, args, this.#queryContext);
    }
}

// ============================================================
// Exports
export default GraphqlTypeHelper;
