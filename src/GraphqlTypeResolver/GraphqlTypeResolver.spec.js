/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */
// ============================================================
// Import packages
import { assert } from 'chai';

// ============================================================
// Import modules

import { Field } from '../public';
import { G } from '../constants';
import GraphqlTypeResolver from './GraphqlTypeResolver';

import GraphqlTypeHelper from '../GraphqlTypeHelper';

// ============================================================
// Tests
describe('GraphqlTypeResolver suite', () => {
    describe('.constructor', () => {
        it('accept class without methods', () => {
            assert.doesNotThrow(
                () => new GraphqlTypeResolver(class {}, {}),
            );
        });

        it('options are not mandatory', () => {
            assert.doesNotThrow(
                () => new GraphqlTypeResolver(class {}),
            );
        });
    });

    describe('.getApolloResolver()', () => {
        it('provide an empty resolver if class has no fields/methods', () => {
            const graphqlTypeResolver = new GraphqlTypeResolver(class {}, {});
            const apolloResolver = graphqlTypeResolver.getApolloResolver();

            assert.isObject(apolloResolver);
            assert.isEmpty(apolloResolver);
        });

        it('transform a class into an apollo resolver', () => {
            const GP = 3;
            const staticGP = 5;
            const P = 7;
            const staticP = 11;
            const C = 13;
            const staticC = 17;

            const methodValue = 2;
            const fieldValue = 4;
            const staticValue = 6;
            const staticFieldValue = 8;
            const overridedByChild = 10;
            const overridedByParent = 12;
            const staticOverridedByChild = 14;

            const GrandParent = class {
                methodGP() {
                    return this.GP * methodValue;
                }

                fieldGP = Field({
                    resolve: 'fieldGP',
                    resolver: function fieldGP() {
                        return this.GP * fieldValue;
                    },
                })

                static staticGP() {
                    return this.GP * staticValue;
                }

                static staticFieldGP = Field({
                    resolve: 'staticFieldGP',
                    resolver: function staticFieldGP() {
                        return this.GP * staticFieldValue;
                    },
                })

                overridedByChild() {
                    return this.GP * overridedByChild;
                }

                overridedByParent() {
                    return this.GP * overridedByParent;
                }

                static staticallyOverridedByChild() {
                    return this.GP * staticOverridedByChild;
                }

                GP = GP;

                static GP = staticGP;
            };

            const Parent = class extends GrandParent {
                methodP() {
                    return this.P * methodValue;
                }

                fieldP = Field({
                    resolve: 'fieldP',
                    resolver: function fieldP() {
                        return this.P * fieldValue;
                    },
                })

                overridedByParent() {
                    return this.P * overridedByParent;
                }

                static staticP() {
                    return this.P * staticValue;
                }

                static staticFieldP = Field({
                    resolve: 'staticFieldP',
                    resolver: function staticFieldP() {
                        return this.P * staticFieldValue;
                    },
                })

                P = P;

                static P = staticP;
            };

            const Child = class extends Parent {
                methodC() {
                    return this.C * methodValue;
                }

                fieldC = Field({
                    resolve: 'fieldC',
                    resolver: function fieldC() {
                        return this.C * fieldValue;
                    },
                })

                static staticC() {
                    return this.C * 6;
                }

                static staticFieldC = Field({
                    resolve: 'staticFieldC',
                    resolver: function staticFieldC() {
                        return this.C * staticFieldValue;
                    },
                })

                overridedByChild() {
                    return this.C * overridedByChild;
                }

                static staticallyOverridedByChild() {
                    return this.C * staticOverridedByChild;
                }

                C = C;

                static C = staticC;
            };

            const graphqlTypeResolver = new GraphqlTypeResolver(
                Child,
                {},
            );

            const ar = graphqlTypeResolver.getApolloResolver();

            assert.hasAllKeys(
                ar,
                [
                    'methodGP', 'fieldGP', 'methodP', 'fieldP', 'methodC', 'fieldC',
                    'overridedByChild', 'overridedByParent',
                    'staticGP', 'staticFieldGP', 'staticP', 'staticFieldP', 'staticC', 'staticFieldC',
                    'staticallyOverridedByChild',
                ],
            );

            assert.strictEqual(ar.methodGP({}, [], {}), GP * methodValue, 'methodGP');
            assert.strictEqual(ar.fieldGP({}, [], {}), GP * fieldValue, 'fieldGP');

            assert.strictEqual(ar.methodP({}, [], {}), P * methodValue, 'methodP');
            assert.strictEqual(ar.fieldP({}, [], {}), P * fieldValue, 'fieldP');

            assert.strictEqual(ar.methodC({}, [], {}), C * methodValue, 'methodC');
            assert.strictEqual(ar.fieldC({}, [], {}), C * fieldValue, 'fieldC');

            assert.strictEqual(ar.overridedByChild({}, [], {}), C * overridedByChild, 'overridedByChild');
            assert.strictEqual(ar.overridedByParent({}, [], {}), P * overridedByParent, 'overridedByParent');

            assert.strictEqual(ar.staticGP({}, [], {}), staticGP * staticValue, 'staticGP');
            assert.strictEqual(ar.staticFieldGP({}, [], {}), staticGP * staticFieldValue, 'staticFieldGP');

            assert.strictEqual(ar.staticP({}, [], {}), staticP * staticValue, 'staticP');
            assert.strictEqual(ar.staticFieldP({}, [], {}), staticP * staticFieldValue, 'statifFieldP');

            assert.strictEqual(ar.staticC({}, [], {}), staticC * staticValue, 'staticC');
            assert.strictEqual(ar.staticFieldC({}, [], {}), staticC * staticFieldValue, 'staticFieldC');

            assert.strictEqual(
                ar.staticallyOverridedByChild({}, [], {}),
                staticC * staticOverridedByChild,
                'staticallyOverridedByChild',
            );
        });

        it('use a cache', () => {
            const graphqlTypeResolver = new GraphqlTypeResolver(class {}, {});
            const apolloResolver = graphqlTypeResolver.getApolloResolver();

            const newValue = graphqlTypeResolver.getApolloResolver();

            assert.strictEqual(newValue, apolloResolver);
        });
    });

    describe('.getResolverClass()', () => {
        it('create a new inherited class', () => {
            const InitialClass = class {};
            const graphqlTypeResolver = new GraphqlTypeResolver(InitialClass, {});

            const ResolverClass = graphqlTypeResolver.getResolverClass();

            assert.isFunction(ResolverClass);
            assert.notStrictEqual(ResolverClass, InitialClass);
            assert.instanceOf(ResolverClass.prototype, InitialClass);
        });

        it('create a new field with G as property', () => {
            const graphqlTypeResolver = new GraphqlTypeResolver(class {}, {});

            const ResolverClass = graphqlTypeResolver.getResolverClass();

            const instance = new ResolverClass();

            assert.property(instance, G);
            assert.instanceOf(instance[G], GraphqlTypeHelper);
        });

        it('use cache', () => {
            const graphqlTypeResolver = new GraphqlTypeResolver(class {}, {});

            const ResolverClass = graphqlTypeResolver.getResolverClass();
            const NewResolverClass = graphqlTypeResolver.getResolverClass();

            assert.strictEqual(NewResolverClass, ResolverClass);
        });
    });

    describe('.resolve()', () => {
        it('resolve instance level field', () => {
            const value = {};
            const argsValue = {};
            const queryContextValue = {};

            const Class = class {
                fieldA(args) {
                    return {
                        queryContext: this[G].queryContext,
                        value: this.value,
                        args,
                    };
                }

                value = value;
            };

            const graphqlTypeResolver = new GraphqlTypeResolver(Class, {});
            graphqlTypeResolver.getApolloResolver();

            const resolvedValue = graphqlTypeResolver.resolve('fieldA', {}, [argsValue], queryContextValue);

            assert.strictEqual(resolvedValue.args, argsValue, 'args');
            assert.strictEqual(resolvedValue.queryContext, queryContextValue, 'queryContext');
            assert.strictEqual(resolvedValue.value, value, 'value');
        });
    });
});
