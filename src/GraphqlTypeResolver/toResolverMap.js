// ============================================================
// Import packages
import _ from 'lodash';

// ============================================================
// Import modules
import {
    FieldSourceType,
    gqlPropertyResolver,
} from '../constants';
import {
    DuplicateDeclarationError,
    InvalidAggregatorDeclarationError,
} from '../errors';

import { fromEntries } from '../helpers';

import { toFieldResolver } from '../public';

const RE_EXCLUDED_FIELDS = /^(get|set|is|has)[A-Z].*/;

// ============================================================
// Functions

/**
 *
 * @param {Object|Class} source
 * @param {string[]}     ignoredProperties
 * @param {Object}       conflictResolutionMap
 * @returns {Object.<GraphqlFieldResolver>}
 */
function toResolverMap(source, ignoredProperties, conflictResolutionMap) {
    if (typeof source === 'function') {
        return classToResolverMap(source, ignoredProperties, conflictResolutionMap);
    }

    return objectToResolverMap(source, ignoredProperties);
}

/**
 * Transform an object to a resolver map
 * @param {Object}   object
 * @param {string[]} ignoredProperties
 * @throws {DuplicateDeclarationError}
 * @returns {Object.<GraphqlFieldResolver>}
 */
function objectToResolverMap(object, ignoredProperties) {
    const ignoredInstanceFields = [...ignoredProperties, 'constructor'];

    const descriptors = Object.getOwnPropertyDescriptors(object);

    const map = descriptorsToMap(descriptors, ignoredInstanceFields, true);

    // Searching for duplicate fields
    const duplicateFields = _.intersection(Object.keys(map.gql));

    if (duplicateFields.length) {
        throw new DuplicateDeclarationError(duplicateFields);
    }

    return map.gql;
}

/**
 * @param {GraphqlTypeOptions} RawResolverClass
 * @param {string[]}           ignoredProperties
 * @returns {Object.<GraphqlFieldResolver>}
 * @throws {DuplicateDeclarationError}
 * @throws {InvalidAggregatorDeclarationError}
 */
function classToResolverMap(Class, ignoredProperties, conflictResolutionMap) {
    const instance = new Class();

    // ==============================
    // Static fields
    const staticMap = getStaticInfo(Class, ignoredProperties);

    // ==============================
    // Instance fields
    const ignoredInstanceFields = [...ignoredProperties, 'constructor'];

    const prototypeDescriptors = getPrototypeDescriptors(Class, ignoredProperties);
    const attributeDescriptors = Object.getOwnPropertyDescriptors(instance);

    Object.values(prototypeDescriptors).forEach((descriptor) => {
        // eslint-disable-next-line no-param-reassign
        descriptor.source = FieldSourceType.prototype;
    });

    Object.values(attributeDescriptors).forEach((descriptor) => {
        // eslint-disable-next-line no-param-reassign
        descriptor.source = FieldSourceType.attribute;
    });

    const instanceMap = descriptorsToMap(
        {
            ...prototypeDescriptors,
            ...attributeDescriptors,
        },
        ignoredInstanceFields,
        false,
    );

    // ==============================
    // Checking and resolving duplications
    Object
        .entries(conflictResolutionMap)
        .forEach(([field, { static: isStatic, resolvedBy }]) => {
            const resolver = isStatic
                ? staticMap.object[resolvedBy]
                : instanceMap.object[resolvedBy];

            if (!resolver) {
                throw new Error(`Property "${resolvedBy}" declared in conflict resolution map was not found`);
            }

            if (!resolver.getResolvedFields().includes(field)) {
                throw new Error(`Field "${field}" is declared as resolved by "${resolvedBy}" but`
                + 'is not present in the resolved field list of the resolver');
            }

            const toDeleteMap = isStatic
                ? instanceMap
                : staticMap;

            const correctMap = isStatic
                ? staticMap
                : instanceMap;

            correctMap.gql[field] = resolver;

            delete toDeleteMap.gql[field];

            delete staticMap.duplications[field];
            delete instanceMap.duplications[field];
        });

    // Searching for duplicate fields
    let duplicateFields = _.intersection(
        Object.keys(staticMap.gql),
        Object.keys(instanceMap.gql),
    );

    duplicateFields = duplicateFields.concat(Object.keys(staticMap.duplications));
    duplicateFields = duplicateFields.concat(Object.keys(instanceMap.duplications));

    if (duplicateFields.length) {
        throw new DuplicateDeclarationError(duplicateFields);
    }

    return {
        ...staticMap.gql,
        ...instanceMap.gql,
    };
}

function getPrototypeDescriptors(Class, ignoredProperties) {
    if (Class === Object.getPrototypeOf(Function)) {
        return {};
    }

    const parent = getPrototypeDescriptors(Object.getPrototypeOf(Class), ignoredProperties);

    // Removing ignored properties
    const descriptors = Object
        .entries(Object.getOwnPropertyDescriptors(Class.prototype))
        .filter(([name]) => !ignoredProperties.includes(name))
        .reduce(fromEntries, {});

    return {
        ...parent,
        ...descriptors,
    };
}

/**
 *
 * @param {Class} Class
 * @param {string[]} ignoredInstanceFields
 *
 */
function getStaticInfo(Class, ignoredInstanceFields) {
    if (Class === Object.getPrototypeOf(Function)) {
        return {
            object: {},
            gql: {},
            duplications: {},
        };
    }

    const descriptors = Object.getOwnPropertyDescriptors(Class);

    Object.values(descriptors).forEach((descriptor) => {
        // eslint-disable-next-line no-param-reassign
        descriptor.source = FieldSourceType.static;
    });

    const child = descriptorsToMap(descriptors, ignoredInstanceFields, true);

    const parent = getStaticInfo(Object.getPrototypeOf(Class), ignoredInstanceFields);

    return {
        object: {
            ...parent.object,
            ...child.object,
        },

        gql: {
            ...parent.gql,
            ...child.gql,
        },

        duplications: {
            ...parent.duplications,
            ...child.duplications,
        },
    };
}

function descriptorsToMap(descriptors, ignoredProperties, allowAggregator) {
    const mapInfo = Object
        .entries(descriptors)
        // Removing non-function values or ignored properties
        .filter(([name, { value }]) => {
            if (name === 'constructor' && value === Reflect.constructor) {
                return false;
            }

            if (typeof value !== 'function' || ignoredProperties.includes(name)) {
                return false;
            }

            if (isResolver(value)) {
                return true;
            }

            return !RE_EXCLUDED_FIELDS.test(name);
        })
        .map(([property, { source, value }]) => {
            const resolver = toFieldResolver(value);

            resolver.setSource(source, property);

            if (!allowAggregator && resolver.isAggregator()) {
                throw new InvalidAggregatorDeclarationError(property, 'field resolved by a non-static property');
            }

            if (resolver.getResolvedFields().length === 0) {
                resolver.setResolvedFields([property]);
            }

            return [property, resolver];
        })
        .reduce(
            (acc, [property, resolver]) => {
                acc.object[property] = resolver;

                resolver.getResolvedFields().forEach((field) => {
                    // Handling the special case of constructor that is always created on objects
                    const isDuplicated = field === 'constructor'
                        ? acc.gql[field] !== Reflect.constructor
                        : acc.gql[field];

                    if (isDuplicated) {
                        if (!Array.isArray(acc.duplications[field])) {
                            acc.duplications[field] = [];
                        }

                        acc.duplications[field].push(property);
                        return;
                    }

                    acc.gql[field] = resolver;
                });

                return acc;
            },
            {
                object: {},
                gql: {},
                duplications: {},
            },
        );

    Object.keys(mapInfo.duplications).forEach((name) => {
        delete mapInfo.gql[name];
    });

    return mapInfo;
}

function isResolver(fct) {
    return Boolean(fct[gqlPropertyResolver]);
}


// ============================================================
// Exports
export default toResolverMap;
