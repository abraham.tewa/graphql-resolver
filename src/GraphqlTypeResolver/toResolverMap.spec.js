/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */
// ============================================================
// Import packages
import { assert, expect } from 'chai';
import faker from 'faker';
import sinon from 'sinon';

// ============================================================
// Import modules
import { Field, MultiField, Aggregator } from '../public';
import {
    DuplicateDeclarationError,
    InvalidAggregatorDeclarationError,
} from '../errors';
import GraphqlTypeResolver from './GraphqlTypeResolver';
import toResolverMap from './toResolverMap';
import GraphqlFieldResolver from '../GraphqlFieldResolver';

// ============================================================
// Tests
describe('toResolverMap suite', () => {
    describe('.constructor', () => {
        it('accept class without methods', () => {
            assert.doesNotThrow(
                () => new GraphqlTypeResolver(class {}, {}),
            );
        });

        it('options are not mandatory', () => {
            assert.doesNotThrow(
                () => new GraphqlTypeResolver(class {}),
            );
        });
    });

    describe('internal toResolverMap()', () => {
        it('work if class has no fields', () => {
            const resolverMap = toResolverMap(class {}, [], {});

            assert.deepEqual(resolverMap, { static: {}, instance: {} });
        });

        it('only keep functions fields', () => {
            // ==============================
            // Preparing
            const Class = class test {
                fieldA() {
                    return this.fieldB;
                }

                fieldB = 12;
            };

            // ==============================
            // Execution
            const map = toResolverMap(Class, [], {});

            // ==============================
            // Assertions
            assert.isObject(map.static, 'static');
            assert.isObject(map.instance, 'instance');

            assert.isEmpty(map.static);
            assert.equal(Object.keys(map.instance).length, 1);
            assert.instanceOf(map.instance.fieldA, GraphqlFieldResolver);
            assert.strictEqual(map.instance.fieldA.call(new Class()), 12);
        });

        it('include inherited functions fields', () => {
            // ==============================
            // Preparing
            const value = {};
            const ParentClass = class {
                fieldA() {
                    return this.fieldB;
                }

                fieldB = value;
            };

            const Class = class extends ParentClass {
                fieldC = 13;
            };

            // ==============================
            // Execution
            const map = toResolverMap(Class, [], {});

            // ==============================
            // Assertions
            assert.isObject(map.static, 'static');
            assert.isObject(map.instance, 'instance');

            assert.isEmpty(map.static);
            assert.equal(Object.keys(map.instance).length, 1);
            assert.instanceOf(map.instance.fieldA, GraphqlFieldResolver);
            assert.strictEqual(map.instance.fieldA.call(new Class()), value);
        });

        it('include both instance and static fields', () => {
            const valueA = faker.random.number();
            const valueB = faker.random.number();

            // ==============================
            // Preparing
            const Class = class {
                fieldA() {
                    return this.valueA * 2;
                }

                valueA = valueA;

                static fieldB() {
                    return this.valueB * 3;
                }

                static valueB = valueB;
            };

            // ==============================
            // Execution
            const map = toResolverMap(Class, [], {});

            // ==============================
            // Assertions
            assert.isObject(map.static);
            assert.isObject(map.instance);

            assert.lengthOf(Object.keys(map.static), 1);
            assert.lengthOf(Object.keys(map.instance), 1);

            assert.instanceOf(map.instance.fieldA, GraphqlFieldResolver, 'fieldA is a resolver');
            assert.instanceOf(map.static.fieldB, GraphqlFieldResolver, 'fieldB is a resolver');

            // Ensuring that the correct function are used
            assert.strictEqual(map.instance.fieldA.call({ valueA }), valueA * 2);
            assert.strictEqual(map.static.fieldB.call({ valueB }), valueB * 3);
        });

        it('"constructor" method is automatically ignored', () => {
            const valueA = {};
            const valueB = {};

            // ==============================
            // Preparing
            const Class = class {
                constructor() {
                    this.valueC = 'any value';
                }

                fieldA() {
                    return this.valueA;
                }

                valueA = valueA;

                static fieldB() {
                    return this.valueB;
                }

                static valueB = valueB;
            };

            // ==============================
            // Execution
            const map = toResolverMap(Class, [], {});

            // ==============================
            // Assertions
            assert.hasAllKeys(map.instance, ['fieldA'], 'instance');
            assert.hasAllKeys(map.static, ['fieldB'], 'static');

            assert.strictEqual(map.instance.fieldA.call(new Class()), valueA);
            assert.strictEqual(map.static.fieldB.call(Class), valueB);
        });

        it('static "constructor" method is not ignored', () => {
            const valueA = faker.random.number();
            const valueB = faker.random.number();

            // ==============================
            // Preparing
            const Class = class {
                static constructor() {
                    return valueB * 4;
                }

                fieldA() {
                    return this.valueA * 2;
                }

                valueA = valueA;

                static fieldB() {
                    return this.valueB * 3;
                }

                static valueB = valueB;
            };

            // ==============================
            // Execution
            const map = toResolverMap(Class, [], {});

            // ==============================
            // Assertions
            assert.isObject(map.static);
            assert.hasAllKeys(map.static, ['constructor', 'fieldB']);

            assert.strictEqual(map.static.constructor.call({ valueB }), valueB * 4);
            assert.strictEqual(map.instance.fieldA.call({ valueA }), valueA * 2);
            assert.strictEqual(map.static.fieldB.call({ valueB }), valueB * 3);
        });

        it('allow Field direct definition', () => {
            const valueA = faker.random.number();
            const value1 = faker.random.number();

            // ==============================
            // Preparing
            const Class = class {
                fieldA = MultiField(
                    ['fieldB', 'fieldC'],
                    function resolver() {
                        return this.valueA * 2;
                    },
                )

                fieldD = Field(
                    function resolver() {
                        return this.valueA * 4;
                    },
                )

                valueA = valueA;

                static field1 = Field(
                    'field2',
                    function resolver() {
                        return this.value1 * 3;
                    },
                )

                static value1 = value1;
            };

            // ==============================
            // Execution
            const map = toResolverMap(Class, [], {});

            // ==============================
            // Assertions
            assert.isObject(map.static);
            assert.isObject(map.instance);

            assert.lengthOf(Object.keys(map.static), 1);
            assert.lengthOf(Object.keys(map.instance), 3);

            assert.instanceOf(map.instance.fieldB, GraphqlFieldResolver, 'fieldB');
            assert.instanceOf(map.instance.fieldC, GraphqlFieldResolver, 'fieldC');
            assert.instanceOf(map.instance.fieldD, GraphqlFieldResolver, 'fieldD');
            assert.instanceOf(map.static.field2, GraphqlFieldResolver, 'field2');

            // fieldB and fieldC are the same resolver
            assert.strictEqual(map.instance.fieldB, map.instance.fieldC);

            // Ensuring that the correct function are used
            assert.strictEqual(map.instance.fieldB.call({ valueA }), valueA * 2);
            assert.strictEqual(map.instance.fieldD.call({ valueA }), valueA * 4);

            assert.strictEqual(map.static.field2.call({ value1 }), value1 * 3);
        });

        it('throws an exception in case of field duplication', () => {
            const instanceCase1 = class {
                resolve = Field(
                    'instanceCase2',
                    sinon.spy(),
                );

                resolveAlso = Field(
                    'instanceCase2',
                    sinon.spy(),
                );
            };

            const instanceCase2 = class {
                resolve = Field({
                    resolve: 'instanceCase3',
                    resolver: sinon.spy(),
                });

                instanceCase3() {
                    return this;
                }
            };

            const staticCase1 = class {
                static resolve = Field({
                    resolve: 'staticCase2',
                    resolver: sinon.spy(),
                });

                static resolveAlso = Field({
                    resolve: 'staticCase2',
                    resolver: sinon.spy(),
                });
            };

            const staticCase2 = class {
                static resolve = Field({
                    resolve: 'staticCase3',
                    resolver: sinon.spy(),
                });

                static staticCase3() {
                    return this;
                }
            };

            const mixedCase1 = class {
                resolve = Field({
                    resolve: 'mixedCase1',
                    resolver: sinon.spy(),
                });

                static alsoResolve = Field({
                    resolve: 'mixedCase1',
                    resolver: sinon.spy(),
                });
            };

            const mixedCase2 = class {
                mixedCase2() {
                    return this;
                }

                static resolveAlso = Field({
                    resolve: 'mixedCase2',
                    resolver: sinon.spy(),
                });
            };

            const mixedCase3 = class {
                static resolve = Field({
                    resolve: 'mixedCase3',
                    resolver: sinon.spy(),
                });

                static mixedCase3() {
                    return this;
                }
            };

            [
                { instanceCase2: instanceCase1 },
                { instanceCase3: instanceCase2 },
                { staticCase2: staticCase1 },
                { staticCase3: staticCase2 },
                { mixedCase1 },
                { mixedCase2 },
                { mixedCase3 },
            ].forEach((obj) => {
                const [name, Class] = Object.entries(obj)[0];

                expect(() => toResolverMap(Class, [], {}))
                    .to.matchErrorSnapshot(DuplicateDeclarationError, name)
                    .to.have.deep.property('names', [name]);
            });
        });

        it('won\'t throw exception if property is ignored', () => {
            const resolver = sinon.spy();

            const Class = class {
                resolveA = new GraphqlFieldResolver({
                    resolve: 'fieldA',
                    resolver,
                });

                resolveAlsoA = new GraphqlFieldResolver({
                    resolve: 'fieldA',
                    resolver,
                });
            };

            assert.doesNotThrow(() => toResolverMap(Class, ['resolveAlsoA'], {}));
        });

        it('include parent instance methods', () => {
            const Parent = class {
                fieldA() {
                    return this.valueA;
                }

                valueA = 'A';
            };

            const Child = class extends Parent {
                fieldB() {
                    return this.valueB;
                }

                valueB = 'B';
            };

            const { static: staticFields, instance: instanceFields } = toResolverMap(Child, [], {});

            assert.isObject(staticFields, 'static fields');
            assert.isObject(instanceFields, 'instance fields');

            assert.lengthOf(Object.keys(staticFields), 0, 'static fields');
            assert.lengthOf(Object.keys(instanceFields), 2, 'instance fields');

            assert.instanceOf(instanceFields.fieldA, GraphqlFieldResolver);
            assert.instanceOf(instanceFields.fieldB, GraphqlFieldResolver);

            assert.strictEqual(instanceFields.fieldA.call({ valueA: 'A' }), 'A');
            assert.strictEqual(instanceFields.fieldB.call({ valueB: 'B' }), 'B');
        });

        it('include parent instance fields', () => {
            const Parent = class {
                fieldA = Field({
                    resolve: 'fieldA',
                    resolver: function fieldA() {
                        return this.valueA;
                    },
                })

                valueA = 'A';
            };

            const Child = class extends Parent {
                fieldB() {
                    return this.valueB;
                }

                valueB = 'B';
            };

            const { static: staticFields, instance: instanceFields } = toResolverMap(Child, [], {});

            assert.isObject(staticFields, 'static fields');
            assert.isObject(instanceFields, 'instance fields');

            assert.lengthOf(Object.keys(staticFields), 0, 'static fields');
            assert.lengthOf(Object.keys(instanceFields), 2, 'instance fields');

            assert.instanceOf(instanceFields.fieldA, GraphqlFieldResolver);
            assert.instanceOf(instanceFields.fieldB, GraphqlFieldResolver);

            assert.strictEqual(instanceFields.fieldA.call({ valueA: 'A' }), 'A');
            assert.strictEqual(instanceFields.fieldB.call({ valueB: 'B' }), 'B');
        });

        it('include parent static fields', () => {
            const Parent = class {
                static fieldA = Field({
                    resolve: 'fieldA',
                    resolver: function fieldA() {
                        return this.valueA;
                    },
                })

                valueA = 'A';
            };

            const Child = class extends Parent {
                fieldB() {
                    return this.valueB;
                }

                valueB = 'B';
            };

            const { static: staticFields, instance: instanceFields } = toResolverMap(Child, [], {});

            assert.isObject(staticFields, 'static fields');
            assert.isObject(instanceFields, 'instance fields');

            assert.lengthOf(Object.keys(staticFields), 1, 'static fields');
            assert.lengthOf(Object.keys(instanceFields), 1, 'instance fields');

            assert.instanceOf(staticFields.fieldA, GraphqlFieldResolver);
            assert.instanceOf(instanceFields.fieldB, GraphqlFieldResolver);

            assert.strictEqual(staticFields.fieldA.call({ valueA: 'A' }), 'A');
            assert.strictEqual(instanceFields.fieldB.call({ valueB: 'B' }), 'B');
        });

        it('deeply include parent ancesters fields', () => {
            const GrandParent = class {
                methodGP() {
                    return this.GP * 2;
                }

                fieldGP = Field({
                    resolve: 'fieldGP',
                    resolver: function fieldGP() {
                        return this.GP * 4;
                    },
                })

                static staticGP() {
                    return this.GP * 8;
                }

                static staticFieldGP = Field({
                    resolve: 'staticFieldGP',
                    resolver: function staticFieldGP() {
                        return this.GP * 10;
                    },
                })

                overridedByChild() {
                    return this.GP * 12;
                }

                overridedByParent() {
                    return this.GP * 14;
                }

                static staticallyOverridedByChild() {
                    return this.GP * 16;
                }

                GP = 3;

                static GP = 5;
            };

            const Parent = class extends GrandParent {
                methodP() {
                    return this.P * 2;
                }

                fieldP = Field({
                    resolve: 'fieldP',
                    resolver: function fieldP() {
                        return this.P * 4;
                    },
                })

                overridedByParent() {
                    return this.P * 6;
                }

                static staticP() {
                    return this.P * 8;
                }

                static staticFieldP = Field({
                    resolve: 'staticFieldP',
                    resolver: function staticFieldP() {
                        return this.P * 10;
                    },
                })

                P = 7;

                static P = 11;
            };

            const Child = class extends Parent {
                methodC() {
                    return this.C * 2;
                }

                fieldC = Field({
                    resolve: 'fieldC',
                    resolver: function fieldC() {
                        return this.C * 4;
                    },
                })

                static staticC() {
                    return this.C * 6;
                }

                static staticFieldC = Field({
                    resolve: 'staticFieldC',
                    resolver: function staticFieldC() {
                        return this.C * 8;
                    },
                })

                overridedByChild() {
                    return this.C * 10;
                }

                static staticallyOverridedByChild() {
                    return this.C * 12;
                }

                C = 13;

                static C = 17;
            };

            const { static: staticFields, instance: instanceFields } = toResolverMap(Child, [], {});

            assert.isObject(staticFields, 'static fields');
            assert.isObject(instanceFields, 'instance fields');

            [
                'methodGP', 'fieldGP', 'methodP',
                'fieldP', 'methodC', 'fieldC',
                'overridedByChild', 'overridedByParent',
            ].forEach((name) => {
                assert.instanceOf(instanceFields[name], GraphqlFieldResolver, name);
            });

            [
                'staticGP', 'staticFieldGP', 'staticP',
                'staticFieldP', 'staticC', 'staticFieldC',
                'staticallyOverridedByChild',
            ].forEach((name) => {
                assert.instanceOf(staticFields[name], GraphqlFieldResolver, name);
            });


            assert.lengthOf(Object.keys(staticFields), 7, 'static fields');
            assert.lengthOf(Object.keys(instanceFields), 8, 'instance fields');

            assert.strictEqual(instanceFields.overridedByChild.call({ C: 13 }), 13 * 10, 'child override');
            assert.strictEqual(instanceFields.overridedByParent.call({ P: 7 }), 7 * 6, 'parent override');
            assert.strictEqual(
                staticFields.staticallyOverridedByChild.call({ C: 17 }),
                17 * 12,
                'static child override',
            );
        });

        it('throw an exception if declaring an aggregator on a non-static field', () => {
            const Class = class {
                fieldA = Aggregator({
                    resolve: 'fieldA',
                    resolver: function fieldA() {
                        return this.valueA;
                    },
                })

                valueA = 'A';
            };

            expect(() => toResolverMap(Class, [], {}))
                .to.matchErrorSnapshot(InvalidAggregatorDeclarationError)
                .to.have.property('name', 'fieldA');
        });

        describe('conflictMap resolution', () => {
            it('throw an execption if a field is declared but was not found', () => {
                const Class = class {};
                const fakeProperty = 'fakeProperty';

                expect(() => toResolverMap(
                    Class,
                    [],
                    {
                        fieldA: { static: false, resolvedBy: fakeProperty },
                    },
                ))
                    .to.matchErrorSnapshot(
                        `Property "${fakeProperty}" declared in conflict resolution map was not found`,
                    );
            });

            it('throw an execption if a static field is declared but was not found', () => {
                const Class = class {};
                const fakeProperty = 'fakeProperty';

                expect(() => toResolverMap(
                    Class,
                    [],
                    {
                        fieldA: { static: true, resolvedBy: fakeProperty },
                    },
                ))
                    .to.matchErrorSnapshot(
                        `Property "${fakeProperty}" declared in conflict resolution map was not found`,
                    );
            });

            it('throw an execption field not declared as resolved by the resolver', () => {
                const Class = class {
                    fieldB = sinon.spy()
                };

                const fakeProperty = 'fieldB';

                expect(() => toResolverMap(
                    Class,
                    [],
                    {
                        fieldA: { static: false, resolvedBy: fakeProperty },
                    },
                ))
                    .to.matchErrorSnapshot(`Field "fieldA" is declared as resolved by "${fakeProperty}" but`
                    + 'is not present in the resolved field list of the resolver');
            });
        });

        describe('handle correctly inheritance override', () => {
            const grandParentA = {};
            const grandParentB = {};
            const grandParentC = {};
            const parentA = {};
            const parentB = {};
            const parentC = {};
            const childA = {};
            const childB = {};
            const childC = {};

            const grandParentSpy = sinon.spy(() => ({
                fieldA: grandParentA,
                fieldB: grandParentB,
                fieldC: grandParentC,
            }));

            const parentSpy = sinon.spy(() => ({
                fieldA: parentA,
                fieldB: parentB,
                fieldC: parentC,
            }));

            const childSpy = sinon.spy(() => ({
                fieldA: childA,
                fieldB: childB,
                fieldC: childC,
            }));

            it('field/field', () => {
                class Parent {
                    resolveA = MultiField(
                        ['fieldA', 'fieldB'],
                        parentSpy,
                    )
                }

                class Child extends Parent {
                    resolveBC = MultiField(
                        ['fieldB', 'fieldC'],
                        childSpy,
                    )
                }

                const { static: staticFields, instance: instanceFields } = toResolverMap(
                    Child,
                    [],
                    {
                        fieldB: { static: false, resolvedBy: 'resolveBC' },
                    },
                );

                assert.isEmpty(staticFields, 'static fields');
                assert.hasAllKeys(instanceFields, ['fieldA', 'fieldB', 'fieldC'], 'instance fields');

                assert.strictEqual(instanceFields.fieldA.call().fieldA, parentA, 'fieldA');
                assert.strictEqual(instanceFields.fieldB.call().fieldB, childB, 'fieldB');
                assert.strictEqual(instanceFields.fieldC.call().fieldC, childC, 'fieldC');
            });

            it('static/field', () => {
                class Parent {
                    static resolveA = MultiField(
                        ['fieldA', 'fieldB'],
                        parentSpy,
                    )
                }

                class Child extends Parent {
                    resolveBC = MultiField(
                        ['fieldB', 'fieldC'],
                        childSpy,
                    )
                }

                const { static: staticFields, instance: instanceFields } = toResolverMap(
                    Child,
                    [],
                    {
                        fieldB: { static: false, resolvedBy: 'resolveBC' },
                    },
                );

                assert.hasAllKeys(staticFields, ['fieldA'], 'static fields');
                assert.hasAllKeys(instanceFields, ['fieldB', 'fieldC'], 'instance fields');

                assert.strictEqual(staticFields.fieldA.call().fieldA, parentA, 'fieldA');
                assert.strictEqual(instanceFields.fieldB.call().fieldB, childB, 'fieldB');
                assert.strictEqual(instanceFields.fieldC.call().fieldC, childC, 'fieldC');
            });

            it('field/static', () => {
                class Parent {
                    resolveA = MultiField(
                        ['fieldA', 'fieldB'],
                        parentSpy,
                    )
                }

                class Child extends Parent {
                    static resolveBC = MultiField(
                        ['fieldB', 'fieldC'],
                        childSpy,
                    )
                }

                const { static: staticFields, instance: instanceFields } = toResolverMap(
                    Child,
                    [],
                    {
                        fieldB: { static: true, resolvedBy: 'resolveBC' },
                    },
                );

                assert.hasAllKeys(staticFields, ['fieldB', 'fieldC'], 'static fields');
                assert.hasAllKeys(instanceFields, ['fieldA'], 'instance fields');

                assert.strictEqual(instanceFields.fieldA.call().fieldA, parentA, 'fieldA');
                assert.strictEqual(staticFields.fieldB.call().fieldB, childB, 'fieldB');
                assert.strictEqual(staticFields.fieldC.call().fieldC, childC, 'fieldC');
            });

            it('static/static with overlap', () => {
                class Parent {
                    static resolveA = MultiField(
                        ['fieldA', 'fieldB'],
                        parentSpy,
                    )
                }

                class Child extends Parent {
                    static resolveBC = MultiField(
                        ['fieldB', 'fieldC'],
                        childSpy,
                    )
                }

                const { static: staticFields, instance: instanceFields } = toResolverMap(
                    Child,
                    [],
                    {},
                );

                assert.hasAllKeys(staticFields, ['fieldA', 'fieldB', 'fieldC'], 'static fields');
                assert.isEmpty(instanceFields, 'instance fields');

                assert.strictEqual(staticFields.fieldA.call().fieldA, parentA, 'fieldA');
                assert.strictEqual(staticFields.fieldB.call().fieldB, childB, 'fieldB');
                assert.strictEqual(staticFields.fieldC.call().fieldC, childC, 'fieldC');
            });

            it('static/static/static with overlap', () => {
                class GrandParent {
                    static resolveGrandParent = MultiField(
                        ['fieldA', 'fieldB'],
                        grandParentSpy,
                    )
                }

                class Parent extends GrandParent {}

                class Child extends Parent {
                    static resolveChild = MultiField(
                        ['fieldB', 'fieldC'],
                        childSpy,
                    )
                }

                const { static: staticFields, instance: instanceFields } = toResolverMap(
                    Child,
                    [],
                    {},
                );

                assert.hasAllKeys(staticFields, ['fieldA', 'fieldB', 'fieldC'], 'static fields');
                assert.isEmpty(instanceFields, 'instance fields');

                assert.strictEqual(staticFields.fieldA.call().fieldA, grandParentA, 'fieldA');
                assert.strictEqual(staticFields.fieldB.call().fieldB, childB, 'fieldB');
                assert.strictEqual(staticFields.fieldC.call().fieldC, childC, 'fieldC');
            });

            it('static/field/static with overlap', () => {
                class GrandParent {
                    static resolveGrandParent = MultiField(
                        ['fieldA', 'fieldB'],
                        grandParentSpy,
                    )
                }

                class Parent extends GrandParent {
                    resolveParent = MultiField(
                        ['fieldB'],
                        parentSpy,
                    )
                }

                class Child extends Parent {
                    static resolveChild = Field(
                        'fieldC',
                        childSpy,
                    )
                }

                const { static: staticFields, instance: instanceFields } = toResolverMap(
                    Child,
                    [],
                    {
                        fieldB: { static: false, resolvedBy: 'resolveParent' },
                    },
                );

                assert.hasAllKeys(staticFields, ['fieldA', 'fieldC'], 'static fields');
                assert.hasAllKeys(instanceFields, ['fieldB'], 'instance fields');

                assert.strictEqual(staticFields.fieldA.call().fieldA, grandParentA, 'fieldA');
                assert.strictEqual(instanceFields.fieldB.call().fieldB, parentB, 'fieldB');
                assert.strictEqual(staticFields.fieldC.call().fieldC, childC, 'fieldC');
            });
        });
    });
});
