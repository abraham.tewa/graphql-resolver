// ============================================================
// Import modules
import { gqlResolverData } from '../constants';

// ============================================================
// Functions
/**
 * @param {GraphqlTypeResolver} typeResolver
 */
function toResolverClass(typeResolver) {
    const RawResolverClass = typeResolver.getRawResolverClass();

    return class extends RawResolverClass {
        /**
         * Resolution context object
         * @type {QueryContext}
         */
        #context;

        /**
         * Object resolved
         * @type {Object}
         */
        #object;

        /** @type {GraphqlTypeResolver} */
        #typeResolver;

        /**
         *
         * @param {QueryContext} context
         * @param {Object} object
         */
        constructor(context, object = {}) {
            super(context, object);

            this.#typeResolver = typeResolver;
            this.#object = object;
            this.#context = context.publicContext;

            const resolverData = {
                // Resolved graphql object
                object,

                // Will contain all solvings by resolvers
                solved: new Map(),

                // Fields
                fields: {},

                // Current instance
                instance: this,
            };

            /*
            Object.defineProperties(
                this,
                attributeMapDescriptors,
            ); */

            const property = {
                value: resolverData,
                enumerable: false,
            };

            Object.defineProperty(this, gqlResolverData, property);
            Object.defineProperty(object, gqlResolverData, property);
        }

        get gql() {
            return this.#object;
        }

        get gqlContext() {
            return this.#context;
        }

        gqlResolve(field, args) {
            return this.#typeResolver.resolve(field, this.#object, args, this.#context);
        }
    };
}

// ============================================================
// Exports
export default toResolverClass;
