/* eslint-disable max-classes-per-file */
// ============================================================
// Import modules

import fieldResolver from '../fieldResolver';
import toResolverMap from './toResolverMap';
import toResolverClass from './buildResolverClass';
import {
    convertToType,
    getResolvedType,
    fromEntries,
} from '../helpers';

// ============================================================
// Class
class GraphqlTypeResolver {
    #apolloResolver;

    /**
     * List of fields that will be ignored
     * @returns {string[]}
     */
    #ignoredFields;

    #ResolverClass;

    #RawResolverClass;

    #conflictResolverMap;

    #name;

    /**
     * @type {Object.<GraphqlFieldResolver>}
     */
    #resolverMap;

    /**
     * @type {Schema}
     */
    #schema;

    /**
     * Map each fields resolved with their graphql resolver class.
     * @type {Object.<GraphqlTypeResolver>}
     */
    #fieldResolvedTypes;

    #attributeMapDescriptors;

    /**
     *
     * @param {Class}    RawResolverClass
     * @param {string[]} ignore    - List of ignored properties
     * @param {Schema}   schema    - Schema in which belong the type resolver
     * @hideconstructor
     */
    constructor(
        RawResolverClass,
        {
            ignore = [],
            conflictResolver = {},
            name,
            schema,
        } = {},
    ) {
        this.#ignoredFields = ignore;
        this.#RawResolverClass = RawResolverClass;
        this.#conflictResolverMap = { ...conflictResolver };
        this.#schema = schema;
        this.#name = name;

        this.#schema.declareTypeResolver(this);

        this.#resolverMap = toResolverMap(
            this.#RawResolverClass,
            this.#ignoredFields,
            this.#conflictResolverMap,
        );

        this.#apolloResolver = toApolloResolver(this, this.#resolverMap);

        this.#attributeMapDescriptors = toAttributeMapDescriptors(
            this.#resolverMap,
            this.#apolloResolver,
        );

        this.#ResolverClass = toResolverClass(this);
    }

    /**
     *
     * @param {string} field - Name of the field
     * @param {*}      value - Value to convert
     * @internal
     */
    convertToType(field, value) {
        const type = this.#fieldResolvedTypes[field];

        if (!type) {
            return value;
        }

        return convertToType(
            type.getResolverClass(),
            value,
        );
    }

    finalize() {
        const mapEntries = this.getResolvedFields()
            .map((name) => {
                const { type: resolvedTypeName } = getResolvedType(
                    this.#schema.getGraphqlSchema(),
                    this.getTypeName(),
                    name,
                );

                const type = this.#schema.getTypeResolver(resolvedTypeName);

                return [
                    name,
                    type,
                ];
            });

        this.#fieldResolvedTypes = Object.fromEntries(mapEntries);
    }

    /**
     * Returning the apollo resolver corresponding
     * @throws {DuplicateDeclarationError}
     * @throws {InvalidAggregatorDeclarationError}
     * @returns {Object.<ApolloPropertyResolver>}
     */
    getApolloResolver() {
        return this.#apolloResolver;
    }

    getAttributeMapDescriptors() {
        return this.#attributeMapDescriptors;
    }

    /**
     * @returns {Schema}
     */
    getGraphqlSchema() {
        return this.#schema;
    }

    /**
     * Return the list of resolved fields
     * @returns {string[]}
     * @public
     */
    getResolvedFields() {
        return Object.keys(this.#apolloResolver);
    }

    /**
     * Create a wrapper around the type resolver declared by the user.
     * @returns {Class}
     */
    getResolverClass() {
        return this.#ResolverClass;
    }

    /**
     * @returns {Class}
     */
    getRawResolverClass() {
        return this.#RawResolverClass;
    }

    /**
     * @returns {Object.<GraphqlFieldResolver>}
     */
    getResolverMap() {
        return this.#resolverMap;
    }

    /**
     * Return the resolved name
     * @returns {string}
     */
    getTypeName() {
        return this.#name;
    }

    /**
     * Resolve a field.
     * The apollo resolver must have been generated.
     *
     * @param {string}       field  - Field to resolve
     * @param {Object}       parent -
     * @param {Array}        args
     * @param {QueryContext} queryContext
     */
    resolve(field, parent, args, queryContext) {
        if (this.#apolloResolver[field]) {
            return this.#apolloResolver[field](parent, args, queryContext);
        }

        return parent[field];
    }
}

// ============================================================
// Helpers

/**
 * @param {GraphqlTypeResolver} TypeResolver
 * @param {Object.<GraphqlFieldResolver} resolverMap
 * @returns {Object.<function>}
 */
function toApolloResolver(TypeResolver, resolverMap) {
    return Object
        .entries(resolverMap)
        .map(([field, resolver]) => [
            field,
            fieldResolver.bind(
                undefined,
                TypeResolver,
                resolver,
                field,
                resolver.getSourceType(),
                resolver.getSourceName(),
            ),
        ])
        .reduce(fromEntries, {});
}

function toAttributeMapDescriptors(resolverMap, apolloResolver) {
    return Object
        .entries(resolverMap)
        .map(([field, resolver]) => [
            resolver.getSourceName(),
            {
                value: apolloResolver[field],
                configurable: true,
                writable: true,
            },
        ])
        .reduce(fromEntries, {});
}

// ============================================================
// Exports
export default GraphqlTypeResolver;
