/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */
// ============================================================
// Import packages
import { assert, expect } from 'chai';
import sinon from 'sinon';

// ============================================================
// Import modules
import { DuplicateDeclarationError } from '../errors';
import toOptions from './toResolverMap';

describe('toOptions()', () => {
    it('accept no parameters', () => {
        assert.doesNotThrow(() => toOptions({}));
    });

    it('accept a function as first parameter', () => {
        const resolver = sinon.spy();
        const options = toOptions({ nameOrOptions: resolver });

        expect(options).to.matchSnapshot();
        assert.strictEqual(options.resolver, resolver);
    });

    it('have default values', () => {
        const resolver = sinon.spy();
        const options = toOptions({ resolver });

        expect(options).to.matchSnapshot();
        assert.strictEqual(options.resolver, resolver);
    });

    it('have default aggregation values', () => {
        const options = toOptions({
            resolver: sinon.spy(),
            isAggregation: true,
        });

        expect(options).to.matchSnapshot();
        assert.property(options, 'groupBy');
    });

    it('accept resolved field as parameter', () => {
        const name = 'name';
        const resolver = sinon.spy();
        const options = toOptions({ nameOrOptions: name, resolver });

        expect(options).to.matchSnapshot();
        assert.strictEqual(options.resolve, name);
    });

    it('accept resolved field list as parameter', () => {
        const names = ['name'];
        const resolver = sinon.spy();
        const options = toOptions({
            nameOrOptions: names,
            resolver,
            isMultifield: true,
        });

        expect(options).to.matchSnapshot();
        assert.isArray(options.resolve);
        assert.lengthOf(options.resolve, 1);
        assert.notStrictEqual(options.resolve, names);
        assert.strictEqual(options.resolve[0], names[0]);
    });

    it('mark the field as skipped if boolean value', () => {
        const names = false;
        const resolver = sinon.spy();
        const options = toOptions({ nameOrOptions: names, resolver });

        expect(options).to.matchSnapshot();
        assert.isTrue(options.skipped);
    });

    it('handle correctly default options', () => {
        const resolver = sinon.spy();

        const options = toOptions({
            nameOrOptions: {
                resolve: 'field',
            },
            resolver,
        });

        expect(options).to.matchSnapshot();
        assert.property(options, 'resolve', 'field');
        assert.property(options, 'resolver', resolver);
    });

    it('handle correctly default aggregation options', () => {
        const options = toOptions({
            isAggregation: true,
            nameOrOptions: {
                groupBy: {
                    properties: ['abc'],
                    limits: {
                        maxWait: 23,
                    },
                },
            },
            resolver: sinon.spy(),
        });

        expect(options).to.matchSnapshot();
        assert.property(options, 'groupBy');
        assert.property(options.groupBy, 'limits');

        assert.deepPropertyVal(options.groupBy, 'properties', ['abc']);
        assert.property(options.groupBy, 'query', true, 'groupBy.query');
        assert.property(options.groupBy.limits, 'maxWait', 23, 'groupBy.limits.maxWait');
        assert.property(options.groupBy.limits, 'maxItems', 100, 'groupBy.limits.maxItems');
    });
});

describe('toResolver', () => {
    it('throw an exception if field declared twice', () => {
        const resolver = sinon.spy();
        const options = {
            nameOrOptions: {
                resolve: ['field', 'field'],
            },
            resolver,
            isMultifield: true,
            isAggregation: false,
        };

        expect(() => toOptions(options))
            .to.matchErrorSnapshot(DuplicateDeclarationError);
    });
});
