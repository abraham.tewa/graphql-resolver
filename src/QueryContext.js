// ============================================================
// Class

class QueryContext {
    publicContext = {};

    constructor(publicContext) {
        this.publicContext = publicContext;
    }
}

// ============================================================
// Exports
export default QueryContext;
