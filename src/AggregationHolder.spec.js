/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert, expect } from 'chai';
import faker from 'faker';
import sinon from 'sinon';

// ============================================================
// Import modules
import { Aggregator } from './public';
import GraphqlTypeResolver from './GraphqlTypeResolver';
import Schema from './Schema';
import AggregationHolder, { toGroup } from './AggregationHolder';

// ============================================================
// Tests
describe('AggregationHolder Suite', () => {
    describe('(internal) class AggregationHolder', () => {
        let ref;
        let schema;
        let lastNameIndex = 0;
        const maxWaitFieldA = 15;

        before(() => {
            ref = Symbol('test');
            schema = Schema.declare(ref);
        });

        after(() => {
            Schema.forgot(ref);
        });

        class TestClass {
            static fieldA = Aggregator(
                {
                    groupBy: {
                        limits: {
                            maxWait: maxWaitFieldA,
                        },
                    },
                },
                (list) => list.map((item, index) => `${index}/x`),
            )

            static fieldB = sinon.spy()
        }

        function generateTypeResolver(options) {
            lastNameIndex += 1;
            return new GraphqlTypeResolver(
                TestClass,
                {
                    schema,
                    name: `Type${lastNameIndex}`,
                    ...options,
                },
            );
        }

        describe('(static internal) AggregationHolder.getCache()', () => {
            it('return the same cache with the same values', () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;

                const cache = AggregationHolder.getCache(schema, TypeResolver, resolver);
                const sameCache = AggregationHolder.getCache(schema, TypeResolver, resolver);

                assert.strictEqual(cache, sameCache);
                assert.isObject(cache);
                assert.isEmpty(cache);
            });

            it('return a different cache if resolver is different', () => {
                const TypeResolver = generateTypeResolver();
                const resolverA = TypeResolver.getResolverMap().fieldA;
                const resolverB = TypeResolver.getResolverMap().fieldB;

                const cacheA = AggregationHolder.getCache(schema, TypeResolver, resolverA);
                const cacheB = AggregationHolder.getCache(schema, TypeResolver, resolverB);

                assert.notStrictEqual(cacheA, cacheB);
            });

            it('return a different cache if type is different', () => {
                const TypeResolverA = generateTypeResolver();
                const TypeResolverB = generateTypeResolver();

                const resolverA = TypeResolverA.getResolverMap().fieldA;
                const resolverB = TypeResolverB.getResolverMap().fieldA;

                const cacheA = AggregationHolder.getCache(schema, TypeResolverA, resolverA);
                const cacheB = AggregationHolder.getCache(schema, TypeResolverB, resolverB);

                assert.notStrictEqual(cacheA, cacheB);
            });

            it('return a different cache if schema is different', () => {
                const refA = Symbol('A');
                const refB = Symbol('B');

                const schemaA = Schema.declare(refA);
                const schemaB = Schema.declare(refB);

                class Class {
                    static fieldA = Aggregator(
                        sinon.spy(),
                    )
                }

                const TypeResolverA = new GraphqlTypeResolver(Class, { schema: schemaA });
                const TypeResolverB = new GraphqlTypeResolver(Class, { schema: schemaB });

                const resolverA = TypeResolverA.getResolverMap().fieldA;
                const resolverB = TypeResolverB.getResolverMap().fieldA;

                const cacheA = AggregationHolder.getCache(schemaA, TypeResolverA, resolverA);
                const cacheB = AggregationHolder.getCache(schemaB, TypeResolverB, resolverB);

                assert.notStrictEqual(cacheA, cacheB);

                Schema.forgot(refA);
                Schema.forgot(refB);
            });
        });

        describe('(static internal) AggregationHolder.getHolder()', () => {
            it('return an holder if no grouping', () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});
                const sameHolder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                assert.instanceOf(holder, AggregationHolder);
                assert.strictEqual(holder, sameHolder);
            });

            describe('return the same holder', () => {
                it('if properties are equals', () => {
                    const TypeResolver = generateTypeResolver();
                    const resolver = TypeResolver.getResolverMap().fieldA;

                    const holder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { properties: { a: 1 } },
                    );

                    const differentHolder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { properties: { a: 1 } },
                    );

                    assert.instanceOf(holder, AggregationHolder);
                    assert.strictEqual(holder, differentHolder);
                });

                it('if aggregators are equals', () => {
                    const TypeResolver = generateTypeResolver();
                    const resolver = TypeResolver.getResolverMap().fieldA;

                    const holder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { aggregator: { a: 1 } },
                    );

                    const differentHolder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { aggregator: { a: 1 } },
                    );

                    assert.instanceOf(holder, AggregationHolder);
                    assert.strictEqual(holder, differentHolder);
                });

                it('if path are equals', () => {
                    const TypeResolver = generateTypeResolver();
                    const resolver = TypeResolver.getResolverMap().fieldA;

                    const holder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { path: 'abc' },
                    );

                    const differentHolder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { path: 'abc' },
                    );

                    assert.instanceOf(holder, AggregationHolder);
                    assert.strictEqual(holder, differentHolder);
                });
            });

            describe('return different holders', () => {
                it('if properties are differents', () => {
                    const TypeResolver = generateTypeResolver();
                    const resolver = TypeResolver.getResolverMap().fieldA;

                    const holder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        {},
                    );

                    const differentHolder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { properties: { a: 2 } },
                    );

                    assert.instanceOf(holder, AggregationHolder);
                    assert.notStrictEqual(holder, differentHolder);
                });

                it('if properties are differents', () => {
                    const TypeResolver = generateTypeResolver();
                    const resolver = TypeResolver.getResolverMap().fieldA;

                    const holder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { properties: { a: 1 } },

                    );
                    const differentHolder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { properties: { a: 2 } },
                    );

                    assert.instanceOf(holder, AggregationHolder);
                    assert.notStrictEqual(holder, differentHolder);
                });

                it('if aggregators are differents', () => {
                    const TypeResolver = generateTypeResolver();
                    const resolver = TypeResolver.getResolverMap().fieldA;

                    const holder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { aggregators: { a: 1 } },
                    );

                    const differentHolder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { aggregators: { a: 2 } },
                    );

                    assert.instanceOf(holder, AggregationHolder);
                    assert.notStrictEqual(holder, differentHolder);
                });

                it('if path are differents', () => {
                    const TypeResolver = generateTypeResolver();
                    const resolver = TypeResolver.getResolverMap().fieldA;

                    const holder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { path: 'abc' },
                    );
                    const differentHolder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { path: 'def' },
                    );

                    assert.instanceOf(holder, AggregationHolder);
                    assert.notStrictEqual(holder, differentHolder);
                });

                it('if path is different by not properties nor aggregators', () => {
                    const TypeResolver = generateTypeResolver();
                    const resolver = TypeResolver.getResolverMap().fieldA;

                    const holder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { path: 'abc', properties: { a: 1 }, aggregator: 'a' },
                    );

                    const differentHolder = AggregationHolder.getHolder(
                        schema,
                        TypeResolver,
                        resolver,
                        { path: 'def', properties: { a: 1 }, aggregator: 'a' },
                    );

                    assert.instanceOf(holder, AggregationHolder);
                    assert.notStrictEqual(holder, differentHolder);
                });
            });
        });

        describe('(static public) AggregationHolder.resolve()', () => {
            it('return the resolved value', async () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;
                const instance = new TestClass();
                const args = {};

                const value = await AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                    },
                    instance,
                    args,
                    { path: 'abc' },
                );

                await wait(maxWaitFieldA);

                assert.strictEqual(value, '0/x');
            });

            it('aggregate calls', async () => {
                const spy = sinon.spy((list) => list.map(({ args }) => args[0]));

                class Class {
                    static fieldA = Aggregator(
                        {
                            groupBy: {
                                limits: {
                                    maxWait: maxWaitFieldA,
                                },
                            },
                        },
                        spy,
                    )
                }

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });
                const resolver = TypeResolver.getResolverMap().fieldA;
                const instanceA = new TestClass();
                const instanceB = new TestClass();
                const instanceC = new TestClass();
                const argsA = [{ name: 'A' }, 'other parameter'];
                const argsB = [{ name: 'B' }, 'other parameter'];
                const argsC = [{ name: 'C' }, 'other parameter'];

                const valueAPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                    },
                    instanceA,
                    argsA,
                    { path: 'abc' },
                );

                const valueBPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                    },
                    instanceB,
                    argsB,
                    { path: 'abc' },
                );

                const valueCPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                    },
                    instanceC,
                    argsC,
                    { path: 'abc' },
                );

                await wait(maxWaitFieldA);
                const [valueA, valueB, valueC] = await Promise.all([valueAPromise, valueBPromise, valueCPromise]);


                expect(valueAPromise).to.be.promise('A');
                expect(valueBPromise).to.be.promise('B');
                expect(valueCPromise).to.be.promise('C');

                assert.strictEqual(valueA, argsA[0], 'A');
                assert.strictEqual(valueB, argsB[0], 'B');
                assert.strictEqual(valueC, argsC[0], 'C');

                assert.isTrue(spy.calledOnce);

                const call = spy.firstCall;

                assert.lengthOf(call.args, 2, 'call args');

                // Checking list items
                assert.isArray(call.args[0]);
                const listItems = call.args[0];

                assert.lengthOf(listItems, 3, 'listItems');
                assert.strictEqual(listItems[0].instance, instanceA, 'instanceA');
                assert.strictEqual(listItems[1].instance, instanceB, 'instanceB');
                assert.strictEqual(listItems[2].instance, instanceC, 'instanceC');

                assert.strictEqual(listItems[0].args, argsA, 'argsA');
                assert.strictEqual(listItems[1].args, argsB, 'argsB');
                assert.strictEqual(listItems[2].args, argsC, 'argsC');

                // Checking group
                // Since no grouping information has been provided, the group is empty
                assert.isObject(call.args[1]);
                assert.isEmpty(call.args[1]);
            });

            it('use another aggregation is maxItems is reached', async function it() {
                this.timeout(maxWaitFieldA * 3);
                const spy = sinon.spy((list) => list.map(({ args }) => args[0]));

                class Class {
                    static fieldA = Aggregator(
                        {
                            groupBy: {
                                path: true,
                                limits: {
                                    maxWait: maxWaitFieldA,
                                    maxItems: 2,
                                },
                            },
                        },
                        spy,
                    )
                }

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });
                const resolver = TypeResolver.getResolverMap().fieldA;
                const instanceA = new TestClass();
                const instanceB = new TestClass();
                const instanceC = new TestClass();
                const argsA = [{ name: 'A' }, 'other parameter'];
                const argsB = [{ name: 'B' }, 'other parameter'];
                const argsC = [{ name: 'C' }, 'other parameter'];

                const valueAPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                        path: 'abc',
                    },
                    instanceA,
                    argsA,
                );

                const valueBPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                        path: 'abc',
                    },
                    instanceB,
                    argsB,
                );

                const valueCPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                        path: 'abc',
                    },
                    instanceC,
                    argsC,
                );

                await wait(maxWaitFieldA);
                const [valueA, valueB, valueC] = await Promise.all([valueAPromise, valueBPromise, valueCPromise]);


                expect(valueAPromise).to.be.promise('A');
                expect(valueBPromise).to.be.promise('B');
                expect(valueCPromise).to.be.promise('C');

                assert.strictEqual(valueA, argsA[0], 'A');
                assert.strictEqual(valueB, argsB[0], 'B');
                assert.strictEqual(valueC, argsC[0], 'C');

                assert.isTrue(spy.calledTwice);

                // ========================================
                // First call
                let call = spy.firstCall;
                assert.lengthOf(call.args, 2);

                // Checking list items of the first call
                assert.isArray(call.args[0]);
                let listItems = call.args[0];

                assert.lengthOf(listItems, 2);
                assert.strictEqual(listItems[0].instance, instanceA, 'instanceA');
                assert.strictEqual(listItems[1].instance, instanceB, 'instanceB');

                assert.strictEqual(listItems[0].args, argsA, 'argsA');
                assert.strictEqual(listItems[1].args, argsB, 'argsB');

                // Checking group
                // Since no grouping information has been provided, the group is empty
                assert.isObject(call.args[1]);
                assert.hasAllKeys(call.args[1], ['path']);
                assert.propertyVal(call.args[1], 'path', 'abc');

                // ========================================
                // First call
                call = spy.secondCall;
                assert.lengthOf(call.args, 2);

                // Checking list items
                assert.isArray(call.args[0]);
                [listItems] = call.args;

                assert.lengthOf(listItems, 1);
                assert.strictEqual(listItems[0].instance, instanceC, 'instanceC');
                assert.strictEqual(listItems[0].args, argsC, 'argsC');
            });

            it('use another aggregation if group is different', async function it() {
                this.timeout(maxWaitFieldA * 3);
                const spy = sinon.spy((list) => list.map(({ args }) => args[0]));

                class Class {
                    static fieldA = Aggregator(
                        {
                            groupBy: {
                                path: true,
                                limits: {
                                    maxWait: maxWaitFieldA,
                                    maxItems: 100,
                                },
                            },
                        },
                        spy,
                    )
                }

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });
                const resolver = TypeResolver.getResolverMap().fieldA;
                const instanceA = new TestClass();
                const instanceB = new TestClass();
                const instanceC = new TestClass();
                const argsA = [{ name: 'A' }, 'other parameter'];
                const argsB = [{ name: 'B' }, 'other parameter'];
                const argsC = [{ name: 'C' }, 'other parameter'];
                const pathA = 'abc';
                const pathB = 'def';

                const valueAPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                        path: pathA,
                    },
                    instanceA,
                    argsA,
                    { path: pathA },
                );

                const valueBPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                        path: pathA,
                    },
                    instanceB,
                    argsB,
                    { path: pathA },
                );

                const valueCPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                        path: pathB,
                    },
                    instanceC,
                    argsC,
                    { path: pathB },
                );

                await wait(maxWaitFieldA);
                const [valueA, valueB, valueC] = await Promise.all([valueAPromise, valueBPromise, valueCPromise]);


                expect(valueAPromise).to.be.promise('A');
                expect(valueBPromise).to.be.promise('B');
                expect(valueCPromise).to.be.promise('C');

                assert.strictEqual(valueA, argsA[0], 'A');
                assert.strictEqual(valueB, argsB[0], 'B');
                assert.strictEqual(valueC, argsC[0], 'C');

                assert.isTrue(spy.calledTwice);

                // ========================================
                // First call
                let call = spy.firstCall;
                assert.lengthOf(call.args, 2);

                // Checking list items of the first call
                assert.isArray(call.args[0]);
                let listItems = call.args[0];

                assert.lengthOf(listItems, 2);
                assert.strictEqual(listItems[0].instance, instanceA, 'instanceA');
                assert.strictEqual(listItems[1].instance, instanceB, 'instanceB');

                assert.strictEqual(listItems[0].args, argsA, 'argsA');
                assert.strictEqual(listItems[1].args, argsB, 'argsB');

                // Checking group
                // Since no grouping information has been provided, the group is empty
                assert.isObject(call.args[1]);
                assert.hasAllKeys(call.args[1], ['path']);
                assert.propertyVal(call.args[1], 'path', pathA);

                // ========================================
                // First call
                call = spy.secondCall;
                assert.lengthOf(call.args, 2);

                // Checking list items
                assert.isArray(call.args[0]);
                [listItems] = call.args;

                assert.lengthOf(listItems, 1);
                assert.strictEqual(listItems[0].instance, instanceC, 'instanceC');
                assert.strictEqual(listItems[0].args, argsC, 'argsC');

                assert.isObject(call.args[1]);
                assert.hasAllKeys(call.args[1], ['path']);
                assert.propertyVal(call.args[1], 'path', pathB);
            });

            it('handle correctly promised groups', async function it() {
                this.timeout(maxWaitFieldA * 3);
                const spy = sinon.spy((list) => list.map(({ args }) => args[0]));

                class Class {
                    static fieldA = Aggregator(
                        {
                            groupBy: {
                                limits: {
                                    maxWait: maxWaitFieldA,
                                    maxItems: 100,
                                },
                            },
                        },
                        spy,
                    )
                }

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });
                const resolver = TypeResolver.getResolverMap().fieldA;
                const instanceA = new TestClass();
                const instanceB = new TestClass();
                const instanceC = new TestClass();
                const argsA = [{ name: 'A' }, 'other parameter'];
                const argsB = [{ name: 'B' }, 'other parameter'];
                const argsC = [{ name: 'C' }, 'other parameter'];
                const propertyA = 'A';
                const propertyB = 'B';

                const valueAPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                        properties: {
                            propertyA: Promise.resolve(propertyA),
                        },
                    },
                    instanceA,
                    argsA,
                    { path: 'abc' },
                );

                const valueBPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                        properties: {
                            propertyA: Promise.resolve(propertyA),
                        },
                    },
                    instanceB,
                    argsB,
                    { path: 'abc' },
                );

                const valueCPromise = AggregationHolder.resolve(
                    schema,
                    {
                        type: TypeResolver,
                        resolver,
                        properties: {
                            propertyA: Promise.resolve(propertyB),
                        },
                    },
                    instanceC,
                    argsC,
                    { path: 'abc' },
                );

                await wait(maxWaitFieldA);
                const [valueA, valueB, valueC] = await Promise.all([valueAPromise, valueBPromise, valueCPromise]);


                expect(valueAPromise).to.be.promise('A');
                expect(valueBPromise).to.be.promise('B');
                expect(valueCPromise).to.be.promise('C');

                assert.strictEqual(valueA, argsA[0], 'A');
                assert.strictEqual(valueB, argsB[0], 'B');
                assert.strictEqual(valueC, argsC[0], 'C');

                assert.isTrue(spy.calledTwice, 'called twice');

                // ========================================
                // First call
                let call = spy.firstCall;
                assert.lengthOf(call.args, 2, 'first call args');

                // Checking list items of the first call
                assert.isArray(call.args[0]);
                let listItems = call.args[0];

                assert.lengthOf(listItems, 2);
                assert.strictEqual(listItems[0].instance, instanceA, 'instanceA');
                assert.strictEqual(listItems[1].instance, instanceB, 'instanceB');

                assert.strictEqual(listItems[0].args, argsA, 'argsA');
                assert.strictEqual(listItems[1].args, argsB, 'argsB');

                // Checking group
                // Since no grouping information has been provided, the group is empty
                assert.isObject(call.args[1]);
                assert.hasAllKeys(call.args[1], ['properties'], 'first call group');
                assert.hasAllKeys(call.args[1].properties, ['propertyA'], 'first call properties');
                assert.propertyVal(call.args[1].properties, 'propertyA', propertyA, 'first call property A');

                // ========================================
                // First call
                call = spy.secondCall;
                assert.lengthOf(call.args, 2);

                // Checking list items
                assert.isArray(call.args[0]);
                [listItems] = call.args;

                assert.lengthOf(listItems, 1);
                assert.strictEqual(listItems[0].instance, instanceC, 'instanceC');
                assert.strictEqual(listItems[0].args, argsC, 'argsC');

                assert.isObject(call.args[1]);
                assert.hasAllKeys(call.args[1], ['properties'], 'second call group');
                assert.hasAllKeys(call.args[1].properties, ['propertyA'], 'second call properties');
                assert.propertyVal(call.args[1].properties, 'propertyA', propertyB, 'second call property A');
            });
        });

        describe('(public) AggregationHolder.prototype.add()', () => {
            it('throw an exception if sealed', async () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                holder.start();
                await holder;

                expect(() => holder.add({}, [])).to.matchErrorSnapshot();
            });

            it('return the resolved value', async () => {
                class Class {
                    static fieldA = Aggregator(
                        {
                            groupBy: {
                                limits: {
                                    maxItems: 2,
                                },
                            },
                        },
                        (list) => list.map(({ args: [value] }) => `${value}x`),
                    )
                }

                const valueA = 'A';
                const valueB = 'B';

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                const resultAPromise = holder.add({}, [valueA]);
                const resultBPromise = holder.add({}, [valueB]);

                holder.start();
                const [resultA, resultB] = await Promise.all([resultAPromise, resultBPromise]);

                assert.strictEqual(resultA, 'Ax', 'A');
                assert.strictEqual(resultB, 'Bx', 'B');
            });

            it('return the value if aggregator return a promise', async () => {
                class Class {
                    static fieldA = Aggregator(
                        {
                            groupBy: {
                                limits: {
                                    maxItems: 2,
                                },
                            },
                        },
                        (list) => Promise.resolve(list.map(({ args: [value] }) => `${value}x`)),
                    )
                }

                const valueA = 'A';
                const valueB = 'B';

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                const resultAPromise = holder.add({}, [valueA]);
                const resultBPromise = holder.add({}, [valueB]);

                holder.start();
                const [resultA, resultB] = await Promise.all([resultAPromise, resultBPromise]);

                assert.strictEqual(resultA, 'Ax', 'A');
                assert.strictEqual(resultB, 'Bx', 'B');
            });

            it('return the value if aggregator list of promise', async () => {
                class Class {
                    static fieldA = Aggregator(
                        {
                            groupBy: {
                                limits: {
                                    maxItems: 2,
                                },
                            },
                        },
                        (list) => list.map(({ args: [value] }) => Promise.resolve(`${value}x`)),
                    )
                }

                const valueA = 'A';
                const valueB = 'B';

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                const resultAPromise = holder.add({}, [valueA]);
                const resultBPromise = holder.add({}, [valueB]);

                holder.start();
                const [resultA, resultB] = await Promise.all([resultAPromise, resultBPromise]);

                assert.strictEqual(resultA, 'Ax', 'A');
                assert.strictEqual(resultB, 'Bx', 'B');
            });
        });

        describe('(public) AggregationHolder.prototype.getItems()', () => {
            it('return an empty list if no items', () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                const items = holder.getItems();

                assert.isArray(items);
                assert.isEmpty(items);
            });

            it('return a copy of the list', () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                const list = holder.getItems();
                const differentList = holder.getItems();

                assert.isArray(list);
                assert.isArray(differentList);
                assert.isEmpty(list);
                assert.isEmpty(differentList);
                assert.notStrictEqual(list, differentList);
            });

            it('return the list of items', () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                const instanceA = {};
                const argsA = [];

                const instanceB = {};
                const argsB = [];

                holder.add(instanceA, argsA);
                holder.add(instanceB, argsB);

                const list = holder.getItems();

                assert.isArray(list);
                assert.lengthOf(list, 2);

                assert.strictEqual(list[0].instance, instanceA, 'instanceA');
                assert.strictEqual(list[0].args, argsA, 'argsA');

                assert.strictEqual(list[1].instance, instanceB, 'instanceB');
                assert.strictEqual(list[1].args, argsB, 'argsB');
            });
        });

        describe('(public) AggregationHolder.prototype.initialize()', () => {
            it('start immediatly if no max wait defined', () => {
                const value = [];
                const spy = sinon.spy(() => value);

                class Class {
                    static fieldA = Aggregator(
                        {
                            groupBy: {
                                limits: {
                                    maxWait: undefined,
                                },
                            },
                        },
                        spy,
                    )
                }

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });

                const resolver = TypeResolver.getResolverMap().fieldA;

                assert.isTrue(spy.notCalled, 'before initialization');
                const holder = new AggregationHolder(TypeResolver, resolver, {}, () => {});

                holder.initialize();
                assert.isTrue(spy.calledOnce, 'after initialization');
            });

            it('do nothing if aggregation already started', () => {
                const value = [];
                const spy = sinon.spy(() => value);

                class Class {
                    static fieldA = Aggregator(spy)
                }

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });

                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = new AggregationHolder(TypeResolver, resolver, {}, () => {});

                assert.isTrue(spy.notCalled);
                holder.start();
                assert.isTrue(spy.calledOnce, 'after start');

                holder.initialize();
                assert.isTrue(spy.calledOnce, 'after initialization');
            });

            it('starts after max wait', async function it() {
                const value = [];
                const spy = sinon.spy(() => value);
                const maxWait = 30;

                this.timeout(maxWait * 3);

                class Class {
                    static fieldA = Aggregator(
                        {
                            groupBy: {
                                limits: {
                                    maxWait,
                                },
                            },
                        },
                        spy,
                    )
                }

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });

                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = new AggregationHolder(TypeResolver, resolver, {}, () => {});

                holder.initialize();
                assert.isFalse(spy.called, 'called after initialization');

                await wait(maxWait * 0.9);

                assert.isFalse(spy.called, 'called after 90% duration');

                await wait(maxWait * 0.3);

                assert.isTrue(spy.called, 'called after 120% duration');
            });
        });

        describe('(public) AggregationHolder.prototype.isClosed()', () => {
            it('return false when holder not closed', () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                assert.isFalse(holder.isClosed());
            });

            it('return true when holder is closed', () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                holder.start();
                assert.isTrue(holder.isClosed());
            });
        });

        describe('(internal) AggregationHolder.prototype.start()', () => {
            it('close the holder', () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                assert.isFalse(holder.isClosed());
                holder.start();
                assert.isTrue(holder.isClosed());
            });

            it('cleanup it\'s cache', () => {
                const TypeResolver = generateTypeResolver();
                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});
                const sameHolder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                assert.strictEqual(holder, sameHolder);
                holder.start();

                const differentHolder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});
                assert.notStrictEqual(holder, differentHolder);
            });

            it('execute the resolver', async () => {
                const value = [];
                const spy = sinon.spy(() => value);

                class Class {
                    static fieldA = Aggregator(spy)
                }

                const TypeResolver = new GraphqlTypeResolver(Class, { schema });

                const resolver = TypeResolver.getResolverMap().fieldA;

                const holder = AggregationHolder.getHolder(schema, TypeResolver, resolver, {});

                assert.isTrue(spy.notCalled);
                holder.start();
                assert.isTrue(spy.calledOnce);

                const result = await holder;

                assert.strictEqual(result, value);
            });
        });
    });

    describe('(internal) toGroup()', () => {
        it('work with no grouping data', () => {
            const group = toGroup({});

            assert.isObject(group);
            expect(group).to.not.be.an.promise();
            assert.isEmpty(group);
        });

        it('add path to grouping data', () => {
            const path = faker.random.word();

            const group = toGroup({ path: true }, { path });
            expect(group).not.to.be.promise();
            assert.hasAllKeys(group, 'path');
            assert.property(group, 'path', path);
        });

        it('add properties to grouping', () => {
            const A = {};
            const B = {};

            const group = toGroup({
                properties: { A, B },
            });

            assert.isObject(group);
            expect(group).not.to.be.promise();
            assert.hasAllKeys(group, ['properties'], 'group');
            assert.hasAllKeys(group.properties, ['A', 'B'], 'group.properties');

            assert.strictEqual(group.properties.A, A, 'A');
            assert.strictEqual(group.properties.B, B, 'B');
        });

        it('add aggregator to grouping', () => {
            const value = {};

            const group = toGroup({
                aggregator: value,
            });

            assert.isObject(group);
            expect(group).not.to.be.promise();
            assert.hasAllKeys(group, ['aggregator'], 'group');

            assert.strictEqual(group.aggregator, value, 'aggregator');
        });

        it('add aggregator, path and properties to grouping', async () => {
            const value = {};
            const A = {};
            const B = {};
            const C = {};
            const path = faker.random.word();

            const group = toGroup(
                {
                    aggregator: value,
                    path,
                    properties: {
                        A,
                        B,
                        C,
                    },
                },
            );

            expect(group).to.not.be.promise();
            assert.isObject(group);
            assert.hasAllKeys(group, ['aggregator', 'properties', 'path'], 'group');
            assert.hasAllKeys(group.properties, ['A', 'B', 'C'], 'group');

            assert.strictEqual(group.aggregator, value, 'aggregator');
            assert.strictEqual(group.path, path, 'path');
            assert.strictEqual(group.properties.A, A, 'A');
            assert.strictEqual(group.properties.B, B, 'B');
            assert.strictEqual(group.properties.C, C, 'C');
        });

        it('return a promise if a property value is a promise', async () => {
            const A = {};
            const B = {};

            const groupPromise = toGroup({
                properties: {
                    A: Promise.resolve(A),
                    B,
                },
            });

            expect(groupPromise).to.be.promise();

            const group = await groupPromise;

            assert.hasAllKeys(group, ['properties'], 'group');
            assert.hasAllKeys(group.properties, ['A', 'B'], 'group.properties');

            assert.strictEqual(group.properties.A, A, 'A');
            assert.strictEqual(group.properties.B, B, 'B');
        });

        it('return a promise if a several property values are promise', async () => {
            const A = {};
            const B = {};
            const C = {};

            const groupPromise = toGroup({
                properties: {
                    A: Promise.resolve(A),
                    B: Promise.resolve(B),
                    C,
                },
            });

            expect(groupPromise).to.be.promise();

            const group = await groupPromise;

            assert.hasAllKeys(group, ['properties'], 'group');
            assert.hasAllKeys(group.properties, ['A', 'B', 'C'], 'group.properties');

            assert.strictEqual(group.properties.A, A, 'A');
            assert.strictEqual(group.properties.B, B, 'B');
            assert.strictEqual(group.properties.C, C, 'C');
        });

        it('return a promise if aggregator is a promise', async () => {
            const value = {};

            const groupPromise = toGroup({
                aggregator: Promise.resolve(value),
            });

            expect(groupPromise).to.be.promise();
            const group = await groupPromise;

            assert.isObject(group);
            assert.hasAllKeys(group, ['aggregator'], 'group');

            assert.strictEqual(group.aggregator, value, 'aggregator');
        });

        it('return a promise if aggregator and several properties promise', async () => {
            const value = {};
            const A = {};
            const B = {};
            const C = {};
            const path = faker.random.word();

            const groupPromise = toGroup(
                {
                    aggregator: Promise.resolve(value),
                    path,
                    properties: {
                        A: Promise.resolve(A),
                        B: Promise.resolve(B),
                        C,
                    },
                },
            );

            expect(groupPromise).to.be.promise();
            const group = await groupPromise;

            assert.isObject(group);
            assert.hasAllKeys(group, ['aggregator', 'path', 'properties'], 'group');
            assert.hasAllKeys(group.properties, ['A', 'B', 'C'], 'group');

            assert.strictEqual(group.aggregator, value, 'aggregator');
            assert.strictEqual(group.path, path, 'path');
            assert.strictEqual(group.properties.A, A, 'A');
            assert.strictEqual(group.properties.B, B, 'B');
            assert.strictEqual(group.properties.C, C, 'C');
        });
    });
});

function wait(duration) {
    const promise = new Promise((resolve) => {
        setTimeout(resolve, duration);
    });

    return promise;
}
