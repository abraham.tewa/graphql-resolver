export const gqlData = Symbol('graphql-resolver: gqlData');
export const gqlType = Symbol('graphql-resolver: GraphqlType');
export const gqlPropertyResolver = Symbol('graphql-resolver: GraphqlFieldResolver');
export const gqlTypeContext = Symbol('GraphQL Context');
export const gqlInstance = Symbol('graphql-resolver: instance');
export const defaultSchema = Symbol('Default schema');
export const gqlAggregator = Symbol('graphql-resolver: aggregator');
export const G = Symbol('graphql-resolver');
export const gqlAggreagationHolder = Symbol('graphql-resolver: aggregationHolder');
export const gqlMetaOptions = Symbol('graphql-resolver: meta-options');
export const gqlQueryContext = Symbol('Graphql QueryContext');
export const gqlResolverData = Symbol('graphql-resolver: resolver data');
export const gqlResolverCallback = Symbol('graphql-resolver: resolver callback');
export const gqlFieldOptions = Symbol('graphql-resolver: field options');

export const FieldSourceType = {
    attribute: 'attribute',
    prototype: 'prototype',
    static: 'static',
};
