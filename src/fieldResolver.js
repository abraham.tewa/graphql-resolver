// ============================================================
// Import modules
import AggregationHolder from './AggregationHolder';
import { gqlResolverData } from './constants';
import { isPromise } from './helpers';

// ============================================================
// Function
/**
 * Resolve a fields
 * @param {GraphqlTypeResolver}           TypeResolver
 * @param {Object.<GraphqlFieldResolver>} resolverMap
 * @param {GraphqlFieldResolver}          resolver
 * @param {string}                        fieldName    - Name of the field
 * @param {boolean}                       isStatic
 * @param {Object}                        parent
 * @param {Array}                         args         - Argument to pass to the field resolver
 * @param {QueryContext}                  queryContext
 * @param {Object}                        info
 */
function resolverCallback(
    TypeResolver,
    resolver,
    fieldName,
    sourceType,
    sourceName,
    parent,
    args = [],
    queryContext,
    info,
) {
    const ResolverClass = TypeResolver.getResolverClass();

    const resolverData = parent[gqlResolverData] || new ResolverClass(queryContext, parent)[gqlResolverData];

    const { solved, fields } = resolverData;

    // Checking if the resolver already has been resolved.
    if (!solved[resolver]) {
        let value;

        // TODO: Handle recursion (if resolver try to resolve it's own field)
        if (resolver.isAggregator()) {
            value = aggregate(
                TypeResolver,
                resolver,
                resolverData.object,
                args,
                queryContext,
                info,
            );
        }
        else if (sourceType === 'static') {
            value = resolver.call(
                ResolverClass,
                resolverData.instance,
                args,
                queryContext.publicContext,
                info,
            );
        }
        else if (sourceType) {
            value = resolver.call(
                resolverData.instance,
                args,
            );
        }

        solved[resolver] = { value };
    }

    const solvedValue = solved[resolver].value;

    // ==============================
    // Saving solved data
    if (!resolver.isMultiFieldResolver()) {
        // If resolver is a single field resolver, we
        // save it so it's accessible through .solved() method
        // eslint-disable-next-line no-param-reassign
        fields[fieldName] = TypeResolver.convertToType(
            fieldName,
            solvedValue,
        );
    }
    // Multi-property resolving promise
    else if (isPromise(solvedValue)) {
        fields[fieldName] = solvedValue.then((value) => TypeResolver.convertToType(
            fieldName,
            value[fieldName],
        ));
    }
    // Multi-property resolving object
    else if (solvedValue) {
        fields[fieldName] = TypeResolver.convertToType(
            fieldName,
            solvedValue[fieldName],
        );
    }
    // If resolved value is undefined, setting null to all fields
    else {
        fields[fieldName] = null;
    }

    return fields[fieldName];
}

/**
 *
 * Resolve a fields
 * @param {Schema}               schema
 * @param {GraphqlTypeResolver}  TypeResolver
 * @param {GraphqlFieldResolver} fieldResolver
 * @param {Object}               instance
 * @param {Array}                args
 * @param {QueryContext}         queryContext
 * @param {GraphqlInfo}          info
 */
async function aggregate(
    TypeResolver,
    fieldResolver,
    instance,
    args,
    queryContext,
    info,
) {
    const group = {
        type: TypeResolver,
        resolver: fieldResolver,
    };

    // Adding current resolved path as group discriminant
    if (fieldResolver.aggregation.path) {
        group.path = info.path;
    }

    // Adding properties as group discriminant
    if (fieldResolver.aggregation.properties.length) {
        group.properties = {};

        fieldResolver.aggregation.properties.forEach((name) => {
            group.properties[name] = TypeResolver.resolve(name, instance, [], queryContext);
        });
    }

    // Adding aggregator result as group discriminant
    if (fieldResolver.aggregation.aggregator) {
        const value = fieldResolver.aggregation.aggregator.call(
            TypeResolver.getResolverClass(),
            instance,
            args,
            queryContext,
        );
        group.aggregator = value;
    }

    const root = fieldResolver.aggregation.query
        ? queryContext
        : TypeResolver;

    // The aggreagation holder to use will depend of the group by.
    const value = AggregationHolder.resolve(root, group, instance, args, queryContext);

    return value;
}

// ============================================================
// Exports
export default resolverCallback;
export {
    aggregate,
};
