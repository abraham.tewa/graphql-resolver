// ============================================================
// Class
class GraphqlFieldResolver {
    /**
     * Indicate if the resolver is multifield or not
     */
    #multifield;

    /**
     * List of resolved
     * @type {string|boolean}
     */
    #resolvedFields;

    /**
     * @type {string}
     */
    #sourceName;

    /** @type {FieldSourceType} */
    #sourceType;

    /**
     *
     * @param {GraphqlResolverOptions} options
     */
    constructor({
        resolver: callback,
        resolve,
        groupBy = false,
        isMultifield = false,
    }) {
        this.callback = callback;
        this.#multifield = isMultifield;
        this.#resolvedFields = resolve
            ? [...resolve]
            : [];

        this.aggregation = typeof groupBy === 'object'
            ? {
                ...groupBy,
                limits: { ...groupBy.limits },
            }
            : groupBy;
    }

    /**
     * Execute the resolver.
     * Have the same signature then the "Function.prototype.apply" methdod.
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Objets_globaux/Function/apply
     */
    apply(thisArg, args) {
        return thisArg[this.#sourceName](...args);
    }

    /**
     * Execute the resolver.
     * Have the same signature then the "Function.prototype.bind" methdod.
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Objets_globaux/Function/bin
     */
    bind(thisArg, ...args) {
        return thisArg[this.#sourceName].bind(thisArg, ...args);
    }

    /**
     * Execute the resolver.
     * Have the same signature then the "Function.prototype.call" methdod.
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Objets_globaux/Function/call
     */
    call(thisArg, ...args) {
        return thisArg[this.#sourceName](...args);
    }

    /**
     * Return the list of fields to resolve
     * @returns {string[]}
     */
    getResolvedFields() {
        return this.#resolvedFields.slice(0);
    }

    getSourceName() {
        return this.#sourceName;
    }

    /**
     * @returns {FieldSourceType}
     */
    getSourceType() {
        return this.#sourceType;
    }


    /**
     * Indicate if the resolver is an aggregator or not.
     * @returns {boolean}
     */
    isAggregator() {
        return Boolean(this.aggregation);
    }

    /**
     * Indicate if the resolve is a multifield resolver or not
     * @returns {boolean}
     */
    isMultiFieldResolver() {
        return this.#multifield;
    }

    /**
     * Define the resolved field(s)
     * @param {string[]} resolve
     * @internal
     */
    setResolvedFields(resolvedFields) {
        this.#resolvedFields = resolvedFields;
    }

    /**
     *
     * @param {FieldSourceType} type
     * @param {string}          name
     */
    setSource(type, name) {
        this.#sourceName = name;
        this.#sourceType = type;
    }

    toString() {
        let fields = this.getResolvedFields().join('", "');
        if (fields) {
            fields = `"${fields}"`;
        }

        return `GraphqlFieldResolver.<${fields}> {\n`
         + `\tisAggregator: ${this.isAggregator()}\n`
         + `\tisMultiField: ${this.isMultiFieldResolver()}\n`
         + '}';
    }
}

// ============================================================
// Exports
export default GraphqlFieldResolver;
