/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import faker from 'faker';
import { assert } from 'chai';
import sinon from 'sinon';

import { Schema, Type, Aggregator } from '../../src';

// ============================================================
// Import modules
import { schema as schemaTools } from '../setup';
import db from './db';

describe('Aggregator()', () => {
    let schema;
    let ref;

    beforeEach(() => {
        ref = Symbol('integration test: API / Field()');
        schema = schemaTools.generate({
            database: db,
            ref,
            includeResolvers: false,
            contextCreator: () => new TestContext(),
        }).schema;
    });

    afterEach(() => {
        Schema.forgot(ref);
    });

    describe('can be', () => {
        it('instance field (implicit)', async () => {
            // ============================================================
            // Preparation
            const spy = sinon.spy(
                (items) => {
                    const list = items.map(
                        ({ instance, args: { includePrefix } }) => toDisplayName(instance, includePrefix),
                    );

                    return list;
                },
            );

            class Author {
                static displayName = Aggregator(
                    'displayName',
                    (...args) => spy(...args),
                );
            }

            await testField(schema, Author, spy);
        });
    });
});


async function testField(schema, Author, spy) {
    // ============================================================
    // Preparation
    class Query {
        static authors() {
            return Object.values(db.authors).map(({ ...author }) => author);
        }
    }

    Type(Query, { schema });
    const authorTypeResolver = Type(Author, { schema });

    await schema.compile();

    const getQuery = function getQuery(includePrefix) {
        const args = typeof includePrefix === 'boolean'
            ? `(includePrefix: ${includePrefix})`
            : '';

        return `query {
                        authors {
                            id
                            displayName${args}
                        }
                    }
                `;
    };

    // ============================================================
    // Execution

    const promises = [
        schema.resolve(getQuery(true)),
        schema.resolve(getQuery(false)),
        schema.resolve(getQuery()),
    ];

    const [
        responseWithPrefix,
        responseWithoutPrefix,
        responseWithoutParameters,
    ] = await Promise.all(promises);

    // ============================================================
    // Assertions
    assertSameAuthorDisplayNamesList('with prefix', responseWithPrefix, true);
    assertSameAuthorDisplayNamesList('without prefix', responseWithoutPrefix, false);
    assertSameAuthorDisplayNamesList('without parameters', responseWithoutParameters, false);

    const contexts = new Map();

    assert.strictEqual(spy.callCount, 3, 'call count');

    spy.getCalls().forEach((call) => {
        const { args, exception } = call;

        assert.isUndefined(exception);
        assert.lengthOf(args, 2);

        const items = args[0];
        const group = args[1];

        assert.isArray(items, 'Items');
        assert.isObject(group, 'group');
        assert.lengthOf(items, Object.keys(db.authors).length);

        items.forEach(({ instance, context }) => {
            assert.instanceOf(instance, authorTypeResolver, 'instance');
            assert.instanceOf(context, TestContext, 'context');

            // All calls are done by the same context
            if (!contexts.has(call)) {
                contexts.set(call, context);
            }

            assert.strictEqual(contexts.get(call), context);
        });
    });

    assert.lengthOf(contexts, 3, 'context count');
}

function toDisplayName(thisArg, includePrefix) {
    const { firstName, lastName } = thisArg.gql;

    const prefix = includePrefix
        ? `${thisArg.gql.prefix} `
        : '';

    return `${prefix}${firstName} ${lastName}`;
}

function assertSameAuthorDisplayNamesList(message, response, includePrefix) {
    const done = {};

    assert.isUndefined(response.errors, `${message}: errors`);
    assert.isArray(response.data.authors);

    const { authors } = response.data;

    assert.lengthOf(authors, Object.keys(db.authors).length);

    authors.forEach(({ id, displayName }) => {
        assert.isString(id);
        assert.isString(displayName);

        assert.isUndefined(done[id], id);

        done[id] = true;

        const author = db.authors[id];

        const expectedDisplayName = toDisplayName({ gql: author }, includePrefix);

        assert.strictEqual(displayName, expectedDisplayName, `${message} / ${id}`);
    });
}

class TestContext {
    val = faker.random.word()
}
