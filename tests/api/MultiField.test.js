/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert } from 'chai';
import sinon from 'sinon';

import { Schema, Type, MultiField } from '../../src';

// ============================================================
// Import modules
import { schema as schemaTools } from '../setup';
import db from './db';

const now = new Date();

describe('MultiField()', () => {
    let schema;
    let ref;

    beforeEach(() => {
        ref = Symbol('integration test: API / Field()');
        schema = schemaTools.generate({
            database: db,
            ref,
            includeResolvers: false,
            contextCreator: () => new TestContext(),
        }).schema;
    });

    afterEach(() => {
        Schema.forgot(ref);
    });

    describe('can be', () => {
        it('attribute field', async () => {
            const spy = sinon.spy(
                (thisValue) => getAgeAndBirthDate(thisValue),
            );

            class Author {
                ageAndBirthDate = MultiField(
                    ['age', 'birthDate'],
                    () => spy(this),
                )
            }

            await testField(schema, Author, spy);
        });

        it('static field', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => getAgeAndBirthDate(thisValue, includePrefix),
            );

            class Author {
                static ageAndBirthDate = MultiField(
                    ['age', 'birthDate'],
                    (instance) => spy(instance),
                )
            }

            await testField(schema, Author, spy);
        });
    });

    describe('can use', () => {
        it('another field', async () => {
            const spy = sinon.spy(
                (thisValue) => getAgeAndBirthDate(thisValue),
            );


            class Author {
                ageAndBirthDate = MultiField(
                    ['age', 'birthDate'],
                    () => {
                        assert.strictEqual(this.gql.birthDate.getTime(), this.getBirthDate().getTime(), 'birth date');
                        assert.strictEqual(getAge(this.gql.birthDate), this.getAge(), 'age');
                        return spy(this);
                    },
                )

                getAge() {
                    return getAge(this.gql.birthDate);
                }

                getBirthDate = () => this.gql.birthDate
            }

            await testField(schema, Author, spy);
        });

        it('gqlResolver', async () => {
            const spy = sinon.spy(
                (thisValue) => getAgeAndBirthDate(thisValue),
            );

            class Author {
                ageAndBirthDate = MultiField(
                    ['age', 'birthDate'],
                    () => {
                        const displayName = this.gqlResolve('displayName');
                        assert.strictEqual(
                            displayName,
                            `${this.gql.firstName} ${this.gql.lastName}`,
                            'displayName',
                        );
                        return spy(this);
                    },
                )

                displayName() {
                    return `${this.gql.firstName} ${this.gql.lastName}`;
                }
            }

            await testField(schema, Author, spy);
        });
    });
});

async function testField(schema, Author, spy) {
    // ============================================================
    // Preparation
    class Query {
        static authors() {
            return Object.values(db.authors).map(({ ...author }) => author);
        }
    }

    Type(Query, { schema });
    const authorTypeResolver = Type(Author, { schema });

    await schema.compile();

    const getQuery = function getQuery() {
        return `
            query {
                authors {
                    id
                    birthDate
                    age
                }
            }
        `;
    };

    // ============================================================
    // Execution
    const responseWithPrefix = await schema.resolve(getQuery());
    const responseWithoutPrefix = await schema.resolve(getQuery());
    const responseWithoutParameters = await schema.resolve(getQuery());

    // ============================================================
    // Assertions
    assertSameAuthorDisplayNamesList('with prefix', responseWithPrefix, true);
    assertSameAuthorDisplayNamesList('without prefix', responseWithoutPrefix, false);
    assertSameAuthorDisplayNamesList('without parameters', responseWithoutParameters, false);

    spy.getCalls().forEach(({ args, exception }) => {
        assert.isUndefined(exception);
        assert.lengthOf(args, 1);

        const instance = args[0];
        assert.instanceOf(instance, authorTypeResolver, 'instance');
        assert.instanceOf(instance.gqlContext, TestContext, 'context');
    });
}

function assertSameAuthorDisplayNamesList(message, response) {
    const done = {};

    assert.isUndefined(response.errors, `${message}: errors`);
    assert.isArray(response.data.authors, `${message}: authors is array`);

    const { authors } = response.data;

    assert.lengthOf(authors, Object.keys(db.authors).length, `${message}: authors count`);

    authors.forEach(({ id, age, birthDate }) => {
        assert.isString(id, `${message} / ${id}: id is string`);
        assert.isNumber(age, `${message} / ${id}: age is number`);

        assert.isUndefined(done[id], id, `${message} / ${id}: author not already treated`);

        done[id] = true;

        const author = db.authors[id];

        const expected = getAgeAndBirthDate({ gql: author });

        assert.strictEqual(expected.age, age, `${message} / ${id} / age`, `${message} / ${id}: age`);
        assert.strictEqual(expected.birthDate.getTime(), birthDate.getTime(), `${message} / ${id}: birth date`);
    });
}

function getAgeAndBirthDate(thisValue) {
    const { birthDate } = thisValue.gql;

    return {
        birthDate,
        age: getAge(birthDate),
    };
}

function getAge(birthDate) {
    return Math.floor((now - birthDate) / (365 * 24 * 60 * 60 * 1000));
}

class TestContext {}
