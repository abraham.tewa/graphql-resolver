/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert } from 'chai';
import sinon from 'sinon';

import { Schema, Type, Field } from '../../src';

// ============================================================
// Import modules
import { schema as schemaTools } from '../setup';
import db from './db';

describe('Field()', () => {
    let schema;
    let ref;

    beforeEach(() => {
        ref = Symbol('integration test: API / Field()');
        schema = schemaTools.generate({
            database: db,
            ref,
            includeResolvers: false,
            contextCreator: () => new TestContext(),
        }).schema;
    });

    afterEach(() => {
        Schema.forgot(ref);
    });

    describe('can be', () => {
        it('prototype function', async () => {
            // ============================================================
            // Preparation
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(thisValue, includePrefix),
            );

            class Author {
                displayName({ includePrefix }) {
                    return spy(this, includePrefix);
                }
            }

            await testField(schema, Author, spy);
        });

        it('instance field (implicit)', async () => {
            // ============================================================
            // Preparation
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(thisValue, includePrefix),
            );

            class Author {
                displayName = ({ includePrefix }) => spy(this, includePrefix);
            }

            await testField(schema, Author, spy);
        });

        it('instance field (explicit without rename and no options)', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(thisValue, includePrefix),
            );

            class Author {
                displayName = Field(
                    'displayName',
                    ({ includePrefix }) => spy(this, includePrefix),
                );
            }

            await testField(schema, Author, spy);
        });

        it('instance field (explicit without rename with options)', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(thisValue, includePrefix),
            );

            class Author {
                displayName = Field(
                    {
                        name: 'displayName',
                    },
                    ({ includePrefix }) => spy(this, includePrefix),
                );
            }

            await testField(schema, Author, spy);
        });

        it('instance field (explicit with rename)', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(thisValue, includePrefix),
            );

            class Author {
                field = Field(
                    'displayName',
                    ({ includePrefix }) => spy(this, includePrefix),
                );
            }

            await testField(schema, Author, spy);
        });

        it('static function', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(thisValue, includePrefix),
            );

            class Author {
                static displayName(instance, { includePrefix }) {
                    return spy(instance, includePrefix);
                }
            }

            await testField(schema, Author, spy);
        });

        it('static attribute (implicit)', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(thisValue, includePrefix),
            );

            class Author {
                static displayName = (instance, { includePrefix }) => spy(instance, includePrefix)
            }

            await testField(schema, Author, spy);
        });

        it('static attribute (explicit without rename and no options)', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(thisValue, includePrefix),
            );

            class Author {
                static displayName = Field(
                    'displayName',
                    (instance, { includePrefix }) => spy(instance, includePrefix),
                )
            }

            await testField(schema, Author, spy);
        });

        it('static attribute (explicit without rename and options)', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(thisValue, includePrefix),
            );

            class Author {
                static displayName = Field(
                    { name: 'displayName' },
                    (instance, { includePrefix }) => spy(instance, includePrefix),
                )
            }

            await testField(schema, Author, spy);
        });

        it('static attribute (explicit with rename and no options)', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(thisValue, includePrefix),
            );

            class Author {
                static field = Field(
                    'displayName',
                    (instance, { includePrefix }) => spy(instance, includePrefix),
                )
            }

            await testField(schema, Author, spy);
        });
    });

    describe('can use', () => {
        it('another field', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(
                    thisValue,
                    includePrefix,
                ),
            );

            class Author {
                displayName({ includePrefix }) {
                    assert.strictEqual(this.gql.firstName, this.getFirstName());
                    assert.strictEqual(this.gql.lastName, this.getLastName());
                    return spy(this, includePrefix);
                }

                getFirstName() {
                    return this.gql.firstName;
                }

                getLastName = () => this.gql.lastName
            }

            await testField(schema, Author, spy);
        });

        it('gqlResolve', async () => {
            const spy = sinon.spy(
                (thisValue, includePrefix) => toDisplayName(
                    thisValue,
                    includePrefix,
                ),
            );

            class Author {
                async displayName({ includePrefix }) {
                    const firstName = await this.gqlResolve('firstName');

                    assert.strictEqual(firstName, this.gql.firstName);
                    assert.strictEqual(this.gql.lastName, this.getLastName());
                    return spy(this, includePrefix);
                }

                getLastName = () => this.gql.lastName
            }

            await testField(schema, Author, spy);
        });
    });
});

async function testField(schema, Author, spy) {
    // ============================================================
    // Preparation
    class Query {
        static authors() {
            return Object.values(db.authors).map(({ ...author }) => author);
        }
    }

    Type(Query, { schema });
    const authorTypeResolver = Type(Author, { schema });

    await schema.compile();

    const getQuery = function getQuery(includePrefix) {
        const args = typeof includePrefix === 'boolean'
            ? `(includePrefix: ${includePrefix})`
            : '';

        return `query {
                        authors {
                            id
                            displayName${args}
                        }
                    }
                `;
    };

    // ============================================================
    // Execution
    const responseWithPrefix = await schema.resolve(getQuery(true));
    const responseWithoutPrefix = await schema.resolve(getQuery(false));
    const responseWithoutParameters = await schema.resolve(getQuery());

    // ============================================================
    // Assertions
    assertSameAuthorDisplayNamesList('with prefix', responseWithPrefix, true);
    assertSameAuthorDisplayNamesList('without prefix', responseWithoutPrefix, false);
    assertSameAuthorDisplayNamesList('without parameters', responseWithoutParameters, false);

    spy.getCalls().forEach(({ args, exception }) => {
        assert.isUndefined(exception);
        assert.lengthOf(args, 2);

        const instance = args[0];
        assert.instanceOf(instance, authorTypeResolver);
        assert.instanceOf(instance.gqlContext, TestContext);
    });
}

function toDisplayName(thisArg, includePrefix) {
    const { firstName, lastName } = thisArg.gql;

    const prefix = includePrefix
        ? `${thisArg.gql.prefix} `
        : '';

    return `${prefix}${firstName} ${lastName}`;
}

function assertSameAuthorDisplayNamesList(message, response, includePrefix) {
    const done = {};

    assert.isUndefined(response.errors, `${message}: errors`);
    assert.isArray(response.data.authors);

    const { authors } = response.data;

    assert.lengthOf(authors, Object.keys(db.authors).length);

    authors.forEach(({ id, displayName }) => {
        assert.isString(id);
        assert.isString(displayName);

        assert.isUndefined(done[id], id);

        done[id] = true;

        const author = db.authors[id];

        const expectedDisplayName = toDisplayName({ gql: author }, includePrefix);

        assert.strictEqual(displayName, expectedDisplayName, `${message} / ${id}`);
    });
}

class TestContext {}
