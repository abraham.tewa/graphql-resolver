/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import faker from 'faker';
import { assert } from 'chai';
import sinon from 'sinon';

import { Schema, Type, MultiFieldAggregator } from '../../src';

// ============================================================
// Import modules
import { schema as schemaTools } from '../setup';
import db from './db';

const now = new Date();

describe('MultiFieldAggregator()', () => {
    let schema;
    let ref;

    beforeEach(() => {
        ref = Symbol('integration test: API / Field()');
        schema = schemaTools.generate({
            database: db,
            ref,
            includeResolvers: false,
            contextCreator: () => new TestContext(),
        }).schema;
    });

    afterEach(() => {
        Schema.forgot(ref);
    });

    describe('can be', () => {
        it('instance field (implicit)', async () => {
            // ============================================================
            // Preparation
            const spy = sinon.spy(
                (items) => {
                    const list = items.map(
                        ({ instance }) => getAgeAndBirthDate(instance),
                    );

                    return list;
                },
            );

            class Author {
                static displayName = MultiFieldAggregator(
                    ['age', 'birthDate'],
                    (...args) => spy(...args),
                );
            }

            await testField(schema, Author, spy);
        });
    });
});

function getAgeAndBirthDate(thisValue) {
    const { birthDate } = thisValue.gql;

    return {
        birthDate,
        age: getAge(birthDate),
    };
}

function getAge(birthDate) {
    return Math.floor((now - birthDate) / (365 * 24 * 60 * 60 * 1000));
}


async function testField(schema, Author, spy) {
    // ============================================================
    // Preparation
    class Query {
        static authors() {
            return Object.values(db.authors).map(({ ...author }) => author);
        }
    }

    Type(Query, { schema });
    const authorTypeResolver = Type(Author, { schema });

    await schema.compile();

    const getQuery = function getQuery() {
        return `query {
                        authors {
                            age
                            birthDate
                            id
                        }
                    }
                `;
    };

    // ============================================================
    // Execution

    const promises = [
        schema.resolve(getQuery()),
        schema.resolve(getQuery()),
        schema.resolve(getQuery()),
    ];

    const [
        responseWithPrefix,
        responseWithoutPrefix,
        responseWithoutParameters,
    ] = await Promise.all(promises);

    // ============================================================
    // Assertions
    assertSameAgeAndBirthDate('with prefix', responseWithPrefix, true);
    assertSameAgeAndBirthDate('without prefix', responseWithoutPrefix, false);
    assertSameAgeAndBirthDate('without parameters', responseWithoutParameters, false);


    const contexts = new Map();

    assert.strictEqual(spy.callCount, 3, 'call count');

    spy.getCalls().forEach((call) => {
        const { args, exception } = call;

        assert.isUndefined(exception);
        assert.lengthOf(args, 2);

        const items = args[0];
        const group = args[1];

        assert.isArray(items, 'Items');
        assert.isObject(group, 'group');
        assert.lengthOf(items, Object.keys(db.authors).length);

        items.forEach(({ instance, context }) => {
            assert.instanceOf(instance, authorTypeResolver, 'instance');
            assert.instanceOf(context, TestContext, 'context');

            // All calls are done by the same context
            if (!contexts.has(call)) {
                contexts.set(call, context);
            }

            assert.strictEqual(contexts.get(call), context);
        });
    });

    assert.lengthOf(contexts, 3, 'context count');
}


function assertSameAgeAndBirthDate(message, response) {
    const done = {};

    assert.isUndefined(response.errors, `${message}: errors`);
    assert.isArray(response.data.authors, `${message}: authors is array`);

    const { authors } = response.data;

    assert.lengthOf(authors, Object.keys(db.authors).length, `${message}: authors count`);

    authors.forEach(({ id, age, birthDate }) => {
        assert.isString(id, `${message} / ${id}: id is string`);
        assert.isNumber(age, `${message} / ${id}: age is number`);

        assert.isUndefined(done[id], id, `${message} / ${id}: author not already treated`);

        done[id] = true;

        const author = db.authors[id];

        const expected = getAgeAndBirthDate({ gql: author });

        assert.strictEqual(expected.age, age, `${message} / ${id} / age`, `${message} / ${id}: age`);
        assert.strictEqual(expected.birthDate.getTime(), birthDate.getTime(), `${message} / ${id}: birth date`);
    });
}


class TestContext {
    val = faker.random.word()
}
