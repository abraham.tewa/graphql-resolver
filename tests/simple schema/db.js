// ============================================================
// Import modules
import { database } from '../setup';

// ============================================================
// Module's constants and variables
const db = database.generate(
    {
        authors: {
            count: 5,
            maxBooks: 10,
        },

        cities: {
            count: 3,
            maxLibraries: 5,
        },

        libraries: {
            maxBooks: 1000,
        },
        seed: 1234,
    },
);

// ============================================================
// Exports
export default db;
