/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert, expect } from 'chai';

import { Schema } from '../../src';

// ============================================================
// Import modules

import {
    schema as schemaTool,
} from '../setup';

import db from './db';

// ============================================================
// Tests

describe('Simple schema', () => {
    let schema;
    let ref;

    before(() => {
        ref = Symbol('integration test: simple schema');
        schema = schemaTool.generate({ database: db, ref }).schema;
    });

    after(() => {
        Schema.forgot(ref);
    });

    it('getRef return the reference of the schema', () => {
        assert.strictEqual(schema.getRef(), ref);
    });

    describe('created schema', () => {
        it('is not sealed', () => {
            assert.isFalse(schema.isSealed());
        });

        it('compile', async () => {
            await schema.compile();
            assert.isTrue(schema.isSealed());

            const content = await schema.getSchemaContent();

            expect(content).to.matchSnapshot();

            const response = await schema.resolve(`
                query {
                    authors {
                        id
                        firstName
                        lastName
                        books {
                            id
                            title
                            author {
                                id
                            }
                        }
                    }
                }
            `);

            assert.isUndefined(response.errors, 'query errors');
            assert.hasAllKeys(response.data, ['authors']);

            // Ensuring that the data match the db
            response.data.authors.forEach((author) => {
                assert.isObject(author);
                const dbAuthor = db.authors[author.id];

                assert.hasAllKeys(author, ['id', 'firstName', 'lastName', 'books']);

                assert.isObject(dbAuthor);
                assert.strictEqual(author.firstName, dbAuthor.firstName);
                assert.strictEqual(author.lastName, dbAuthor.lastName);

                assert.isArray(author.books);
                assert.lengthOf(author.books, dbAuthor.books.length);

                author.books.forEach((book) => {
                    const dbBook = db.books[book.id];

                    assert.hasAllKeys(book, ['id', 'title', 'author']);
                    assert.strictEqual(book.title, dbBook.title);
                    assert.strictEqual(book.author.id, author.id);
                });
            });
        });
    });
});
