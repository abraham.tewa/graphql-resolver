// ============================================================
// Import packages
import { Schema } from '../../../src';

// ============================================================
// Import modules
import generateAuthor from './Author';
import generateBook from './Book';
import generateQuery from './Query';

// ============================================================
// Functions

/**
 *
 * @param {Object} database
 * @param {*}      ref - Schema reference to use
 */
function generate({
    database,
    ref,
    includeResolvers = true,
    contextCreator,
}) {
    const types = {};

    const schema = new Schema(ref, undefined, contextCreator);

    Schema.declare(schema);

    schema.addGraphqlFilePattern('**/*.graphql', __dirname);

    if (includeResolvers) {
        types.Author = generateAuthor({ database, schema, types });
        types.Book = generateBook({ database, schema, types });
        types.Query = generateQuery({ database, schema, types });
    }

    return { types, schema };
}

// ============================================================
// Exports

export default generate;
