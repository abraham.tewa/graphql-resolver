// ============================================================
// Import packages
import { MultiField, Type } from '../../../src';

// ============================================================
// Type

function generate({ database, schema }) {
    class Author {
        #id;

        main = MultiField(
            ['firstName', 'lastName'],
            function main() {
                const { firstName, lastName } = this.getDocument();
                return { firstName, lastName };
            },
        )

        books() {
            return this.getDocument().books;
        }

        getDocument() {
            return database.authors[this.#id];
        }

        static create(context, id) {
            const author = new this(context);

            author.gql.id = id;
            author.#id = id;
            return author;
        }
    }

    return Type(
        Author,
        {
            ignore: ['create', 'getDocument'],
            schema,
        },
    );
}

// ============================================================
// Exports
export default generate;
