// ============================================================
// Import packages
import { Type } from '../../../src';

// ============================================================
// Class
function generate({ database, types, schema }) {
    class Query {
        static authors(context) {
            const authors = Object
                .values(database.authors)
                .map(({ id }) => types.Author.create(context, id));

            return authors;
        }
    }

    return Type(
        Query,
        {
            name: 'Query',
            schema,
        },
    );
}

// ============================================================
// Exports
export default generate;
