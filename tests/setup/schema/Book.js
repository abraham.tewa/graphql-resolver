// ============================================================
// Import packages
import { MultiField, Type } from '../../../src';

// ============================================================
// Type
function generate({ database, schema }) {
    class Book {
        main = MultiField(
            ['title', 'author', 'publicationYear'],
            function main() {
                const { title, author, publicationYear } = this.getDocument();
                return { title, author, publicationYear };
            },
        )

        libraries() {
            return this.getDocument().libraries;
        }

        getDocument() {
            return database.books[this.gql.id];
        }
    }

    return Type(
        Book,
        {
            ignore: ['create', 'libraries'],
            schema,
        },
    );
}

// ============================================================
// Exports
export default generate;
