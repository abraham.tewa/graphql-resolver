/* eslint-disable max-classes-per-file */
// ============================================================
// Import packages

import faker from 'faker';

// ============================================================
// Classes

class DbModel {
    /** @type {string} */
    id = faker.random.uuid();

    static instances = new WeakMap();

    constructor() {
        const Class = Object.getPrototypeOf(this).constructor;
        DbModel.instances.get(Class)[this.id] = this;
    }

    static initialize() {
        DbModel.instances.set(this, {});
    }

    static getAll() {
        return DbModel.instances.get(this);
    }

    /**
     * Return a random instance
     */
    static random() {
        return this.randomList(1)[0];
    }

    static randomList(nb) {
        const list = [];

        const all = Object.values(this.getAll()).values();

        while (list.count < nb) {
            const index = faker.random.number(list.length - 1);
            list.push(all[index]);
        }

        return list;
    }

    static generate(nb, ...args) {
        let i = 0;

        const list = [];
        while (i < nb) {
            i += 1;
            list.push(new this(...args));
        }

        return list;
    }
}

class Author extends DbModel {
    /** @type {Book[]} */
    books;

    /** @type {Date} */
    birthDate = faker.date.between(new Date('1920-01-01'), new Date('2005-12-31'));

    /** @type {string} */
    firstName = faker.name.firstName();

    /** @type {string} */
    lastName = faker.name.lastName();

    prefix = faker.name.prefix();

    constructor(maxBooks) {
        super();
        const nbBooks = faker.random.number(maxBooks);
        this.books = Book.generate(nbBooks, this);
    }
}

class Book extends DbModel {
    /** @type {string} */
    title = faker.lorem.sentence();

    /** @type {Author} */
    author;

    /** @type {number} */
    publicationYear = 1920 + faker.random.number(100);

    constructor(author) {
        super();
        this.author = author;
    }

    /**
     * Return the list of libraries that have the book
     * @returns {Library[]}
     * @public
     */
    get libarries() {
        return Library.getAll().filter(({ books }) => books.has(this));
    }
}

class City extends DbModel {
    /** @type {string} */
    name = faker.address.city();

    /** @type {string} */
    country = faker.address.country();

    /** @type {string} */
    zipCode = faker.address.zipCode();

    /** @type {Library[]} */
    libraries;

    /**
     *
     * @param {number} maxLibraries
     * @param {number} maxBooksByLibraries
     */
    constructor(maxLibraries, maxBooksByLibraries) {
        super();

        const nbLibraries = faker.random.number(maxLibraries);

        this.libraries = Library.generate(nbLibraries, maxBooksByLibraries);
    }
}

class Library extends DbModel {
    /** @type {string} */
    streetAddress = faker.address.streetAddress();

    /** @type {City} */
    city = City.random();

    constructor(maxBooks) {
        super();
        const nbBooks = faker.random.number(maxBooks);
        this.books = Book.randomList(nbBooks);
    }
}

// ============================================================
// Functions

function generate({
    authors: {
        count: nbAuthors,
        maxBooks: maxBooksAuthors,
    },
    cities: {
        count: nbCities,
        maxLibraries: maxLibrariesByCities,
    },
    libraries: {
        maxBooks: maxBooksByLibraries,
    },
    seed,
}) {
    if (typeof seed === 'number') {
        faker.seed(seed);
    }

    Author.initialize();
    Book.initialize();
    City.initialize();
    Library.initialize();

    Author.generate(nbAuthors, maxBooksAuthors);
    City.generate(nbCities, maxLibrariesByCities, maxBooksByLibraries);

    return {
        authors: Author.getAll(),
        books: Book.getAll(),
        cities: City.getAll(),
        libraries: Library.getAll(),
    };
}

function isEmpty() {
    return Object.keys(DbModel.instances).length === 0;
}

// ============================================================
// Exports
export default generate;
export {
    isEmpty,
};
