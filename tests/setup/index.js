import generateDb from './db';
import generateSchema from './schema';

export const schema = {
    generate: generateSchema,
};

export const database = {
    generate: generateDb,
};
