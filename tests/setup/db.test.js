/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert } from 'chai';

// ============================================================
// Import modules
import generate, { isEmpty } from './db';

describe('Database generator', () => {
    let database;

    before(() => {
        database = generate({
            authors: {
                count: 10,
                maxBooks: 10,
            },

            cities: {
                count: 10,
                maxLibraries: 5,
            },

            libraries: {
                maxBooks: 100,
            },
        });
    });

    it('is an object', () => {
        assert.isObject(database);

        assert.isNotEmpty(database.authors, 'Authors');
        assert.isNotEmpty(database.cities, 'Cities');
        assert.isNotEmpty(database.libraries, 'Libraries');
        assert.isNotEmpty(database.books, 'Books');
    });

    after(() => {
        assert.isTrue(isEmpty());
    });
});
