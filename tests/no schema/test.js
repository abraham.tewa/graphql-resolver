/* eslint-disable max-classes-per-file */
/* eslint-env node, mocha */

// ============================================================
// Import packages
import { assert, expect } from 'chai';
import faker from 'faker';
import sinon from 'sinon';

import { Field, Schema, Type } from '../../src';

// ============================================================
// Tests

describe('No schema', () => {
    describe('with one resolvers', () => {
        let ref;

        /** @type {Schema} */
        let schema;

        beforeEach(() => {
            ref = Symbol('integration tests: No schema / with resolvers');
            schema = Schema.declare(ref);

            schema.addGraphqlFilePattern(
                '**/*.graphql',
                __dirname,
            );
        });

        afterEach(() => {
            Schema.forgot(ref);
        });

        it('compile without resolvers', async () => {
            await schema.compile();
            assert.isTrue(schema.isSealed());

            const content = await schema.getSchemaContent();

            expect(content).to.matchSnapshot();

            const data = await schema.resolve(`
                query {
                    emptySchema
                }
            `);

            assert.isUndefined(data.errors);
            assert.hasAllKeys(data.data, ['emptySchema']);
            assert.isNull(data.data.emptySchema);
        });

        it('static resolver', async () => {
            Type(
                class Query {
                    static emptySchema() {
                        return true;
                    }
                },
                { schema },
            );

            await schema.compile();

            const data = await schema.resolve(`
                query {
                    emptySchema
                }
            `);

            assert.isUndefined(data.errors, 'errors');
            assert.hasAllKeys(data.data, ['emptySchema']);
            assert.isTrue(data.data.emptySchema);
        });

        it('static field resolver', async () => {
            Type(
                class Query {
                    static emptySchema = () => false
                },
                { schema },
            );

            await schema.compile();

            const data = await schema.resolve(`
                query {
                    emptySchema
                }
            `);

            assert.isUndefined(data.errors);
            assert.hasAllKeys(data.data, ['emptySchema']);
            assert.isFalse(data.data.emptySchema);
        });

        it('instance resolver', async () => {
            const value = faker.random.boolean();

            Type(
                class Query {
                    // eslint-disable-next-line class-methods-use-this
                    emptySchema() {
                        return value;
                    }
                },
                { schema },
            );

            await schema.compile();

            const data = await schema.resolve(`
                query {
                    emptySchema
                }
            `);

            assert.isUndefined(data.errors);
            assert.hasAllKeys(data.data, ['emptySchema']);
            assert.strictEqual(data.data.emptySchema, value);
        });

        it('renamed instance field resolver', async () => {
            const value = faker.random.boolean();
            const spy = sinon.spy(() => value);

            Type(
                class Query {
                    field = Field(
                        'emptySchema',
                        spy,
                    )
                },
                { schema },
            );

            await schema.compile();

            const data = await schema.resolve(`
                query {
                    emptySchema
                }
            `);

            assert.isUndefined(data.errors);
            assert.hasAllKeys(data.data, ['emptySchema']);
            assert.strictEqual(data.data.emptySchema, value);

            assert.isObject(spy.firstCall.thisValue);
        });
    });
});
