/* eslint-disable import/no-extraneous-dependencies */
/* eslint-env node, mocha */

import chai from 'chai';
import chaiJestSnapshot from 'chai-jest-snapshot';

function matchErrorSnapshotExpect(_chai, utils) {
    utils.addMethod(
        _chai.Assertion.prototype,
        'promise',
        function isPromise(message) {
            const obj = utils.flag(this, 'object');
            const negate = utils.flag(this, 'negate');

            const promiseAssertion = new chai.Assertion(obj, message);

            let result = Boolean(obj && typeof obj.then === 'function');

            if (negate) {
                result = !result;
            }

            // eslint-disable-next-line no-unused-expressions
            promiseAssertion.assert(
                result,
                'expected #{this} to be a promise',
                'expected #{this} not to be a promise',
            );
        },
    );

    utils.addMethod(
        _chai.Assertion.prototype,
        'promise',
        function isPromise(message) {
            const obj = utils.flag(this, 'object');
            const negate = utils.flag(this, 'negate');

            const promiseAssertion = new chai.Assertion(obj, message);

            let result = Boolean(obj && typeof obj.then === 'function');

            if (negate) {
                result = !result;
            }

            // eslint-disable-next-line no-unused-expressions
            promiseAssertion.assert(
                result,
                'expected #{this} to be a promise',
                'expected #{this} not to be a promise',
            );
        },
    );


    utils.addChainableMethod(
        _chai.Assertion.prototype,
        'matchErrorSnapshot',
        function matchErrorSnapshot(errorInfo, messsage) {
            const obj = utils.flag(this, 'object');

            let error;

            try {
                obj();
            }
            catch (err) {
                error = err;
            }

            if (typeof errorInfo === 'function') {
                new chai.Assertion(error).to.be.an.instanceof(errorInfo, messsage);
            }
            else if (typeof errorInfo === 'string') {
                new chai.Assertion(error.message).to.equal(errorInfo, messsage);
            }
            else if (errorInfo instanceof RegExp) {
                new chai.Assertion(error.message).to.match(errorInfo, messsage);
            }

            new chai.Assertion(error).to.matchSnapshot(messsage);

            utils.flag(this, 'object', error);
        },
    );
}

chai.use(chaiJestSnapshot);
chai.use(matchErrorSnapshotExpect);

before(() => {
    chaiJestSnapshot.resetSnapshotRegistry();
});

beforeEach(function beforeEach() {
    chaiJestSnapshot.configureUsingMochaContext(this);
});
